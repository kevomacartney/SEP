package com.Models;

import com.google.gson.annotations.SerializedName;

public enum BikeStatus {
        @SerializedName("0")
        Available,
        @SerializedName("1")
        Busy,
        @SerializedName("2")
        Lost;
}
