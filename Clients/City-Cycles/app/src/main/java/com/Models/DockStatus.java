package com.Models;

import java.util.List;
import java.util.UUID;

public class DockStatus {
    private UUID dockId;

    private String dockName;

    private int maximumCapacity;

    private List<Bikes> bikes;

    public String getDockName() {
        return dockName;
    }

    public UUID getDockId() {
        return dockId;
    }

    public int getMaximumCapacity() {
        return maximumCapacity;
    }

    public List<Bikes> getBikes() { return bikes; }

    //Necessary for spinner
    public String toString() {
        return dockName;
    }
}
