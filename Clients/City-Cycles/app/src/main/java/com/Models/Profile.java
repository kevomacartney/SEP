package com.Models;

import java.util.UUID;

public class Profile {


    private UUID userId;
    private String firstName;
    private String lastName;
    private String email;
    private String postCode;
    private String streetAddress;
    private String city;
    //Needs to be implemented in api
    private String SecurityToken;

    public UUID getId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String FirstName) {
        firstName = FirstName;
    }

    public String getLastName() { return lastName; }

    public void setLastName(String LastName) {
        lastName = LastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String Email) {
        email = Email;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String PostCode) {
        postCode = PostCode;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String StreetAddress) {
        streetAddress = StreetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String City) {
        city = City;
    }

    public String getSecurityToken() {
        return SecurityToken;
    }
}
