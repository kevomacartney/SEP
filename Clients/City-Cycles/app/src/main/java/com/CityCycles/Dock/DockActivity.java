package com.CityCycles.Dock;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.CityCycles.NfcActivity;
import com.CityCycles.CityCyclesApi.ApiProvider;
import com.CityCycles.UpdateActivity;
import com.CityCycles.app.R;
import com.Models.BikeStatus;
import com.Models.DockStatus;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class DockActivity extends AppCompatActivity implements View.OnClickListener,AdapterView.OnItemSelectedListener {

    public DockActivity()
    {

    }
    // The Api provider
    ApiProvider mApiProvider;

    private int availableBikes;
    private Button buttonNfc;
    private Button buttonUpdate;
    private Button buttonLogout;
    private Spinner spinner;
    private static final String mLogTag = "DockActivity";

    // Initialise input array for spinner (drop-down list).
    private static ArrayList<DockStatus> docks = new ArrayList<>();

    private TextView dockCapacityTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dock);

        mApiProvider = new ApiProvider();
        Gson gson = new Gson();

        //Initialise starting place for available bikes before incrementing begins.
        availableBikes = 0;

        //Get text views which will have values changed at run time
        dockCapacityTextView = findViewById(R.id.dockCapacity);

        //Create drop down list frame
        spinner = findViewById(R.id.dockSpinner);

        //Click on this to switch to nfc screen
        buttonNfc = findViewById(R.id.buttonNfc);
        buttonNfc.setOnClickListener(this);

        //Click on this to log out
        buttonLogout = findViewById(R.id.buttonLogout);
        buttonLogout.setOnClickListener(this);

        //Click on this to switch to update screen
        buttonUpdate = findViewById(R.id.buttonUpdate);
        buttonUpdate.setOnClickListener(this);

        //Clear spinner array when page is loaded
        docks.clear();

        try {
            //Build URL (To reach correct part of API)
            String url = mApiProvider.BuildUrl("Dock", "FindAll", null);
            //Returns json string with lots of values
            String result = mApiProvider.SendGetRequest(url);

            //Give Gson correct type info
            Type type = new TypeToken<ArrayList<DockStatus>>() {
            }.getType();

            //Turn json into arrayList
            docks = gson.fromJson(result, type);
        } catch (RuntimeException e) {

            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        } catch (Exception e) {

            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();

        }

        //Create adapter for spinner, basically formats it and inputs data.
        ArrayAdapter<DockStatus> adapter = new ArrayAdapter<>(
                this, android.R.layout.simple_spinner_item, docks);

        //Formatting
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        //Assign the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);


    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        availableBikes = 0;

        //Obtain selected object
        DockStatus dock = (DockStatus) spinner.getSelectedItem();
        Log.i(mLogTag, dock.getDockName());
        Log.i(mLogTag, "Item has been selected");

        // For each dock in the list

        if(docks != null) {
            availableBikes = (int) docks.stream()
                    .filter(d -> d.getDockName() == dock.getDockName())
                    .findFirst()
                    .get().getBikes()
                    .stream()
                    .filter(b -> b.getStatus() == BikeStatus.Available).count();
        }
        else Log.i(mLogTag, "Docks is null");

        //Display max capacity of selected dock
        dockCapacityTextView.setVisibility(View.VISIBLE);
        dockCapacityTextView.setText( "Available bikes: " + availableBikes + " out of " + dock.getMaximumCapacity() + " possible bikes.");

    }

    public void onNothingSelected(AdapterView<?> parent) {
        //This is required.
    }

    @Override
    public void onClick(View view) {
        if(view == buttonNfc) {
            finish();
            //Move to nfc activity (With phone connected)
            startActivity(new Intent(this, NfcActivity.class));
        }
        if(view == buttonUpdate) {
            finish();
            //Move to update activity
            startActivity(new Intent(this, UpdateActivity.class));
        }
        if(view == buttonLogout) {
            finish();
            //Log out
            mApiProvider.LogOut();
        }
    }
}
