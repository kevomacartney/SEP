package com.CityCycles.AppConfigurations;

import com.CityCycles.AppConfigurations.Base.IAppConfiguration;
import com.google.gson.Gson;

/**
 * @brief Contains configurations needed to load the API
 */
public class ApiConfiguration implements IAppConfiguration {

    public String ApiUrl;

    public int Timeout;

    @Override
    public String SerializeConfiguration() {
        // Convert this object to json
        return new Gson().toJson (this);
    }
}
