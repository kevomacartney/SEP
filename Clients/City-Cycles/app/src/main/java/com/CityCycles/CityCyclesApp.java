package com.CityCycles;

import android.app.Application;
import android.content.Context;

import com.CityCycles.AppConfigurations.Base.ConfigurationsModule;
import com.CityCycles.AppConfigurations.ApiConfiguration;

/**
 * @brief Application class
 */
    public class CityCyclesApp extends Application {
        private static Context context;

        public void onCreate() {
            super.onCreate();
            context = getApplicationContext();

            CreateConfigurationRegistrations();
        }

        public static Context getAppContext() {
            return context;
        }

        public void CreateConfigurationRegistrations()
        {
            // Todo, Add your configurations here

            try {
                ConfigurationsModule.RegisterConfiguration(ApiConfiguration.class);
            }
            catch (Exception e) {
            }
        }
    }
