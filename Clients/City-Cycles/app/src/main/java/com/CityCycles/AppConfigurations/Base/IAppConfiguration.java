package com.CityCycles.AppConfigurations.Base;

public interface IAppConfiguration {
    /**
     *
     * @return Json Representation of the configuration class
     */
    String SerializeConfiguration();
}
