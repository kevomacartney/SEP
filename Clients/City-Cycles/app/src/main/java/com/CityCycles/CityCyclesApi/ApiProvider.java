package com.CityCycles.CityCyclesApi;

import android.icu.util.Calendar;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.util.Pair;
import android.util.Property;

import com.CityCycles.AppConfigurations.ApiConfiguration;
import com.CityCycles.AppConfigurations.Base.ConfigurationsModule;
import com.CityCycles.ManualResetEvent;
import com.Models.LoginModel;
import com.Models.Profile;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.ObjectStreamException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ApiProvider /*implements Parcelable*/{

    // Media type for API
    private static final MediaType JSON
            = MediaType.get("application/json; charset=utf-8");

    // The log tag
    private String mLogTag = "ApiProvider";

    // The configuration module instance
    private ConfigurationsModule mConfiguration;

    // Access token containing bearer token for the api
    private static AccessToken mToken = new AccessToken();

    // Reset event used to sync http request for getting the bearer token
    private ManualResetEvent mGetTokenSync = new ManualResetEvent(true);

    // the http client we use to send requests
    private OkHttpClient mClient = new OkHttpClient();

    // Executor service with 10 threads
    private ExecutorService mExecutorService = Executors.newFixedThreadPool(10);

    // The login model containing the user's credentials
    private LoginModel mLoginModel = new LoginModel();

    private ApiConfiguration mApiConfig;

<<<<<<< HEAD
<<<<<<< HEAD
    public ApiProvider(/*Parcel parcel*/) {
        // Get the configuration instance
        mConfiguration = ConfigurationsModule.GetInstance();

        ApiConfiguration config = new ApiConfiguration();
        config.ApiUrl = "http://10.0.2.2:44322/api/";
        //config.ApiUrl = "http://192.168.0.196/api/";
        config.Timeout = 30;
        mConfiguration.UpdateConfiguration(config);
        mApiConfig = (ApiConfiguration) mConfiguration.GetConfiguration(ApiConfiguration.class);

        //mToken = (AccessToken) parcel.readValue(null);

=======
=======
>>>>>>> Develop
    public ApiProvider() {
        // Get the configuration instance
        mConfiguration = ConfigurationsModule.GetInstance();

        mApiConfig = mConfiguration.GetConfiguration(ApiConfiguration.class);

        // Check if the configuration exists
        //if(mApiConfig == null)
            CreateDefaultConfigurations(); // Create the default config if the configuration wasn't found
<<<<<<< HEAD
>>>>>>> Develop
=======
>>>>>>> Develop
    }

    /**
     * Will dispatch the request and wait for the response
     * @param request An instance of the request to be sent
     * @return A pair Response object and the body content
     */
    private Pair<Response, String> CompleteRequest(Request request)
    {
        //Try executing the request
        try (Response response = mClient.newCall(request).execute()) {
            if(!response.isSuccessful()) {
                // Builds an error string
                StringBuilder b = new StringBuilder();

                b.append("Request to ")
                        .append(request.url().toString())
                        .append(" was not successful.")
                        .append("\nStatus Code")
                        .append(response.code());

                Log.w(mLogTag, b.toString());
            }

            return new Pair<>(response, response.body().string());

        } catch (IOException e) {
            Log.e(mLogTag, "Could not complete http request.", e);
            return null;
        }
        // TODO: not sure if any other exception is thrown will crash the app. Need to do some testing.
    }

    /**
     * Tries to authenticate user and returns 0 if successful and api status code otherwise
     * @param model The login model
     * @return The status code
     */
    public int Authenticate(LoginModel model) {

        try{
            mToken = RequestAccessToken(model);
            return 0;
        }
        catch (ExecutionException | InterruptedException | TimeoutException e)
        {
            return -1;
        }
        catch(RuntimeException e)
        {
            return Integer.parseInt(e.getMessage());
        }
    }

    /**
     * @return The Bearer token for the app
     * @brief Requests a bearer token from the API
     */
<<<<<<< HEAD
<<<<<<< HEAD

    public AccessToken RequestAccessToken(LoginModel model) throws ExecutionException, InterruptedException, TimeoutException {
=======
    private AccessToken RequestAccessToken(LoginModel model) throws ExecutionException, InterruptedException, TimeoutException {
>>>>>>> Develop
=======
    private AccessToken RequestAccessToken(LoginModel model) throws ExecutionException, InterruptedException, TimeoutException {
>>>>>>> Develop
        // Create the body for the request
        RequestBody body = RequestBody.create(JSON, new Gson().toJson(model));

        // Build the request
        Request request = new Request.Builder()
                .url(BuildUrl("Auth", "AuthenticateUserApp"))
                .post(body)
                .build();

        // Place the work in the worker thread pool and get a Future
        Future<Pair<Response, String>> responseFuture = mExecutorService.submit(() -> CompleteRequest(request));

        try {
            // Get the response and timeout after 30 seconds if no response was received
            Pair<Response, String> responseFromServer = responseFuture.get(30, TimeUnit.SECONDS);

            // An exception was thrown
            if (responseFromServer == null)
                throw new TimeoutException("Could not retrieve token from api");

            // We do not have a valid response from the server
            if (!responseFromServer.first.isSuccessful()) {
                throw new RuntimeException(String.valueOf(responseFromServer.first.code()));
            }

            // Create the token
            mToken = new AccessToken();
            mToken.setToken(responseFromServer.second);

            // Get current time(as calender) and add 59 minutes to the time
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MINUTE, 59);
            mToken.setExpiryDate(cal.getTime());

            return mToken;
        } catch (ExecutionException | InterruptedException e) {
            Log.e(mLogTag, "There was an thread concurrency exception while getting response from Future", e);
            throw e;
        } catch (TimeoutException e) {
            Log.e(mLogTag, "Request from api timeout", e);
            throw e;
        }
    }

    /**
     * @return The Bearer token for the app
     * @brief Gets the current access token and if it has expired will renew it.
     */
    private String GetAccessToken() throws InterruptedException, ExecutionException, TimeoutException {
        Calendar cal = Calendar.getInstance();
        long diff = cal.getTime().getTime() - mToken.getExpiryDate().getTime();

        // The token will expire soon
        if (TimeUnit.MINUTES.convert(diff, TimeUnit.MILLISECONDS) > 1) {
            mToken = RequestAccessToken(mLoginModel);
        }

        return mToken.getToken();
    }

    /**
     * Will build the Url using the controller, and the action then return the full Url.
     *
     * @param controller The controller the url will create for
     * @param action     The action in the controller to handle the request
     * @return The complete Url
     */
    public String BuildUrl(String controller, String action)
    {
        return BuildUrl(controller, action, null);
    }

    /**
     * Will build the Url using the controller, and the action then return the full Url.
     *
     * @param controller The controller the url will create for
     * @param action     The action in the controller to handle the request
     * @return The complete Url
     */
    public String BuildUrl(String controller, String action, String queries) {
        //Set up the builder
        StringBuilder builder = new StringBuilder();
        builder.append(mApiConfig.ApiUrl);
        builder.append(controller + "/" + action);

        if(queries != null) builder.append("?" + queries);


        return builder.toString();
    }

    /**
     * Sends a post request to the URL
     *
     * @param url  The URL the request will be dispatched to
     * @param data The data that will be sent with the request
     * @return The message returned by the request
     */
    public String SendPostRequest(String url, Object data) throws TimeoutException, ExecutionException, InterruptedException {
        // Create the body for the request
        RequestBody body = RequestBody.create(JSON, new Gson().toJson(data));

        // Build the request
        Request request = new Request.Builder()
                .url(url)
                .addHeader("User-Agent", "CityCycles.App")
                .addHeader("Accept", "application/json; q=0.5")
                .addHeader("Authorization", "Bearer " + GetAccessToken())
                .post(body).build();

        try {
            // Place the work in the worker thread pool and wait for the response
            Pair<Response, String> responsePair = mExecutorService.submit(() -> CompleteRequest(request)).get(mApiConfig.Timeout, TimeUnit.SECONDS);

            // Throw an exception if status code was not successful with message as the body content
            if (!responsePair.first.isSuccessful())
                throw new RuntimeException(responsePair.second);

            return responsePair.second;
        } catch (ExecutionException | InterruptedException e) {
            Log.e(mLogTag, "Thread concurrency exception was thrown while sending post", e);
            throw e;
        }
    }

    public String SendGetRequest(String url) throws TimeoutException, ExecutionException, InterruptedException{

        // prepare the request pipeline (headers)
        Request request = new Request.Builder()
                .url(url)
                .get()
                .addHeader("User-Agent", "CityCycles.App")
                .addHeader("Accept", "application/json; q=0.5")
                .addHeader("Authorization", "Bearer " + GetAccessToken())
                .build();

        try {
            // Place the work in the worker thread pool and wait for the response
            Pair<Response, String> responsePair = mExecutorService.submit(() -> CompleteRequest(request)).get(mApiConfig.Timeout, TimeUnit.SECONDS);

            // Throw an exception if status code was not successful with message as the body content
            if (!responsePair.first.isSuccessful())
                throw new RuntimeException(responsePair.second);

            return responsePair.second;
        } catch (ExecutionException | InterruptedException e) {
            Log.e(mLogTag, "Thread concurrency exception was thrown while sending get", e);
            throw new RuntimeException("There was an error while handling the request please try again");
        }

    }

    /**
     * @brief Creates default configuration and writes it to the phone storage
     */
    private void CreateDefaultConfigurations(){
        ApiConfiguration config = new ApiConfiguration();
        //config.ApiUrl = "http://10.0.2.2:44322/api/";
        //config.ApiUrl = "http://192.168.0.196:44322/api/";
        //Below url is my phone hotspot
        config.ApiUrl = "http://172.20.10.2:44322/api/";
        config.Timeout = 30;

        // Update the config
        mConfiguration.UpdateConfiguration(config);
        mApiConfig = mConfiguration.GetConfiguration(ApiConfiguration.class);
    }

    public void LogOut(){
        mToken = null;
    }

    /**
     *
     * @brief Gets the current user profile and returns it
     */
    public Profile getProfile() {
        Gson g = new Gson();
        try {
            //Build URL (To reach correct part of API)
            String url = BuildUrl("Auth", "Me", null);

            //Returns json string with lots of values
            String result = SendGetRequest(url);

            //Convert json to Profile object
            Profile userProfile = g.fromJson(result, Profile.class);
            Log.i(mLogTag, "User profile obtained");
            return userProfile;
        }
        catch (ExecutionException | InterruptedException | TimeoutException e) {
            Log.e(mLogTag, "Thread concurrency exception was thrown while sending get profile", e);
            throw new RuntimeException("There was an error while handling the request please try again");
        }
    }
}
