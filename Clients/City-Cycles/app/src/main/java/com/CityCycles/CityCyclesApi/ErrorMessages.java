package com.CityCycles.CityCyclesApi;

import android.annotation.SuppressLint;

import java.util.HashMap;

public class ErrorMessages {
    private static HashMap<Integer, String> Messages = new HashMap<Integer, String>()
    {{
        // todo, get message
        put(1, "");
        put(520, "");
    }};

    /**
     * Gets a human friendly message fot the specified error code
     * @param apiCode The api code returned by the api
     * @return A human friendly api code
     */
    public static String GetErrorMessage(int apiCode)
    {
        // Gets the message for the code
        String msg =  Messages.get(apiCode);

        if(msg ==null)
            return "Server returned unexpected message.";

        return msg;
    }
}
