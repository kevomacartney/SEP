package com.CityCycles;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.CityCycles.CityCyclesApi.ApiProvider;
import com.CityCycles.Dock.DockActivity;
import com.CityCycles.app.R;
import com.Models.DockStatus;
import com.Models.Profile;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class UpdateActivity extends AppCompatActivity implements View.OnClickListener{
    //Define variables
    private Button buttonUpdate;
    private Profile userProfile;

    //Define textViews
    private TextView textViewFirstName, textViewLastName, textViewEmail, textViewPostcode;
    private TextView textViewStreetAddress, textViewCity;

    //Define editTexts
    private EditText editTextFirstName, editTextLastName, editTextEmail;
    private EditText editTextPostcode, editTextStreetAddress, editTextCity;

    //Define Strings for use in Profile class
    private String firstName, lastName, email, postcode, streetAddress, city;

    //Will be using ApiProvider class
    ApiProvider mApiProvider;
    //Log tag
    private static final String mLogTag = "UpdateActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);

        //Configure button
        buttonUpdate = findViewById(R.id.buttonUpdate);
        buttonUpdate.setOnClickListener(this);

        mApiProvider = new ApiProvider();
        //Get user profile
        userProfile = mApiProvider.getProfile();

        Log.i(mLogTag, userProfile.toString());

        //Assign TextViews
        textViewFirstName = findViewById(R.id.textViewFirstName);
        textViewLastName = findViewById(R.id.textViewLastName);
        textViewEmail = findViewById(R.id.textViewEmail);
        textViewPostcode = findViewById(R.id.textViewPostcode);
        textViewStreetAddress = findViewById(R.id.textViewStreetAddress);
        textViewCity = findViewById(R.id.textViewCity);
        //Set them to server values
        textViewFirstName.setText(textViewFirstName.getText() + " " + userProfile.getFirstName());
        textViewLastName.setText(textViewLastName.getText() + " " + userProfile.getLastName());
        textViewEmail.setText(textViewEmail.getText() + " " + userProfile.getEmail());
        textViewPostcode.setText(textViewPostcode.getText() + " " + userProfile.getPostCode());
        textViewStreetAddress.setText(textViewStreetAddress.getText() + " " + userProfile.getStreetAddress());
        textViewCity.setText(textViewCity.getText() + " " + userProfile.getCity());

        //Assign editTexts
        editTextFirstName = findViewById(R.id.editTextFirstName);
        editTextLastName = findViewById(R.id.editTextLastName);
        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPostcode = findViewById(R.id.editTextPostcode);
        editTextStreetAddress = findViewById(R.id.editTextStreetAddress);
        editTextCity = findViewById(R.id.editTextCity);
    }

    //Verification takes place in this function. Checks user input
    public void verifyFields() {
        //Get user input
        firstName = editTextFirstName.getText().toString().trim();
        lastName = editTextLastName.getText().toString().trim();
        email = editTextEmail.getText().toString().trim();
        postcode = editTextPostcode.getText().toString().trim();
        streetAddress = editTextStreetAddress.getText().toString().trim();
        city = editTextCity.getText().toString().trim();

        //Verify user input to see if it has been changed.
        if(firstName.length() != 0) {
            userProfile.setFirstName(firstName);
        }
        if(lastName.length() != 0) {
            userProfile.setLastName(lastName);
        }
        if(email.length() != 0) {
            userProfile.setEmail(email);
        }
        if(postcode.length() != 0) {
            userProfile.setPostCode(postcode);
        }
        if(streetAddress.length() != 0) {
            userProfile.setStreetAddress(streetAddress);
        }
        if(city.length() != 0) {
            userProfile.setCity(city);
        }

        updateStatus();
    }

    //If verification completes successfully, status is updated.
    public void updateStatus() {

        String url = mApiProvider.BuildUrl("Auth", "Me");

        //Send the user profile to the API
        try {
            Toast.makeText(this, "Details changed", Toast.LENGTH_LONG).show();
            mApiProvider.SendPostRequest(url, userProfile);
        }
        catch (TimeoutException | ExecutionException | InterruptedException e){
            Log.e(mLogTag, "Error during userProfile POST");
            Toast.makeText(this, "Error changing profile information.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        if(view == buttonUpdate) {
            //Verify then update
            verifyFields();
            Intent myIntent = new Intent(this, DockActivity.class);
            this.startActivity(myIntent);

        }
    }
}
