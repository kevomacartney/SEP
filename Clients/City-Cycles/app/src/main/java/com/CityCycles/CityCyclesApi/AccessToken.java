package com.CityCycles.CityCyclesApi;

import java.util.Date;

/**
 * @breif Represents
 */
public class AccessToken {
    private String Token;

    private Date ExpiryDate;

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }


    public Date getExpiryDate() {
        return ExpiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        ExpiryDate = expiryDate;
    }
}