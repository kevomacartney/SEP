package com.CityCycles.AppConfigurations.Base;


import android.content.Context;
import android.util.Log;

import com.CityCycles.CityCyclesApp;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;


public class ConfigurationsModule {

    private static String sLogTag = "App-Configuration";

    // Configuration instance
    private static ConfigurationsModule sConfigurationsModule;

    // App context instance
    private Context mContext;

    // The configuration file name
    private String mConfigName = "appsettings-v1.2.1.json";

    // Boolean indicating the configuration has been started
    private static boolean mStarted = false;

    // Contains a list of all register configuration types
    private static HashSet<Class<? extends IAppConfiguration>> sRegisteredConfigurations = new HashSet<>();

    // A hash map of all the configurations that are available configurations
    private HashMap<String, String> mConfigurations = new HashMap<>();

    public static ConfigurationsModule GetInstance() {
        // If instance doesn't exist, create a new one
        if (sConfigurationsModule == null)
            sConfigurationsModule = new ConfigurationsModule();

        // Return the only instance
        return sConfigurationsModule;
    }

    private ConfigurationsModule() {
        mContext = CityCyclesApp.getAppContext();

        if(false)

        try {
            // Read the configuration file
            String json = ReadConfiguration();

            // Create the configuration map
            mConfigurations = CreateConfigurationMap(json);

            mStarted = true;
        }
        catch (IOException e)
        {
            Log.d(sLogTag, "Could not load configuration file.", e);
        }
    }

    public  <T extends IAppConfiguration> T GetConfiguration(Class<T> clazz)
    {
        String config = mConfigurations.get(clazz.getName());
        Gson gson = new Gson();

       return gson.fromJson(config, clazz);
    }

    /**
     * @brief Updates the configuration and writes it to storage. OVERWRITES
     * @param config The configuration instance
     */
    public void UpdateConfiguration(IAppConfiguration config)
    {
        // Serialize the configuration instance
        String configStr = config.SerializeConfiguration();

        mConfigurations.put(config.getClass().getName(), configStr);

        // Write the new configuration
        WriteConfiguration();
    }

    /**
     * @brief Reads the json configuration file
     * @return The contents of the JSON file
     */
    private String ReadConfiguration () throws IOException
    {
        FileInputStream fis;
        try {
            // Tru opening the file
             fis = mContext.openFileInput(mConfigName);

            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;

            // For each line, write it to the string builder
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            return sb.toString();
        }
        catch (FileNotFoundException e) {
            Log.w(sLogTag, " file was not found", e);

            return null;
        }
    }

    /**
     * @brief Register a class to to have it loaded as a configuration
     * @param configuration The configuration instance
     */
    public static void RegisterConfiguration(Class<? extends IAppConfiguration> configuration) throws Exception {
        if (mStarted)
            throw new RuntimeException("Cannot register configuration after module has been started");

        if(sRegisteredConfigurations.contains(configuration))
        {
            Log.w(sLogTag, "Configuration type already registered");

            return;
        }
        // register the class type
        sRegisteredConfigurations.add(configuration);
    }

    private HashMap<String, String> CreateConfigurationMap(String json)
    {
        // Convert the strong to dictionary
        Gson gson = new Gson();
        HashMap<String, String> configs = gson.fromJson(json, HashMap.class);

        if(configs == null)
            return new HashMap<>();

        return configs;
    }

    /**
     * @brief Writes the configuration to the storage
     */
    public void WriteConfiguration()
    {
        Gson s = new Gson();
        String str = s.toJson(mConfigurations);
        try {
            FileOutputStream fos = CityCyclesApp.getAppContext().openFileOutput(mConfigName, Context.MODE_PRIVATE);

            // Check if the string is empty
            if (str != null) {
                fos.write(str.getBytes());
            }

            // Close the stream
            fos.close();
        } catch (IOException ioException) {
            Log.e(sLogTag, "There was an error while writing the configuration file");
        }
    }
}
