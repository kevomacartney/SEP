package com.CityCycles.Account;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.CityCycles.CityCyclesApi.ApiProvider;
import com.CityCycles.Dock.DockActivity;
import com.CityCycles.app.R;
import com.Models.LoginModel;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button buttonLogIn;
    private EditText editTextEmail, editTextPassword;

    ApiProvider mApiProvider;

    private static final String mLogTag = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        buttonLogIn =  findViewById(R.id.buttonLogIn);

        mApiProvider = new ApiProvider();

        buttonLogIn.setOnClickListener(this);
    }

    /**
     * @brief Binds elements and their event handlers
     */

    private void userLogIn() {
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            //email is empty
            Toast.makeText(this, "Please enter email", Toast.LENGTH_SHORT).show();

            Log.w(mLogTag, "User provided an invalid email");
            return;
        }

        if (TextUtils.isEmpty(password)) {
            //password is empty
            Toast.makeText(this, "Please enter password", Toast.LENGTH_SHORT).show();

            Log.w(mLogTag, "User provided an invalid password");
            return;
        }

        LoginModel model = new LoginModel();

        model.setPassword(password);
        model.setUsername(email);

        Log.i(mLogTag, "Logging in a user");

        // 1. Instantiate a dialog with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

<<<<<<< HEAD
<<<<<<< HEAD
        try {
            Log.i(mLogTag, "Logging in a user");


            // Request access token for user
            AccessToken token = mApiProvider.RequestAccessToken(model);

            // We have an access token from the server, set it
            mApiProvider.SetAccessToken(token);

            Log.i(mLogTag, "User has been authenticated");

            //todo, switch to dock activity
            //Intent intent = new Intent(this, DockActivity.class);
            //intent.putExtra("ApiProvider", mApiProvider);
            //startActivity(intent);
            startActivity(new Intent(this, NfcActivity.class));
        }
        catch (TimeoutException | InterruptedException | ExecutionException e) {
            builder.setMessage("Error while connecting to the Server")
                    .setTitle("Oops");

            Log.e(mLogTag, "Threading exception while handing login request", e);

            // Close button
            builder.setPositiveButton("Ok", null);
        }
        catch (RuntimeException e){
            builder.setMessage(e.getMessage())
                    .setTitle("Oops");

            Log.e(mLogTag, "API provider threw a runtime exception while handling login request", e);

            // Close button
            builder.setPositiveButton("Ok", null);
=======
=======
>>>>>>> Develop
        switch (mApiProvider.Authenticate(model)) {
            case 0:
                Log.i(mLogTag, "User has been authenticated");
                Toast.makeText(this, "Logging in...", Toast.LENGTH_LONG).show();

                // Start the dock activity
                Intent myIntent = new Intent(this, DockActivity.class);
                this.startActivity(myIntent);
                return;
            case -1:
                Log.i(mLogTag, "Please check your connection");
                Toast.makeText(this, "Please check your connection", Toast.LENGTH_SHORT).show();
                break;
            case 524:
                Log.i(mLogTag, "Please check username and/or password. ");
                Toast.makeText(this, "Please check username and/or password", Toast.LENGTH_SHORT).show();
                return;
            default:
                Log.i(mLogTag, "Could not authenticate user");

                builder.setMessage("Error while connecting to the server, try again later")
                        .setTitle("Oops");
                builder.setPositiveButton("Ok", null);
                break;
<<<<<<< HEAD
>>>>>>> Develop
=======
>>>>>>> Develop
        }

        // 2. Show the dialog, if there was an error
        builder.create().show();
    }

    @Override
    public void onClick(View view) {
        if(view == buttonLogIn) {
            userLogIn();
        }
    }
}
