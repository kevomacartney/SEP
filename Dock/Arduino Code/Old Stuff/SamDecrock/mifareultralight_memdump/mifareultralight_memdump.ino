/**************************************************************************/
/*!
    This example attempts to dump the contents of a Mifare Ultralight card
    Note that you need the baud rate to be 115200 because we need to print
    out the data and read from the card at the same time!
    To enable debug message, define DEBUG in PN532/PN532_debug.h
    Edited by Sam Decrock to read out Mifare Ultralight cards
*/
/**************************************************************************/

/* change this to 1 if you want to use SPI: */
#if 1 
  #include <SPI.h>
  #include <PN532_SPI.h>
  #include "PN532.h"

  PN532_SPI pn532spi(SPI, 10);
  PN532 nfc(pn532spi);
#else 
  #include <Wire.h>
  #include <PN532_I2C.h>
  #include <PN532.h>
  #include <NfcAdapter.h>
  
  PN532_I2C pn532_i2c(Wire);
  PN532 nfc(pn532_i2c);
#endif




void setup(void) {
  // has to be fast to dump the entire memory contents!
  Serial.begin(115200);
  Serial.println("Looking for PN532...");

  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("Didn't find PN53x board");
    while (1); // halt
  }
  // Got ok data, print it out!
  Serial.print("Found chip PN5"); Serial.println((versiondata>>24) & 0xFF, HEX);
  Serial.print("Firmware ver. "); Serial.print((versiondata>>16) & 0xFF, DEC);
  Serial.print('.'); Serial.println((versiondata>>8) & 0xFF, DEC);

  // configure board to read RFID tags
  nfc.SAMConfig();

  Serial.println("Waiting for an ISO14443A Card ...");
}


void loop(void) {
  uint8_t success;                          // Flag to check if there was an error with the PN532
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
  uint8_t currentpage;                      // Counter to keep track of which page we're on
  bool authenticated = false;               // Flag to indicate if the sector is authenticated
  uint8_t data[4];                          // Array to store page data during reads



  // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
  // 'uid' will be populated with the UID, and uidLength will indicate
  // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);

  if (success) {
    // Display some basic information about the card
    Serial.println("Found an ISO14443A card");
    Serial.print("  UID Length: ");Serial.print(uidLength, DEC);Serial.println(" bytes");
    Serial.print("  UID Value: ");
    for (uint8_t i = 0; i < uidLength; i++) {
      Serial.print(uid[i], HEX);
      Serial.print(' ');
    }
    Serial.println("");


    // read all 16 pages (of 4 bytes each):
    for (currentpage = 0; currentpage < 16; currentpage++)
    {
    
      // Dump the data into the 'data' array
      success = nfc.mifareultralight_ReadPage(currentpage, data);
      if (success)
      {
        // Read successful
        Serial.print("Page ");Serial.print(currentpage, DEC);
        if (currentpage < 10)
        {
          Serial.print("  ");
        }
        else
        {
          Serial.print(" ");
        }
        // Dump the raw data
        nfc.PrintHexChar(data, 4);
      }
      else
      {
        // Oops ... something happened
        Serial.print("Page ");Serial.print(currentpage, DEC);
        Serial.println(" unable to read this page");
      }

    }

  }
  // Wait a bit before trying again
  Serial.println("\n\nSend a character to run the mem dumper again!");
  Serial.flush();
  while (!Serial.available());
  while (Serial.available()) {
  Serial.read();
  }
  Serial.flush();
}
