void setup() {
  Serial.begin(9600);    // Initialize serial communications with the PC
  while (!Serial);    // Do nothing if no serial port is opened (added for Arduinos based on ATMEGA32U4)
  // put your setup code here, to run once:
  Serial.println("Hello world");
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(10);
  Serial.println("Hello world");
}
