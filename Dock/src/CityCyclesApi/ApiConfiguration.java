package CityCyclesApi;

import com.google.gson.Gson;

/**
 * @brief Contains configurations needed to load the API
 */
public class ApiConfiguration {

    public String ApiUrl;

    public int Timeout;

    public String SerializeConfiguration() {
        // Convert this object to json
        return new Gson().toJson(this);
    }
}
