package Models;

public class QuickOrderModel {
    private String Name;
    private Integer SlotNo;
    private String UUID;

    public QuickOrderModel(String Name, Integer SlotNo, String UUID){
        this.Name = Name;
        this.SlotNo = SlotNo;
        this.UUID = UUID;
    }
}
