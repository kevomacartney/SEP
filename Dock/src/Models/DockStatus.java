package Models;

import java.util.UUID;

public class DockStatus {
    private UUID dockId;

    private String dockName;

    private int maximumCapactity;

    public String getDockName() {
        return dockName;
    }

    public UUID getDockId() {
        return dockId;
    }

    public int getMaximumCapactity() {
        return maximumCapactity;
    }
}
