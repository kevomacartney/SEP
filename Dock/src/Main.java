import CityCyclesApi.ApiProvider;
import Models.LoginModel;
import Models.QuickOrderModel;
import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortPacketListener;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Main
{
    /*private static final class PacketListener implements SerialPortPacketListener
    {
        private StringBuilder input = new StringBuilder();

        public StringBuilder getInput()
        {
            return input;
        }

        public void clearInput()
        {
            input.delete(0, input.length());
        }

        @Override
        public int getListeningEvents() { return SerialPort.LISTENING_EVENT_DATA_RECEIVED; }

        @Override
        public int getPacketSize() { return 38; }

        @Override
        public void serialEvent(SerialPortEvent event)
        {
            byte[] newData = event.getReceivedData();

            for (int i = 0; i < newData.length; ++i)
                input.append(newData[i]);
        }
    }*/

    public static void main(String[] args)
    {
        StringBuilder input = new StringBuilder();

        String url = "";

        LoginModel model = new LoginModel();
        String Name = "";
        String Password = "";
        Integer SlotCount = 0;
        Integer BikeStatus = 2;

        Path path = Paths.get("src/LoginData");
        try
        {
            Scanner scanner = new Scanner(path);
            Name = scanner.nextLine();
            Password = scanner.nextLine();
            SlotCount = Integer.parseInt(scanner.nextLine());

            scanner.close();
        }
        catch (IOException e)
        {
            Name = "Dock1";
            Password = "Passerword";
            SlotCount = 1;
        }

        model.setUsername(Name);
        model.setPassword(Password);

        System.out.println(Name);
        System.out.println(Password);

        ApiProvider mApiProvider = new ApiProvider();

        mApiProvider.Authenticate(model);

        switch (mApiProvider.Authenticate(model)) {
            case 0:
                System.out.println("User has been authenticated");
                break;
            case -1:
                System.out.println("Please check your connection");
                return;
            case 524:
                System.out.println("Please check username and/or password");
                return;
            default:
                System.out.println("Could not authenticate user");
                return;
        }

        for (int i = 0; i<SlotCount; i++)
        {
            //API Request for positions of bikes in dock
            //url = mApiProvider.BuildUrl("" //Something with Name and i )
            //if(mApiProvider.sendGetRequest(url) == 0)
            if (false)
            {
                System.out.println("Unlocked Slot " + i);
            }
        }

        SerialPort comPort = SerialPort.getCommPort("COM4");
        comPort.openPort();
        comPort.addDataListener(new SerialPortDataListener() {
            @Override
            public int getListeningEvents() { return SerialPort.LISTENING_EVENT_DATA_AVAILABLE; }
            @Override
            public void serialEvent(SerialPortEvent event)
            {
                if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
                    return;

                byte[] newData = new byte[comPort.bytesAvailable()];
                int numRead = comPort.readBytes(newData, newData.length);
                for (int i = 0; i<newData.length; i++)
                {
                    input.append((char)newData[i]);
                }
                if (input.length() == 40)
                {
                    if(input.charAt(1) == '/' && input.charAt(3) == '/' && input.charAt(12) == '-' && input.charAt(17) == '-' && input.charAt(22) == '-' && input.charAt(27) == '-')
                    {
                        if(input.charAt(2) == 'A')
                        {
                            String[] data = input.toString().split("/");

                            //Checks if bike is available, the user can hire a bike, and that a booked bike belongs to them
                            //url1 = mApiProvider.BuildUrl("" //Something with Name and data[0] (SlotNo) )
                            //url2 = mApiProvider.BuildUrl("" //Something with data[2] (UserID) )
                            //url3 = mApiProvider.BuildUrl("" //Something with data[2] (UserID), data[0] (SlotNo), and Name)
                            //if(mApiProvider.sendGetRequest(url1) == 2 && mApiProvider.sendGetRequest(url2) == 2)
                            /*if(BikeStatus == 2)
                            {
                                //API Call to release bike for user with UUID data[2]
                                QuickOrderModel model = new QuickOrderModel(Name, data[0], data[2]);
                                //url = mApiProvider.BuildURL( //Something with Name, data[0] (SlotNo), and data[2] (UserID))
                                //mApiProvider.sendPostRequest(url, model)
                                */System.out.println("Bike Released by user - " + data[2]);/*
                                BikeStatus = 0;
                            } //else if(mApiProvider.sendGetRequest(url1) == 1 && mApiProvider.sendGetRequest(url2) == 2)
                            else if(BikeStatus == 1)
                            {
                                //if(mApiProvider.sendGetRequest(url3) == true)
                                if(false)
                                {
                                    //API Call to release bike for user with UUID data[2]
                                    QuickOrderModel model = new QuickOrderModel(Name, data[0], data[2]);
                                    //url = mApiProvider.BuildURL( //Something with Name, data[0] (SlotNo), and data[2] (UserID))
                                    //mApiProvider.sendPostRequest(url, model)
                                    System.out.println("Bike Released");
                                    BikeStatus = 0;
                                }
                            }*/
                        }
                        else if(input.charAt(2) == 'B')
                        {
                            String[] data = input.toString().split("/");

                            //API Call to return bike with UUID data[1]
                            /*QuickOrderModel model = new QuickOrderModel(Name, data[0], data[2]);
                            //url = mApiProvider.BuildURL( //Something with Name, data[0] (SlotNo), and data[2] (UserID))
                            //mApiProvider.sendPostRequest(url, model)*/
                            System.out.println("Bike Released by user - " + data[2]);
                        }
                    }
                    else
                    {
                        input.delete(0, input.length());
                    }
                }
                else if(input.length() > 40)
                {
                    input.delete(0, input.length());
                }
            }
        });

        while(true){}
    }
}
