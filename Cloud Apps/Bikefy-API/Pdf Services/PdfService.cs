﻿using System;
using CityCyclesAPI.Services;
using IronPdf;

namespace CityCyclesAPI.PdfServices
{
    public class PdfService : IPdfService
    {
        HtmlToPdf Renderer { get; }

        public PdfService()
        {
            Renderer = new HtmlToPdf();
        }

        public byte[] GeneratePdf(string html)
        {
            var result = Renderer.RenderHtmlAsPdf(html);

            // return the binary data
            return result.BinaryData;
        }
    }
}
