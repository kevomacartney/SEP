﻿using System;
namespace CityCyclesAPI.Models
{
    public class ApplicationAccountModel
    {
        public string ApplicationName { get; set; }

        public Guid ApplicationId { get; set; }

        public string Password { get; set; }
    }
}
