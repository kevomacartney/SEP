﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CityCycles.Composite.Data.ManagementData;
using CityCycles.Composite.Model.Bike;
using CityCycles.Composite.Repository;
using CityCyclesAPI.Api;
using CityCyclesAPI.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CityCyclesAPI.Controllers
{
    [Authorize]
    [Produces("application/json"), Consumes("application/json")]
    [Route("api/[controller]/[action]")]
    public class BikesController : Controller
    {
        public static class LogEvents
        {
            public static readonly EventId CreatingNewBike = new EventId(1, nameof(CreateNew));
            public static readonly EventId DeletingBike = new EventId(2, nameof(Delete));
        }
        /// <summary>
        /// The bike repository.
        /// </summary>
        private readonly IBikeRepository _bikeRepository;

        /// <summary>
        /// The dock repository.
        /// </summary>
        private readonly IDockRepository _dockRepository;

        private readonly ILogger _logger;

        public BikesController(IServiceProvider serviceProvider)
        {
            _bikeRepository = serviceProvider.GetService<IBikeRepository>();
            _dockRepository = serviceProvider.GetService<IDockRepository>();

            _logger = serviceProvider.GetService<ILogger<BikesController>>();
        }

        // GET: api/Bike/FindById
        [HttpGet("{id}")]
        public async Task<JsonResult> FindById([FromRoute(Name = "id")]Guid Id)
        {
            var bike = await _bikeRepository.GetByIdAsync(Id);

            return Json(bike);
        }

        // GET: api/Bike/FindAll
        [HttpGet]
        public async Task<JsonResult> FindAll()
        {
            var bikes = new List<BikeModel>();

            // Get all bikes
            (await _bikeRepository.GetAllBikes()).ForEach(b =>
            {
                bikes.Add(new BikeModel()
                {
                    BikeId = b.Id,
                    DockId = b.DockId,
                    Status = b.Status
                });
            });

            return Json(bikes);
        }

        // POST: api/Bike/CreateNew
        [HttpPost]
        public async Task<IActionResult> CreateNew([FromBody]BikeRequest request)
        {
            var dock = await _dockRepository.GetByIdAsync(request.DockId);

            // If dock was not found
            if (dock == null)
            {
                _logger.LogWarning(LogEvents.CreatingNewBike, "Could not create bike, Dock with ID {dockId} was not found", request.DockId);
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityNotFound));
            }

            if (dock.Bikes.Count >= dock.MaximumCapacity)
            {
                _logger.LogWarning(LogEvents.CreatingNewBike, "Could add bike to dock with ID {dockId}, dock at maximum capacity", request.DockId);
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.DockAtMaxCapacity));
            }

            var b = new Bike()
            {
                Id = request.BikeId == Guid.Empty ? Guid.NewGuid() : request.BikeId,
                Status = request.Status,
                LastReport = DateTime.UtcNow
            };

            // Add the bike to the dock
            dock.Bikes.Add(b);

            // Update the dock to include the new bike
            await _dockRepository.UpdateAsync(dock);

            _logger.LogInformation(LogEvents.CreatingNewBike, "Bike with ID {bikeid} was added to the dock with ID {dockId}", request.BikeId, request.DockId);

            return Json(new Dictionary<string, object>() { ["BikeId"] = b.Id });
        }

        // GET: api/Bike/Delete
        [HttpGet("{id}")]
        public async Task<IActionResult> Delete([FromRoute(Name = "id")]Guid bikeId)
        {
            // Validate he ID
            if (bikeId == Guid.Empty)
            {
                _logger.LogWarning(LogEvents.DeletingBike, "Bike not delete, Invalid bike id was passed}", bikeId);
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidModel));
            }


            var bike = await _bikeRepository.GetByIdAsync(bikeId);

            // Bike not foind
            if (bike == null)
            {
                _logger.LogWarning(LogEvents.DeletingBike, "Bike not delete, could not find a bike with the id {bikeId}", bikeId);
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityNotFound));
            }

            // Drop the entity
            await _bikeRepository.DeleteAsync(bike);

            _logger.LogInformation(LogEvents.DeletingBike, "Deleted bike with ID {bikeId}", bikeId);
            return Ok();
        }
    }
}
