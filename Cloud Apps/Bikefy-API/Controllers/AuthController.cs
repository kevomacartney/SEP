using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CityCycles.Api.Managers;
using CityCycles.Api.Services;
using CityCycles.Composite.Model.Auth;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;
using System.IdentityModel.Tokens.Jwt;
using CityCyclesAPI.Extensions;
using System.Net;
using CityCyclesAPI.Models;
using CityCycles.Composite.Data.ManagementData.User;
using CityCycles.Composite.Data;
using CityCycles.Composite.Shared_Services;
using CityCyclesAPI.Api;
using NServiceBus;
using CityCycles.Composite.BusMessages.API;
using CityCycles.Composite.BusMessages.Management;
using CityCycles.Composite.Model.Account;
using Braintree;
using System.Text.RegularExpressions;
using System.Security.Claims;

namespace CityCycles.Api.Controllers
{
    /// <summary>
    /// Authentication controller class
    /// </summary>
    [Produces("text/plain")]
    [Route("api/[controller]/[action]")]
    public class AuthController : Controller
    {
        public static class LogEvents
        {
            public static readonly EventId Auth = new EventId(4000, nameof(Authenticate));
            public static readonly EventId CreatingUser = new EventId(4001, nameof(CreateUser));
        }

        /// <summary>
        /// The service provider.
        /// </summary>
        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// The client manager.
        /// </summary>
        private readonly IClientManager _clientManager;
        private readonly IBraintreeGateway _braintree;

        /// <summary>
        /// The auth token manager.
        /// </summary>
        private readonly AuthTokenManager _authTokenManager;

        /// <summary>
        /// Gets or sets the logger
        /// </summary>
        private readonly ILogger _logger;

        private readonly IEndpointInstance _endpointInstance;

        public AuthController(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;

            _logger = _serviceProvider.GetService<ILogger<AuthController>>();

            // Resolve the client manager from the services
            _clientManager = _serviceProvider.GetService<IClientManager>();

            _braintree = _serviceProvider.GetService<IBraintreeGateway>();

            // Resolve the aurthorisation token manager
            _authTokenManager = _serviceProvider.GetService<AuthTokenManager>();

            _endpointInstance = _serviceProvider.GetService<IEndpointInstance>();
        }

        // POST api/Auth/Authenticate
        [HttpPost, AllowAnonymous]
        public async Task<IActionResult> Authenticate([FromBody]AuthenticateModel model)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidModel), new SerializableError(ModelState));
            }

            // Check user's password
            var result = await _clientManager.PasswordSigninAsync(model.Username, model.Password);

            _logger.LogInformation(LogEvents.Auth, $"Requested a log in for user '{model.Username}' with result '{result}'");


            if (result == SignInResult.Success)
            {
                // Get the user entity
                var user = await _clientManager.GetClientByUsernameAsync(model.Username);

                // Return the authorization token to the client
                var s = await _authTokenManager.CreateBearerTokenAsync(user, "CityCycles.Web");

                return Ok(s);
            }

            if (result == SignInResult.LockedOut)
                // Client not authentised, the failed 
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.AccountLockedOut));

            if (result == SignInResult.NotAllowed)
                // Client not authentised, the failed 
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.AccountDisabled));

            // Client not authentised, the failed 
            return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidLogin), "Please check your username or password");
        }

        // POST api/Auth/AuthenticateUserApp
        [HttpPost, AllowAnonymous]
        public async Task<IActionResult> AuthenticateUserApp([FromBody]AuthenticateModel model)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidModel));
            }

            var user = await _clientManager.GetClientByUsernameAsync(model.Username);

            if (user == null)
            {
                _logger.LogWarning(LogEvents.Auth, "User with email {email} could not be found", model.Username);
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidLogin));
            }

            // Sign in the user
            var result = await _clientManager.PasswordSigninAsync(model.Username, model.Password);
            _logger.LogInformation(LogEvents.Auth, $"App log in requested for '{model.Username}' with result '{result}'");

            // If the application wasn't signed in, then return the result
            if (result != SignInResult.Success)
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidLogin));

            // Return token for the application
            return Ok(await _authTokenManager.CreateBearerTokenAsync(user, "CityCycles.Api"));
        }

        // POST api/Auth/AuthenticateApp
        [HttpPost, AllowAnonymous]
        public async Task<IActionResult> AuthenticateApp([FromBody]ApplicationAuthenticateModel model)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidModel), new SerializableError(ModelState));
            }

            var result = await _clientManager.ApplicationSignInAsync(model.AppId, model.AccessKey);

            _logger.LogInformation(LogEvents.Auth, $"Application log in requested for '{model.AppId}' with result '{result}'");

            // If the application wasn't signed in, then return the result
            if (result != SignInResult.Success)
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidLogin));

            var user = await _clientManager.GetClientByIdAsync(model.AppId);

            // Return token for the application
            return Ok(await _authTokenManager.CreateBearerTokenAsync(user, "CityCycles.Api"));
        }

        // Post api/Auth/CreateUser
        [HttpPost]
        [Produces("application/json")]
        public async Task<IActionResult> CreateUser([FromBody]CreateAccountModel model)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidModel), new SerializableError(ModelState));
            }

            try
            {
                var userId = await _clientManager.CreateNewUserAsync(model);

                // Place the user registered on the bus
                await _endpointInstance.SendLocal(new UserRegistered()
                {
                    UserId = userId
                });

                // Return the user Id
                return Json(new Dictionary<string, Guid>() { ["UserId"] = userId });
            }
            catch (InvalidOperationException e)
            {
                _logger.LogError(LogEvents.CreatingUser, e, "Could not create user");

                // Return bad request
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EmailTaken));
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> NamesById(Guid id)
        {
            // Check the Id
            if (id == Guid.Empty)
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityIdNotProvided));

            //get the user
            var user = await _clientManager.GetClientByIdAsync(id);

            if (user == null)
            {
                return Json(new Dictionary<string, string>
                {
                    ["FirstName"] = "Unknown",
                    ["LastName"] = "Unknown"
                });
            }

            return Json(new Dictionary<string, string>
            {
                ["FirstName"] = user.FirstName,
                ["LastName"] = user.LastName
            });
        }

        [HttpGet("{id}")]
        [Produces("application/json")]
        public async Task<IActionResult> Profile([FromRoute(Name = "id")]Guid userId)
        {
            // Check the user id is vali
            if (userId == Guid.Empty)
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityNotFound));

            // Get the user with the given id
            var user = await _clientManager.GetClientByIdAsync(userId);
            var customer = await _braintree.Customer.FindAsync(userId.ToString());
            var address = customer.Addresses[0];

            var profile = new Profile()
            {
                UserId = userId,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                City = address.Locality,
                PostCode = address.PostalCode,
                StreetAddress = address.StreetAddress
            };

            return Json(profile);
        }

        // GET api/Auth/Me
        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> Me()
        {
            var id = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            // Get my profile
            var user = await _clientManager.GetClientByIdAsync(id);
            var customer = await _braintree.Customer.FindAsync(id.ToString());

            var address = customer.Addresses[0];

            var profile = new Profile()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                UserId = id,
                StreetAddress = address.StreetAddress,
                City = address.Locality,
                PostCode = address.PostalCode,
                Email = user.Email
            };

            return Json(profile);
        }

        [HttpPost]
        [Produces("application/json")]
        public async Task<IActionResult> Me([FromBody]Profile profile)
        {
            var id = Guid.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            // Get my profile
            var user = await _clientManager.GetClientByIdAsync(id);
            var customer = await _braintree.Customer.FindAsync(id.ToString());

            var address = customer.Addresses[0];

            // email regex
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            // Check the email address matches regex
            if (!regex.Match(profile.Email).Success)
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidModel));

            // Update user
            user.Email = profile.Email;
            user.FirstName = profile.FirstName;
            user.LastName = profile.LastName;

            var request = new AddressRequest()
            {
                Locality = profile.City,
                PostalCode = profile.PostCode,
                StreetAddress = profile.StreetAddress
            };
            // Update the user's address
            await _braintree.Address.UpdateAsync(user.Id.ToString(), address.Id, request);

            // Update the user account
            await _clientManager.UpdateAsync(user);
            return Ok();
        }


        [HttpPost("{id}")]
        [Produces("application/json")]
        public async Task<IActionResult> UpdateProfile([FromRoute(Name = "id")]Guid userId, [FromBody]Profile profile)
        {
            // Check the user id is valid
            if (userId == Guid.Empty)
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityNotFound));

            // Get the user with the given id
            var user = await _clientManager.GetClientByIdAsync(userId);
            var customer = await _braintree.Customer.FindAsync(userId.ToString());
            var address = customer.Addresses[0];

            // Make sure that the user has a valid email
            if (string.IsNullOrEmpty(profile.Email))
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidModel));

            // email regex
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            // Check the email address matches regex
            if (!regex.Match(profile.Email).Success)
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidModel));

            // Update user
            user.Email = profile.Email;
            user.FirstName = profile.FirstName;
            user.LastName = profile.LastName;

            var request = new AddressRequest()
            {
                Locality = profile.City,
                PostalCode = profile.PostCode,
                StreetAddress = profile.StreetAddress
            };

            // Update the user's address
            await _braintree.Address.UpdateAsync(user.Id.ToString(), address.Id, request);

            // Update the user account
            await _clientManager.UpdateAsync(user);
            return Ok();
        }

        [AllowAnonymous]
        [Produces("application/json")]
        public async Task<IActionResult> CreateApplication([FromBody]ApplicationAccountModel model)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidModel), new SerializableError(ModelState));
            }

            var user = new User()
            {
                Id = model.ApplicationId == Guid.Empty ? Guid.NewGuid() : model.ApplicationId,
                FirstName = model.ApplicationName,
                PasswordHash = new PasswordHasher(_serviceProvider).HashPassword(model.Password),
                SecurityStamp = Guid.NewGuid().ToString()
            };

            var context = _serviceProvider.GetService<ManagementDataContext>();
            await context.AddAsync(user);
            await context.SaveChangesAsync();

            var encryption = _serviceProvider.GetService<EncryptionService>();
            var hashed = encryption.Encrypt(model.Password);

            return Ok($"'{hashed}' {user.Id}");
        }
    }
}
