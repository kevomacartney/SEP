﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using CityCycles.Composite.Data.ManagementData;
using CityCycles.Composite.Model.Pricing;
using CityCycles.Composite.Repository;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using CityCyclesAPI.Extensions;
using Microsoft.EntityFrameworkCore;
using CityCyclesAPI.Api;
using Microsoft.Extensions.Configuration;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CityCyclesAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class PricingController : Controller
    {
        private readonly IPricingRepository _pricingRepo;
        private readonly IConfiguration _configuration;

        public PricingController(IServiceProvider serviceProvider)
        {
            _pricingRepo = serviceProvider.GetService<IPricingRepository>();
            _configuration = serviceProvider.GetService<IConfiguration>();
        }

        // POST: api/Pricing/CreateNew
        [HttpPost]
        public async Task<IActionResult> CreateNew([FromBody]PricingRequest request)
        {
            if (!ModelState.IsValid)
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidModel));

            var pricing = new Pricing()
            {
                Id = request.Id == Guid.Empty ? Guid.NewGuid() : request.Id,
                BookingFees = request.BookingFee,
                Label = request.Label,
                MissedBookingFees = request.BookingFee,
                HourlyPrice = request.Hourly,
                OverdueHourlyPrice = request.OverdueHourly,
                UpdateAt = DateTime.UtcNow,
                Updatedby = request.UpdatedBy
            };

            // Add the pricing entity to the database
            await _pricingRepo.AddAsync(pricing);

            return Json(new Dictionary<string, object>() { ["PricingId"] = pricing.Id });
        }

        // GET: api/Pricing/Update
        [HttpGet("{id}")]
        public async Task<IActionResult> Delete([FromRoute(Name = "id")]Guid pricingId)
        {
            if (pricingId == Guid.Empty)
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityIdNotProvided));

            var pricing = await _pricingRepo.GetByIdAsync(pricingId);

            if (pricing == null)
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityNotFound));

            // Delete the entity
            await _pricingRepo.DeleteAsync(pricing);
           return Ok();
        }


        // POST: api/Pricing/Update
        [HttpPost]
        public async Task<IActionResult> Update([FromBody]PricingRequest request)
        {
            if (request.Id == Guid.Empty)
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityIdNotProvided));

            // Get the pricing entity for the request
            var pricing = await _pricingRepo.GetByIdAsync(request.Id);

            if (pricing == null)
            {
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityNotFound));
            }

            // Update the entity
            pricing.BookingFees = request.BookingFee;
            pricing.HourlyPrice = request.Hourly;
            pricing.MissedBookingFees = request.MissedBookingFee;
            pricing.OverdueHourlyPrice = request.OverdueHourly;
            pricing.UpdateAt = DateTime.UtcNow;
            pricing.Updatedby = request.UpdatedBy;
            pricing.Label = request.Label;
            pricing.DiscountHourlyPrice = request.DiscountRate;

            // Update the entity
            await _pricingRepo.UpdateAsync(pricing);

            return Json(pricing);
        }

        // GET: api/Pricing/Update
        [HttpGet("{id}")]
        public async Task<IActionResult> FindById([FromRoute(Name = "id")]Guid pricing)
        {
            if (pricing == Guid.Empty)
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityIdNotProvided));

            return Json(await _pricingRepo.GetByIdAsync(pricing));
        }

        // GET: api/Pricing/Update
        [HttpGet]
        public async Task<IActionResult> FindAll()
        {
            return Json(await _pricingRepo.LocalContext.Pricings.ToListAsync());
        }

        // GET: api/Pricing/Default
        [HttpGet]
        public async Task<IActionResult> Default()
        {
            // get the default pricing model from the configurations
            var defaultPricing = _configuration.GetValue<Guid>("Pricing:Default");

            // Get entity and check if the entity exist, returning EntityNotFound if it's the case
            var pricing = await _pricingRepo.GetByIdAsync(defaultPricing);
            if (pricing == null)
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityNotFound));

            // entity was found, return it as a json
            return Json(pricing);
        }
    }

}
