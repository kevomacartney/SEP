﻿using CityCycles.Composite.Data.ManagementData;
using CityCycles.Composite.Model.Bike;
using CityCycles.Composite.Model.Dock;
using CityCycles.Composite.Repository;
using CityCyclesAPI.Api;
using CityCyclesAPI.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CityCyclesAPI.Controllers
{
    [Authorize]
    [Produces("application/json"), Consumes("application/json")]
    [Route("api/[controller]/[action]")]
    public class DockController : Controller
    {
        public static class LogEvents
        {
            public static readonly EventId FindingAll = new EventId(1, nameof(FindAll));
            public static readonly EventId CreatingNewDock = new EventId(2, nameof(CreateNew));
            public static readonly EventId UpdatingDock = new EventId(3, nameof(Update));
            public static readonly EventId DeletingDock = new EventId(4, nameof(Delete));
        }

        /// <summary>
        /// The dock repository.
        /// </summary>
        private readonly IDockRepository _dockRepository;
        private readonly IBikeRepository _bikeRepository;

        private readonly ILogger _logger;

        public DockController(IServiceProvider serviceProvider)
        {
            _dockRepository = serviceProvider.GetService<IDockRepository>();
            _bikeRepository = serviceProvider.GetService<IBikeRepository>();

            _logger = serviceProvider.GetService<ILogger<DockController>>();
        }

        // GET: api/Dock/FindById
        [HttpGet("{id}")]
        public async Task<JsonResult> FindById(Guid id)
        {
            // Get the dock by id 
            var dock = await _dockRepository.GetByIdAsync(id);

            return Json(dock);
        }

        // GET: api/Dock/FindAll
        [HttpGet]
        public async Task<JsonResult> FindAll()
        {
            var docks = new List<DockStatus>();

            // Get all the docks and create a collection of statuses
            (await _dockRepository.GetAllDocks()).ForEach(d =>
            {
                var bikes = new HashSet<BikeModel>();

                // Iterate over all the bikes for this dock
                foreach (var bike in d.Bikes)
                {
                    // Add the current bike to the dock model
                    bikes.Add(new BikeModel()
                    {
                        BikeId = bike.Id,
                        Status = bike.Status,
                        DockId = bike.DockId,
                        LastUpdate = bike.LastReport
                    });
                }

                // Add the current dock dock with collection 
                docks.Add(new DockStatus()
                {
                    Bikes = bikes,
                    DockId = d.Id,
                    DockName = d.Name,
                    Location = d.Location,
                    MaximumCapacity = d.MaximumCapacity
                });
            });

            _logger.LogInformation(LogEvents.FindingAll, "Request for all docks found {dockCount} docks", docks.Count);

            // Return the object as a json
            return Json(docks);
        }

        // POST: api/Dock/CreateNew
        [HttpPost]
        public async Task<IActionResult> CreateNew([FromBody]DockRequest request)
        {
            _logger.LogInformation(LogEvents.CreatingNewDock, "Request to create new dock with name {dockName}, and maximum capacity of {capacity}", request.Name, request.Capacity);

            if (request.Id != Guid.Empty)
            {
                var exists = await _dockRepository.GetByIdAsync(request.Id);

                // If dock exists, then return bad request
                if (exists != null)
                {
                    _logger.LogWarning(LogEvents.CreatingNewDock, "Could not create new dock because dock ID {dockId} is already taken", request.Id);
                    return StatusCode(this.StatusCodeToInt(ApiStatusCodes.DuplicateEntity));
                }
            }

            var dock = new Dock()
            {
                Id = request.Id == Guid.Empty ? Guid.NewGuid() : request.Id,
                Location = $"{request.Latitude} {request.Longitude}",
                Name = request.Name,
                MaximumCapacity = request.Capacity
            };

            // Create the dock
            await _dockRepository.AddAsync(dock);

            _logger.LogInformation(LogEvents.CreatingNewDock, "Successfully created dock {dockName} with ID {dockId}", request.Name, request.Id);

            // Return Dock Id
            return Json(new Dictionary<string, object>() { ["dockId"] = dock.Id });
        }

        // POST: api/Dock/Update
        [HttpPost]
        public async Task<IActionResult> Update([FromBody]DockRequest request)
        {
            if (request.Id == Guid.Empty)
            {
                _logger.LogWarning(LogEvents.UpdatingDock, "Could not update dock because no ID was specified");
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityIdNotProvided));
            }

            var dock = await _dockRepository.GetByIdAsync(request.Id);

            if (dock == null)
            {
                _logger.LogWarning(LogEvents.UpdatingDock, "Could  update dock because a dock with ID {dockId} was not found", request.Id);
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityNotFound));
            }

            _logger.LogInformation(LogEvents.UpdatingDock, "Dock with dock ID {dockId} was found", request.Id);

            dock.Location = $"{request.Latitude} {request.Longitude}";
            dock.Name = request.Name;

            // Write the update to the database
            await _dockRepository.UpdateAsync(dock);

            _logger.LogInformation(LogEvents.UpdatingDock, "Dock with dock ID {dockId} was successfully updated", request.Id);

            return Json(dock);
        }

        // GET: api/Dock/Delete
        [HttpGet("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            if (id == Guid.Empty)
            {
                _logger.LogWarning(LogEvents.DeletingDock, "Could not delete a dock because the ID was not specified");
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityIdNotProvided));
            }

            var dock = await _dockRepository.GetByIdAsync(id);

            if (dock == null)
            {
                _logger.LogWarning(LogEvents.DeletingDock, "Could not delete a dock because a dock with ID {dockId} was not found", id);
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityNotFound));
            }

            _logger.LogInformation(LogEvents.DeletingDock, "Found a dock with ID {dockId} which has {bikeCount} bikes", dock.Id, dock.Bikes.Count);

            // Get all bikes as a list
            var bikes = dock.Bikes.ToList();

            for (var i = 0; i < dock.Bikes.Count; i++)
            {
                // Delete the bike
                await _bikeRepository.DeleteByIdAsync(bikes[i].Id);
                _logger.LogInformation(LogEvents.DeletingDock, "Deleted bike {bikeId} for dock {dockId}", bikes[i], dock.Id);
            }

            // Delete the dock
            await _dockRepository.DeleteAsync(dock);

            _logger.LogInformation(LogEvents.DeletingDock, "Successfully deleted Dock {dockId}", dock.Id);
            return Ok();
        }

    }
}
