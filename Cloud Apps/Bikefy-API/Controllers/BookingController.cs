﻿using System;
using CityCycles.Composite.Model.Booking;
using Microsoft.AspNetCore.Mvc;
using CityCyclesAPI.Api;
using CityCyclesAPI.Extensions;
using CityCycles.Composite.Data.ManagementData;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using CityCycles.Composite.Repository;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using NServiceBus;
using CityCycles.Composite.BusMessages.Management;
using CityCycles.Composite.Model.Dock;
using CityCycles.Composite.Shared_Services;
using Newtonsoft.Json;
using CityCycles.Api.Services;
using Braintree;
using QRCoder;
using System.Drawing;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace CityCycles.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class BookingController : Controller
    {
        public static class LogEvents
        {
            public static readonly EventId FindingAllUserBookings = new EventId(1, nameof(FindAllUserBookings));
            public static readonly EventId FindingAllBookings = new EventId(2, nameof(FindAll));
            public static readonly EventId FindBooking = new EventId(3, nameof(FindById));
            public static readonly EventId CreateNewBooking = new EventId(4, nameof(CreateNew));
            public static readonly EventId CreateNewDockBooking = new EventId(4, nameof(CreateNewDockBooking));
        }

        private readonly IConfiguration _configuration;

        private readonly IDockRepository _dockRepo;
        private readonly IBikeRepository _bikeRepo;
        private readonly IBookingRepository _bookingRepo;
        private readonly IPricingRepository _pricingRepo;

        private readonly IEndpointInstance _endpointInstance;

        private readonly EncryptionService _encryptionService;
        private readonly IClientManager _clientManager;
        private readonly IBraintreeGateway _braintree;

        private readonly ILogger _logger;

        public BookingController(IServiceProvider serviceProvider)
        {
            _configuration = serviceProvider.GetService<IConfiguration>();

            _dockRepo = serviceProvider.GetService<IDockRepository>();
            _bookingRepo = serviceProvider.GetService<IBookingRepository>();
            _pricingRepo = serviceProvider.GetService<IPricingRepository>();
            _bikeRepo = serviceProvider.GetService<IBikeRepository>();
            _endpointInstance = serviceProvider.GetService<IEndpointInstance>();

            _braintree = serviceProvider.GetService<IBraintreeGateway>();
            _clientManager = serviceProvider.GetService<IClientManager>();
            _logger = serviceProvider.GetService<ILogger<BookingController>>();
            _encryptionService = serviceProvider.GetService<EncryptionService>();
        }

        // GET: api/Booking/FindAllUserBookings/{userId}
        [HttpGet("{id}")]
        public async Task<IActionResult> FindAllUserBookings([FromRoute(Name = "id")]Guid userId)
        {
            // Get all the bookings for a user
            var bookings = await _bookingRepo.GetAllBookingsByUserAsync(userId);

            _logger.LogInformation(LogEvents.FindingAllUserBookings, "Request to find all bookings for user {userId} found {bookingsCount}", userId, bookings.Count);

            return Json(bookings);
        }

        // GET: api/Booking/FindAll
        [HttpGet]
        public async Task<IActionResult> FindAll()
        {
            // Get all the booking
            var bookings = await _bookingRepo.GetAllBookingsAsync();

            _logger.LogInformation(LogEvents.FindingAllUserBookings, "Request to find all bookings found {bookingsCount}", bookings.Count);

            return Json(bookings);
        }

        // GET: api/Booking/FindById
        [HttpGet("{id}")]
        public async Task<IActionResult> FindById([FromRoute]Guid id)
        {
            if (id == Guid.Empty)
            {
                _logger.LogWarning(LogEvents.FindBooking, "Could not find a booking because no booking id was provided");

                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityIdNotProvided));
            }

            // Get the booking entity
            var booking = await _bookingRepo.GetByIdAsync(id);

            // Check of the booking exists
            if (booking == null)
            {
                _logger.LogWarning(LogEvents.FindBooking, "Could not find a booking with ID {bookingId}", id);
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityNotFound));
            }

            _logger.LogInformation(LogEvents.FindBooking, "Successfully found booking with ID {bookingId}", booking.Id);

            return Json(booking);
        }

        // POST: api/Booking/CreateNew
        [HttpPost]
        public async Task<IActionResult> CreateNew([FromBody]BookingRequest request)
        {
            // Check if the model is valid
            if (!ModelState.IsValid)
            {
                _logger.LogWarning(LogEvents.CreateNewBooking, "Could not create new booking, Model not valid");
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidModel));
            }

            Guid pricingId;

            if (request.Pricing == Guid.Empty)
            {
                pricingId = _configuration.GetValue<Guid>("Pricing:Default");

                _logger.LogInformation(LogEvents.CreateNewBooking, "Creating a boooking using default pricing {pricingId}", pricingId);
            }
            else
            {
                pricingId = request.Pricing;
                _logger.LogInformation(LogEvents.CreateNewBooking, "Creating a boooking using a specified pricing {pricingId}", pricingId);
            }

            var dock = await _dockRepo.GetByIdAsync(request.DockId);

            // Check that the dock exists
            if (dock == null)
            {
                _logger.LogWarning(LogEvents.CreateNewBooking, "Could not find dock with ID {dockId}", request.DockId);
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.EntityNotFound));
            }

            Bike bike = null;

            dock.Bikes.ToList().ForEach(b =>
            {
                // Get all active bookings for the specified date range
                var active = _bikeRepo.GetAllActiveBookings(b.Id, request.StartDate, request.EndDate).Result;

                // If bike has active bookings for the given range
                if (!active.Any())
                {
                    bike = b;
                    return;
                }
            });

            // If we couldn't find a bike
            if (bike == null)
            {
                _logger.LogInformation(LogEvents.CreateNewBooking, "Could not find an available bike in the dock with ID {dockId}", request.DockId);

                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.BikeNotAvaiable));
            }

            // Check that the dock has a free bike
            if (bike == null)
            {
                _logger.LogWarning(LogEvents.CreateNewBooking, "Could not find an available bike in the dock with ID {dockId}", request.DockId);
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.NoAvaiableBikeInDock));
            }

            var pricing = await _pricingRepo.GetByIdAsync(pricingId);

            if (pricing == null)
            {
                _logger.LogWarning(LogEvents.CreateNewBooking, "Could not find pricing scheme with the Id {pricingId}", pricingId);
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.PricingNotFound));
            }

            // Create the booking entity;
            var booking = new Booking()
            {
                Id = Guid.NewGuid(),
                CreatedOn = DateTime.UtcNow,
                StartDate = request.StartDate,
                EndDate = request.EndDate,
                PricingId = pricingId,
                UserId = request.UserId,
                BikeId = bike.Id,
                BookingUsed = false,
                IsDockBooking = false
            };

            // add the booking tot he database
            await _bookingRepo.AddAsync(booking);

            _logger.LogInformation(LogEvents.CreateNewBooking, "Created new booking {bookingId} for user {userId}", booking.Id, request.UserId);

            // Return the booking Id
            return Json(new Dictionary<string, Guid>()
            {
                ["BookingId"] = booking.Id
            });
        }

        [HttpGet]
        public IActionResult CreateSign([FromBody]Object payload)
        {
            var str = JsonConvert.SerializeObject(payload);
            var sig = _encryptionService.Sign(str);

            return Ok(sig);
        }

        // POST: api/Booking/NewDockBooking/{dockId}
        [HttpPost("{id}")]
        public async Task<IActionResult> NewDockBooking([FromRoute(Name = "id")]Guid dockId, [FromBody]DockBookingToken bookingToken)
        {
            // verify the integrity of the message
            if (!_encryptionService.VerifySignature(bookingToken.Signature, bookingToken.Payload))
            {
#if DEBUG
                _logger.LogDebug(LogEvents.CreateNewDockBooking, "Could not verify signature {signature} for payload {payload}", bookingToken.Signature, bookingToken.Payload);
#else 
                _logger.LogDebug(LogEvents.CreateNewDockBooking, "Could not verify signature for booking token requested by dock {dockId}", dockId);
#endif

                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.DockBookingTokenInvalid));
            }

            #region Validate Event

            // deserialize the payload to booking event 
            var bookingEvent = JsonConvert.DeserializeObject<DockBookingEvent>(bookingToken.Payload);

            // Get the user with the given ID
            var user = await _clientManager.GetClientByIdAsync(bookingEvent.UserId);

            if (user == null)
            {
                _logger.LogWarning(LogEvents.CreateNewDockBooking, "User with id {userId} could not be found", bookingEvent.UserId);
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.UserNotFound));
            }

            // Get the dock with the given Id
            var dock = await _dockRepo.GetByIdAsync(dockId);

            if (dock == null)
            {
                _logger.LogWarning(LogEvents.CreateNewDockBooking, "Dock with id {dockId} could not be found", dockId);
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.DockNotFound));
            }

            // Find the bike in the dock
            if (!dock.Bikes.Any(b => b.Id == bookingEvent.BikeId))
            {
                _logger.LogWarning(LogEvents.CreateNewDockBooking, "Bike with ID {bikeId} was not found in dock with ID {dockId}", bookingEvent.BikeId, dockId);
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.BikeNotAvaiable));
            }

            // Check if the token has expired
            if ((DateTime.UtcNow - bookingEvent.CreatedOn) > TimeSpan.FromMinutes(15))
            {
                _logger.LogWarning(LogEvents.CreateNewDockBooking, "The dock booking for user {userId} was created on {bookingCreatedDate}, it older than 15 minutes and has expired.",
                     bookingEvent.UserId, bookingEvent.CreatedOn);

                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.BookingExpired));
            }

            // Check the security token is valid
            if (!user.SecurityStamp.Equals(bookingEvent.SecurityToken, StringComparison.InvariantCulture))
            {
                _logger.LogWarning(LogEvents.CreateNewBooking, "The security token {securityToken} provided for user {userId} was invalid.", bookingEvent.SecurityToken, bookingEvent.UserId);

                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidSecurityToken));
            }


            // Update the client's security token
            await _clientManager.UpdateSecurityTokenAsync(bookingEvent.UserId);

            #endregion

            var activeBookings = await _bookingRepo.GetActiveUserDockBooking(bookingEvent.UserId);


            if (activeBookings.Any())
            {
                _logger.LogWarning(LogEvents.CreateNewDockBooking, "User {userId} cannot make booking because they have {bookingCount} active bookings", bookingEvent.UserId, activeBookings.Count);

                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.UserHasActiveBooking));
            }

            // Get the customer from braintreee
            var customer = await _braintree.Customer.FindAsync(bookingEvent.UserId.ToString());

            if (customer.DefaultPaymentMethod == null)
            {
                _logger.LogWarning(LogEvents.CreateNewDockBooking, "User {userId} cannot make booking they don't have a default credit card", bookingEvent.UserId, activeBookings.Count);

                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.UserHasNoDefaultCard));
            }
            // Create the booking
            Guid pricingId = _configuration.GetValue<Guid>("Pricing:Default");

            _logger.LogInformation(LogEvents.CreateNewBooking, "Creating a boooking using default pricing {pricingId}", pricingId);

            // Create the booking entity;
            var booking = new Booking()
            {
                Id = Guid.NewGuid(),
                StartDate = bookingEvent.CreatedOn,
                CreatedOn = DateTime.UtcNow,
                PricingId = pricingId,
                UserId = bookingEvent.UserId,
                BikeId = bookingEvent.BikeId,
                BookingUsed = false,
                IsDockBooking = true
            };

            // Add the booking
            await _bookingRepo.AddAsync(booking);

            // Update the bike satus
            var bike = await _bikeRepo.GetByIdAsync(bookingEvent.BikeId);
            bike.Status = Composite.Enums.BikeStatus.Busy; // Bike is busy
            bike.LastReport = DateTime.UtcNow;

            // Update the booking
            await _bikeRepo.UpdateAsync(bike);

            _logger.LogInformation(LogEvents.CreateNewBooking, "Successfully created a dock booking with id {bookingId} for user {userId}", booking.Id, booking.UserId);

            return Ok();
        }

        // POST: api/Booking/CloseDockBooking/{dockId}
        [HttpPost("{id}")]
        public async Task<IActionResult> CloseDockBooking([FromRoute(Name = "id")]Guid dockId, [FromBody]DockBookingToken bookingToken)
        {
            // verify the integrity of the message
            if (!_encryptionService.VerifySignature(bookingToken.Signature, bookingToken.Payload))
            {
#if DEBUG
                _logger.LogDebug(LogEvents.CreateNewDockBooking, "Could not verify signature {signature} for payload {payload}", bookingToken.Signature, bookingToken.Payload);
#else 
                _logger.LogDebug(LogEvents.CreateNewDockBooking, "Could not verify signature for booking token requested by dock {dockId}", dockId);
#endif

                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.DockBookingTokenInvalid));
            }


            // deserialize the payload to booking event 
            var bookingEvent = JsonConvert.DeserializeObject<DockCloseBookingEvent>(bookingToken.Payload);

            // Get the booking for the bike and user
            var booking = await _bikeRepo.GetActiveDockBooking(bookingEvent.BikeId);

            if (booking == null)
            {
                _logger.LogWarning(LogEvents.CreateNewDockBooking, "Could not find an active booking for bike {bikeId}", bookingEvent.BikeId);
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.BookingNotFound));
            }


            // Get the user with the given ID
            var user = await _clientManager.GetClientByIdAsync(booking.UserId);

            if (user == null)
            {
                _logger.LogWarning(LogEvents.CreateNewDockBooking, "User with id {userId} could not be found", booking.UserId);
                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.UserNotFound));
            }
            // Check the security token is valid
            if (!user.SecurityStamp.Equals(bookingEvent.SecurityToken, StringComparison.InvariantCulture))
            {
                _logger.LogWarning(LogEvents.CreateNewBooking, "The security token {securityToken} provided for user {userId} was invalid.", bookingEvent.SecurityToken, booking.UserId);

                return StatusCode(this.StatusCodeToInt(ApiStatusCodes.InvalidSecurityToken));
            }

            await _clientManager.UpdateSecurityTokenAsync(booking.UserId);

            // Update the booking to include the closing time
            booking.EndDate = bookingEvent.ClosedOn;
            booking.BookingUsed = true; // Make booking as closed

            await _bookingRepo.UpdateAsync(booking);

            // Get the bike
            var bike = await _bikeRepo.GetByIdAsync(bookingEvent.BikeId);

            // Update the bike to move it to the new dock
            bike.DockId = dockId;
            bike.LastReport = DateTime.UtcNow;
            bike.Status = Composite.Enums.BikeStatus.Available; // Bike is returned and is avaiable

            await _bikeRepo.UpdateAsync(bike);

            // Get the customer from braintreee
            var customer = await _braintree.Customer.FindAsync(booking.UserId.ToString());

            // Get the default credit card for the customer
            Result<PaymentMethodNonce> result = _braintree.PaymentMethodNonce.Create(customer.DefaultPaymentMethod.Token);
            string nonce = result.Target.Nonce;

            // publish the booking to be processed
            await _endpointInstance.SendLocal(new ProcessBooking()
            {
                BookingId = booking.Id,
                CreatedOn = booking.StartDate,
                PaymentNonce = nonce
            });

            _logger.LogInformation(LogEvents.CreateNewBooking, "Successfully closed booking {bookingId} for user {userId}", booking.Id, booking.UserId);

            return Ok();
        }

        // POST: api/Booking/GenerateQrCode/{dockId}
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<FileResult> GenerateQrCode([FromRoute(Name = "id")]Guid bookingId)
        {
            if (bookingId == Guid.Empty)
                return null;

            // Get the booking
            var booking = await _bookingRepo.GetByIdAsync(bookingId);

            // Check if the booking exists
            if (booking == null)
                return null;

            // Get the web app url
            var url = _configuration.GetValue<string>("WebAppUrl");

            QRCodeGenerator qrGenerator = new QRCodeGenerator();

            // Create data for the qr code for the requested booking
            QRCodeData qrCodeData = qrGenerator.CreateQrCode($"{url}Receipt/{bookingId}", QRCodeGenerator.ECCLevel.Q);

            // Generate the qr code into a bitmap
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);


            // Convert the bitmap image into a memory stream to be sent to the client
            using (MemoryStream ms = new MemoryStream())
            {
                // Write the image into the memory stream
                qrCodeImage.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

                // Return the image as jpeg
                return File(ms.ToArray(), "image/jpeg", $"{bookingId}-qr.jpeg");
            }

        }
    }
}
