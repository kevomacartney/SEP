using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Threading.Tasks;
using CityCycles.Api.Services;
using CityCycles.Composite.Data.ManagementData.User;
using CityCycles.Composite.Shared_Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace CityCycles.Api.Managers
{
    /// <summary>
    /// Manages the authentication tokens handed to user
    /// </summary>
    public class AuthTokenManager
    {
        private readonly IServiceProvider _serviceProvider;

        private readonly IClientManager _clientManager;

        private readonly EncryptionService _encryptionService;

        public AuthTokenManager(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;

            _clientManager = _serviceProvider.GetService<IClientManager>();
            _encryptionService = _serviceProvider.GetService<EncryptionService>();
        }

        /// <summary>
        /// Creates the bearer token.
        /// </summary>
        /// <returns>The bearer token.</returns>
        /// <param name="userId">User identifier.</param>
        public async Task<string> CreateBearerTokenAsync(Guid userId, string audience)
        {
            var user = await _clientManager.GetClientByIdAsync(userId);
            return await CreateBearerTokenAsync(user, audience);
        }

        /// <summary>
        /// Creates the bearer token.
        /// </summary>
        /// <returns>The bearer token.</returns>
        /// <param name="user">User.</param>
        public async Task<string> CreateBearerTokenAsync(User user, string audience)
        {
            // Create identity for the client
            var claims = await _clientManager.CreateUserClaimsIdentityAsync(user.Id);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = claims,
                Expires = DateTime.UtcNow.AddHours(2),
                IssuedAt = DateTime.UtcNow,
                Issuer = "Bikefy Api",
                Audience = audience,
                SigningCredentials = _encryptionService.TokenSignKey
            };

            // Create the token
            var token = new JwtSecurityTokenHandler().CreateJwtSecurityToken(tokenDescriptor);

            // Write the token to string.
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
