using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CityCycles.Api.Services;
using CityCycles.Composite.Data;
using CityCycles.Composite.Data.ManagementData.User;
using CityCycles.Composite.Model.Auth;
using CityCycles.Composite.Shared_Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace CityCycles.Api.Managers
{
    public class ClientManager : IClientManager
    {
        /// <summary>
        /// The number of times a user can get their key wrong
        /// </summary>
        private readonly int Lockoutaccesscount = 5;

        /// <summary>
        /// An instance of the Context being used by ClientManager
        /// </summary>
        private ManagementDataContext Context { get; }

        /// <summary>
        /// Gets an instance of the password hasher used for verifying claimed key
        /// </summary>
        private PasswordHasher PasswordHasher { get; }

        /// <summary>
        /// Gets the type of the identity provider claim.
        /// </summary>
        /// <value>The type of the identity provider claim.</value>
        private string IdentityProviderClaimType
            => "http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider";

        /// <summary>
        /// Gets the type of the security stamp claim.
        /// </summary>
        /// <value>The type of the security stamp claim.</value>
        private string SecurityStampClaimType
            => "Bikeyfy.Identity.SecurityStamp";

        /// <summary>
        /// Local service scope 
        /// </summary>
        private readonly IServiceScope _scope;


        public ClientManager(IServiceProvider serviceProvider)
        {
            // Create a scope to isolate client manager
            _scope = serviceProvider.CreateScope();

            Context = _scope.ServiceProvider.GetService<ManagementDataContext>();
            PasswordHasher = _scope.ServiceProvider.GetService<PasswordHasher>();
        }


        public async Task<SignInResult> PasswordSigninAsync(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(username))
                throw new ArgumentNullException($"{nameof(username)} or {nameof(password)}");

            var user = await GetClientByUsernameAsync(username);

            if (user == null)
                return SignInResult.Failed;

            // Check if can sign in
            var error = await PreSignInCheckAsync(user.Id);
            if (error != null)
                return error;

            if (CheckPasssword(user, password))
            {
                await ResetLockoutAsync(user);

                // Log this sign in
                await LogSignInAsync(user);

                return SignInResult.Success;
            }

            // Increment the access failed counter
            await ClientAccessFailedAsync(user.Id);

            // Check if client has been locked out
            if (await IsLockedOutAsync(user.Id))
                return await LockOutAsync(user.Id);

            return SignInResult.Failed;
        }

        public async Task<SignInResult> ApplicationSignInAsync(Guid appId, string accessKey)
        {
            if (string.IsNullOrEmpty(accessKey))
                throw new ArgumentNullException($"{nameof(accessKey)}");

            var user = await GetClientByIdAsync(appId);

            if (user == null)
                return SignInResult.Failed;
            // Check if can sign in
            var error = await PreSignInCheckAsync(user.Id);
            if (error != null)
                return error;

            var encyrptService = _scope.ServiceProvider.GetService<EncryptionService>();
            if (CheckPasssword(user, encyrptService.Decrypt(accessKey)))
            {
                await ResetLockoutAsync(user);

                // Log this sign in
                await LogSignInAsync(user);

                return SignInResult.Success;
            }

            // Increment the access failed counter
            await ClientAccessFailedAsync(user.Id);

            // Check if client has been locked out
            if (await IsLockedOutAsync(user.Id))
                return await LockOutAsync(user.Id);

            return SignInResult.Failed;
        }

        private Task LogSignInAsync(User user)
        {
            if (user == null)
                throw new ArgumentNullException($"{nameof(user)}");
            //todo
            return Task.CompletedTask;
        }

        public bool CheckPasssword(User user, string password)
        {
            if (user == null || string.IsNullOrEmpty(password))
                throw new ArgumentNullException($"{nameof(user)} or {nameof(password)}");

            return PasswordHasher.VerifyHashedPassword(user.PasswordHash, password);
        }

        public async Task<ClaimsIdentity> CreateUserClaimsIdentityAsync(Guid clientId)
        {
            if (clientId == null || Guid.Empty == clientId)
                throw new ArgumentNullException($"{nameof(clientId)}");

            var user = await GetClientByIdAsync(clientId);
            var id = new ClaimsIdentity("ApiKey", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var secKey = await GetSecurityTokenAsync(clientId);

            // Add default claims
            id.AddClaim(new Claim(ClaimTypes.NameIdentifier, clientId.ToString(), ClaimValueTypes.String));
            if (user.FirstName != null)
                id.AddClaim(new Claim(ClaimTypes.GivenName, user.FirstName, ClaimValueTypes.String));
            if (user.LastName != null)
                id.AddClaim(new Claim(ClaimTypes.Surname, user.LastName, ClaimValueTypes.String));

            id.AddClaim(new Claim(IdentityProviderClaimType, "City Cycles Identity", ClaimValueTypes.String));
            id.AddClaim(new Claim(SecurityStampClaimType, secKey, ClaimValueTypes.String));

            id.AddClaim(new Claim($"{nameof(User.IsStaff)}", user.IsStaff.ToString(), ClaimValueTypes.Boolean));

            // Get roles 
            var roles = await GetRolesAsync(clientId);
            foreach (var role in roles)
                id.AddClaim(new Claim(ClaimTypes.Role, role));

            // Add user claims
            id.AddClaims(await GetClaimsAsync(clientId));

            return id;
        }

        public async Task DeleteUserAsync(Guid userId)
        {
            // Get user id
            var user = await Context.User.FindAsync(userId);

            if (user == null)
                throw new InvalidOperationException($"Could not find user with id {userId}");

            // Delete the entity
            Context.Remove(user);
            await Context.SaveChangesAsync();
        }

        public async Task<User> GetClientByIdAsync(Guid clientId)
        {
            if (Guid.Empty == clientId)
                throw new ArgumentNullException($"{nameof(clientId)}");

            return await Context.User.FirstOrDefaultAsync(u => u.Id == clientId);


        }

        public Task<User> GetClientByUsernameAsync(string email)
        {
            if (string.IsNullOrEmpty(email))
                throw new ArgumentNullException(nameof(email));

            return Context.User.Include(u => u.UserRoles).Include(u => u.UserClaims).FirstOrDefaultAsync(u => u.Email == email);
        }

        public async Task UpdateAsync(User user)
        {
            if (!Validate(user))
                throw new InvalidOperationException($"{nameof(user)} not valid");

            Context.User.Update(user);

            await Context.SaveChangesAsync();
        }

        public async Task<string> GetSecurityTokenAsync(Guid clientId)
        {
            if (clientId == null || Guid.Empty == clientId)
                throw new ArgumentNullException($"{nameof(clientId)}");

            var user = await GetClientByIdAsync(clientId);
            return user.SecurityStamp;
        }

        public async Task<List<string>> GetRolesAsync(Guid clientId)
        {
            if (Guid.Empty == clientId)
                throw new ArgumentNullException($"{nameof(clientId)}");

            var roleName = new List<string>();

            // Get the user's role
            var roles = await Context.UserRoles.Where(u => u.UserId == clientId).Select(s => s.Role).ToListAsync();

            // For each role, add the name to the collection
            foreach (var role in roles)
            {
                roleName.Add(role.Name);
            }

            // return the users roles
            return roleName;
        }

        public async Task AddRolesAsync(Guid clientId, string[] roles)
        {
            if (Guid.Empty == clientId || roles == null)
                throw new ArgumentNullException($"{nameof(clientId)} or {nameof(roles)}");

            foreach (var role in roles) // For each of the roles, add user to the role
                await AddRoleAsync(clientId, role);
        }

        public async Task<IdentityResult> AddRoleAsync(Guid clientId, string role)
        {
            if (Guid.Empty == clientId || role == null)
                throw new ArgumentNullException($"{nameof(clientId)} or {nameof(role)}");

            // get the role from db
            var entity = await Context.Roles.FirstOrDefaultAsync(r => r.Name == role);

            // Return identity result failed if the role was not found.
            if (entity == null)
                return IdentityResult.Failed(new IdentityError() { Description = $"Could not find a role with the given name  '{role}'" });

            // Call the overload function which will return the actual result of the operation
            return await AddRoleAsync(clientId, entity);
        }

        public async Task<IdentityResult> AddRoleAsync(Guid clientId, Role role)
        {
            if (Guid.Empty == clientId || role == null)
                throw new ArgumentNullException($"{nameof(clientId)} or {nameof(role)}");

            // Get the user instance
            var user = await GetClientByIdAsync(clientId);

            // Add the user role to the relationship
            role.UserRoles.Add(new UserRole()
            {
                Role = role,
                User = user
            });

            // Write the changes to the db 
            Context.Update(role);
            await Context.SaveChangesAsync();

            return IdentityResult.Success;
        }

        public async Task CreateRoleAsync(Role role)
        {
            if (role == null)
                throw new ArgumentNullException($"{nameof(role)}");

            if (role.Id == Guid.Empty)
                role.Id = Guid.NewGuid();

            await Context.Roles.AddAsync(role);
            await Context.SaveChangesAsync();
        }

        public async Task<bool> RoleExistsAsync(string role)
            => await Context.Roles.AnyAsync(r => r.Name == role);

        public async Task<List<Claim>> GetClaimsAsync(Guid clientId)
        {
            if (clientId == null || Guid.Empty == clientId)
                throw new ArgumentNullException($"{nameof(clientId)}");

            var userClaims = await Context.UserClaims.Where(u => u.UserId == clientId).ToListAsync();
            var claims = new List<Claim>();

            userClaims.ForEach(claim =>
            {
                claims.Add(new Claim(claim.ClaimType, claim.ClaimValue, ClaimValueTypes.String));
            });

            return claims;
        }

        public async Task AddClaimsAsync(Guid clientId, Claim claim)
        {
            var newClaim = new UserClaim()
            {
                User = await GetClientByIdAsync(clientId),
                ClaimType = claim.Type,
                ClaimValue = claim.Value
            };

            await Context.UserClaims.AddAsync(newClaim);
            await Context.SaveChangesAsync();
        }

        public async Task<IdentityResult> RemoveClaimAsync(Guid clientId, Claim claim)
        {
            if (clientId == null || Guid.Empty == clientId)
                throw new ArgumentNullException($"{nameof(clientId)}");

            if (clientId == Guid.Empty || claim == null)
                return IdentityResult.Failed(new IdentityError[]
                {
                    new IdentityError()
                        {Description = "Null Parameters"}
                });


            var entity = Context.UserClaims.FirstOrDefault(c =>
                c.UserId == clientId && c.ClaimType == claim.Type && c.ClaimValue == claim.Value);

            if (entity == null)
                return IdentityResult.Failed(new IdentityError[]
                {
                    new IdentityError()
                        {Description = "Claim not found"}
                });


            Context.UserClaims.Remove(entity);
            await Context.SaveChangesAsync();

            return IdentityResult.Success;
        }

        protected async Task ClientAccessFailedAsync(Guid clientId)
        {
            if (clientId == null || Guid.Empty == clientId)
                throw new ArgumentNullException($"{nameof(clientId)}");

            var user = await GetClientByIdAsync(clientId);
            user.AccessFailedCount++;

            Context.Update(user);
            await Context.SaveChangesAsync();
        }

        public async Task<SignInResult> LockOutAsync(Guid clientId)
        {
            if (clientId == null || Guid.Empty == clientId)
                throw new ArgumentNullException($"{nameof(clientId)}");

            var user = await GetClientByIdAsync(clientId);

            user.LockoutEnabled = true;
            user.LockoutEndDateUtc = DateTime.UtcNow;

            Context.Update(user);
            await Context.SaveChangesAsync();

            return SignInResult.LockedOut;
        }

        public async Task<bool> IsLockedOutAsync(Guid clientId)
        {
            if (clientId == null || Guid.Empty == clientId)
                throw new ArgumentNullException($"{nameof(clientId)}");

            var user = await GetClientByIdAsync(clientId);
            return user.AccessFailedCount >= Lockoutaccesscount;
        }

        protected async Task<bool> CanSignInAsync(Guid clientId)
        {
            if (Guid.Empty == clientId)
                throw new ArgumentNullException($"{nameof(clientId)}");

            var user = await GetClientByIdAsync(clientId);
            return !user.AccountClosed;
        }

        protected async Task<SignInResult> PreSignInCheckAsync(Guid user)
        {
            if (user == null)
                throw new ArgumentNullException($"{nameof(user)}");

            if (!await CanSignInAsync(user))
            {
                return SignInResult.NotAllowed;
            }
            if (await IsLockedOutAsync(user))
            {
                return await LockOutAsync(user);
            }
            return null;
        }

        public async Task ResetLockoutAsync(User user)
        {
            if (user == null)
                throw new ArgumentNullException($"{nameof(user)}");

            user.AccessFailedCount = 0;
            user.LockoutEnabled = false;
            user.LockoutEndDateUtc = null;

            Context.Update(user);
            await Context.SaveChangesAsync();
        }


        private bool Validate(User user)
        {
            if (user == null)
                throw new ArgumentNullException($"{nameof(user)}");

            return user.Id != Guid.Empty;
        }


        public void Dispose()
            => _scope.Dispose();

        public async Task<Guid> CreateNewUserAsync(CreateAccountModel model)
        {
            if (model == null)
                throw new ArgumentNullException(nameof(model));

            // Check that the user email doesn't exist
            var user = await GetClientByUsernameAsync(model.Email.ToLower());

            if (user != null)
                throw new InvalidOperationException("A user with the email already exsits");

            user = new User()
            {
                Id = Guid.NewGuid(),
                Email = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                PasswordHash = PasswordHasher.HashPassword(model.Password), // Has the user's password
                SecurityStamp = Guid.NewGuid().ToString(),
                DateCreated = DateTime.UtcNow
            };

            // Add the user entity to the database
            await Context.User.AddAsync(user);
            Context.SaveChangesAsync().Wait();

            return user.Id;
        }

        public Task<Role> GetRoleByIdAsync(Guid roleId)
             => Context.Roles.FirstOrDefaultAsync(s => s.Id == roleId);

        public async Task UpdateSecurityTokenAsync(Guid clientId)
        {
            // Check that the user email doesn't exist
            var user = await GetClientByIdAsync(clientId);

            if (user == null)
                throw new InvalidOperationException("A user with the ID could not be found");

            user.SecurityStamp = Guid.NewGuid().ToString();

            // Update the user
            await UpdateAsync(user);
        }
    }
}
