﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Serilog;
using Serilog.Events;

namespace CityCycles.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
                     WebHost.CreateDefaultBuilder(args)
                    .UseIISIntegration()
                    .ConfigureLogging(loggingBuilder =>
                    {
                        loggingBuilder.AddSerilog();
                    })
                    .UseApplicationInsights()
                    .UseStartup<Startup>();
    }
}
