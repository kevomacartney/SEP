﻿using System;
namespace CityCyclesAPI.Api
{
    public enum ApiStatusCodes
    {

        #region General

        InvalidModel = 420,
        DuplicateEntity = 69,

        EntityIdNotProvided = 521,
        EntityNotFound,

        #endregion

        #region Auth Controller

        EmailTaken,
        InvalidLogin,
        AccountLockedOut,
        AccountDisabled,

        #endregion

        #region Dock Controller

        DockAtMaxCapacity,

        #endregion

        #region Booking 

        NoAvaiableBikeInDock,
        PricingNotFound,
        BikeNotAvaiable,

        #region Dock Booking
        DockBookingTokenInvalid,
        UserNotFound,
        DockNotFound,
        BookingExpired,
        InvalidSecurityToken,
        BookingNotFound,
        UserHasNoDefaultCard,
        UserHasActiveBooking
        #endregion

        #endregion
    }
}
