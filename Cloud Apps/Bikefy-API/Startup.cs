using System;
using System.Net.Mail;
using CityCycles.Api.Extensions;
using CityCycles.Api.Extensions.Authentication;
using CityCycles.Api.Extensions.ServiceBus;
using CityCycles.Api.Managers;
using CityCycles.Api.Services;
using CityCycles.Composite.Braintree;
using CityCycles.Composite.Data;
using CityCycles.Composite.Data.ManagementData.User;
using CityCycles.Composite.Logging;
using CityCycles.Composite.Repository;
using CityCycles.Composite.Repository.Concrete;
using CityCyclesAPI.Emailing;
using CityCyclesAPI.Emailing.Configuration;
using CityCyclesAPI.PdfServices;
using CityCyclesAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace CityCycles.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add seq logging to the pipeline
            services.AddSeqLogging(Configuration);

            // Configure authentication
            services.ConfigureAuthentication(Configuration);

            // Register management datacontext with the services
            services.AddDbContext<ManagementDataContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("ManagementDb")));


            // Register client manager as a service
            services.AddScoped<IClientManager, ClientManager>();

            // Register the authorisation token manager
            services.AddScoped<AuthTokenManager>();

            // Register the password hasher as a service
            services.AddTransient<PasswordHasher>();

            // Register repsitories 
            services.AddScoped<IDockRepository, DockRepository>();
            services.AddScoped<IBikeRepository, BikeRepository>();
            services.AddScoped<IPricingRepository, PricingRepository>();
            services.AddScoped<IBookingRepository, BookingRepository>();

            // View engine
            services.AddScoped<IRazorViewToStringRenderer, RazorViewToStringRenderer>();
            services.AddScoped<IPdfService, PdfService>();

            // Add braintree to the pipeline
            services.AddBraintree(Configuration);

            // Add SMTP client
            services.AddTransient((IServiceProvider serviceProvider) =>
            {
                var configuration = serviceProvider.GetService<IConfiguration>();

                var config = new GmailConfiguration();
                configuration.Bind("GmailEndpoint", config);

                // create stmpclient instance
                SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com")
                {
                    Port = 587,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    EnableSsl = true
                };
                // Credential
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Credentials = new System.Net.NetworkCredential(config.Email, config.Password);

                return SmtpServer;
            });

            services.AddMvc(options =>
            {
                var policy = new AuthorizationPolicyBuilder()
                             .RequireAuthenticatedUser()
                             .Build();

                options.Filters.Add(new AuthorizeFilter(policy));


                // Ensures that all models going to controllers are valid, returns error other wise
                //options.Filters.Add(typeof(ValidatorActionFilter));

            })
            .AddJsonOptions(
            options =>
            {
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            })
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);


            // Add Service bus to the the pipeline
            services.AddServiceBus(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            // Authentication middle-ware to protect MVC
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
