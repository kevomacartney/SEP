﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using NServiceBus;
using System.Collections.Generic;
using CityCycles.Composite.Repository;
using CityCycles.Composite.Configurations;
using Microsoft.Extensions.Configuration;
using Braintree;
using CityCycles.Composite.BusMessages.API;
using CityCycles.Composite.BusMessages.Management;
using CityCyclesAPI.Services;
using CityCyclesAPI.Emailing;
using CityCycles.Composite.Data.ManagementData;
using CityCycles.Api.Services;

namespace CityCyclesAPI.BusHandlers.Bookings
{
    public class ProcessBookingHandler : IHandleMessages<ProcessBooking>
    {
        private static class LogEvents
        {
            public static readonly EventId HandlingProcessOrderRequest = new EventId(1, nameof(Handle));
            public static readonly EventId CalculatingBookingTotal = new EventId(2, nameof(CalculateTotalAsync));
        }
        private readonly IConfiguration _configuration;
        private readonly ILogger _logger;

        private readonly IBookingRepository _bookingRepo;
        private readonly IPricingRepository _pricingRepo;

        private readonly ServiceBusConfiguration _serviceBusConfig;
        private readonly IBraintreeGateway _braintree;
        private readonly IClientManager _clientManager;

        public ProcessBookingHandler(IServiceProvider serviceProvider)
        {
            _logger = serviceProvider.GetService<ILogger<ProcessBookingHandler>>();
            _bookingRepo = serviceProvider.GetService<IBookingRepository>();
            _pricingRepo = serviceProvider.GetService<IPricingRepository>();

            _configuration = serviceProvider.GetService<IConfiguration>();
            _serviceBusConfig = serviceProvider.GetService<ServiceBusConfiguration>();
            _braintree = serviceProvider.GetService<IBraintreeGateway>();
            _clientManager = serviceProvider.GetService<IClientManager>();


        }

        /// <summary>
        /// Handle the specified message and context.
        /// </summary>
        /// <returns>The handle.</returns>
        /// <param name="message">Message.</param>
        /// <param name="context">Context.</param>
        public async Task Handle(ProcessBooking message, IMessageHandlerContext context)
        {
            using (_logger.BeginScope(new Dictionary<string, object>()
            {
                ["Booking Id"] = message.BookingId,
                ["Created On"] = message.CreatedOn
            }))
            {
                // Validate the booking id
                if (message.BookingId == Guid.Empty)
                {
                    _logger.LogWarning(LogEvents.HandlingProcessOrderRequest, "Could not process booking with invalid booking id");
                    return;
                }

                _logger.LogInformation(LogEvents.HandlingProcessOrderRequest, "Processing Booking with ID {bookingId} that was created", message.BookingId, message.CreatedOn);

                var booking = await _bookingRepo.GetByIdAsync(message.BookingId);

                // Validate the booking exists
                if (booking == null)
                {
                    _logger.LogError(LogEvents.HandlingProcessOrderRequest, "Could not find a booking with the ID {bookingId}", message.BookingId);
                    return;
                }

                // Get the user
                var user = await _clientManager.GetClientByIdAsync(booking.UserId);

                // Validate the booking exists
                if (user == null)
                {
                    _logger.LogError(LogEvents.HandlingProcessOrderRequest, "Could not find the owner of the booking with Id  {userId}", booking.UserId);
                    return;
                }

                // Check if transactions are allowed
                if (!_configuration.GetValue<bool>("AllowTransactions"))
                {
                    // Send this to process email
                    await context.SendLocal(new PaymentNotProcessed()
                    {
                        BookingId = message.BookingId,
                        DeclinedOn = DateTime.UtcNow,
                        UserEmail = user.Email,
                        Reason = "There was an internal error while processing your order"
                    });
                    return;
                }

                // Mark booking as processing
                booking.Status = BookingStatus.Processed;
                await _bookingRepo.UpdateAsync(booking);

                // If order request was created more than 3 hours ago, then the payment nonce has expired
                if ((DateTime.UtcNow - message.CreatedOn).TotalHours >= TimeSpan.FromHours(3).TotalHours)
                {
                    _logger.LogWarning("Could not process a booking, the payment noce has expired");

                    // Send this to process email
                    await context.SendLocal(new PaymentNotProcessed()
                    {
                        BookingId = message.BookingId,
                        DeclinedOn = DateTime.UtcNow,
                        UserEmail = user.Email,
                        Reason = "There was a problem with your payment method"
                    });
                    return;
                }

                if (string.IsNullOrEmpty(message.PaymentNonce))
                {
                    _logger.LogWarning(LogEvents.HandlingProcessOrderRequest, "A paymenet nonce was not provided for booking {bookingId}", message.BookingId);

                    // Get the customer
                    var customer = await _braintree.Customer.FindAsync(booking.UserId.ToString());

                    // If customer does not have a payment method
                    if (customer.DefaultPaymentMethod == null)
                    {
                        _logger.LogWarning(LogEvents.HandlingProcessOrderRequest, "Could not create a payment nonce, customer does not have a default payment.");

                        // Send this to process email
                        await context.SendLocal(new PaymentNotProcessed()
                        {
                            BookingId = message.BookingId,
                            DeclinedOn = DateTime.UtcNow,
                            UserEmail = user.Email,
                            Reason = "There was a problem with your payment method"
                        });
                        return;
                    }

                    var nonceResult = await _braintree.PaymentMethodNonce.CreateAsync(customer.DefaultPaymentMethod.Token);

                    if (!nonceResult.IsSuccess())
                    {
                        _logger.LogWarning(LogEvents.HandlingProcessOrderRequest, $"Could not create a payment nonce, {nonceResult.Message}");

                        // Send this to process email
                        await context.SendLocal(new PaymentNotProcessed()
                        {
                            BookingId = message.BookingId,
                            DeclinedOn = DateTime.UtcNow,
                            UserEmail = user.Email,
                            Reason = "There was a problem with your payment method"
                        });
                        return;
                    }

                    // Set the nonce
                    message.PaymentNonce = nonceResult.Target.Nonce;
                }

                var total = await CalculateTotalAsync(message);

                // If we couldn't calculate th price return;
                if (total < 0)
                    return;


                var transactionRequest = new TransactionRequest()
                {
                    Amount = total,
                    PaymentMethodNonce = message.PaymentNonce,
                    Options = new TransactionOptionsRequest
                    {
                        SubmitForSettlement = true
                    }
                };

                // Send transaction request to braintree
                Result<Transaction> result = _braintree.Transaction.Sale(transactionRequest);
                if (result.IsSuccess())
                {
                    _logger.LogInformation($"Successfully charged customer {result.Target.CurrencyIsoCode} {transactionRequest.Amount}");

                    // Mark booking as used
                    await _bookingRepo.UpdateAsync(booking);

                    // Payment was processed, place message on bus 
                    await context.SendLocal(new PaymentProcessed()
                    {
                        BookingId = message.BookingId,
                        TransactionId = result.Target.Id,
                        ProcessedOn = result.Target.CreatedAt.Value,
                        UserEmail = user.Email
                    });
                }
                else
                {
                    using (_logger.BeginScope(new Dictionary<string, object>()
                    {
                        ["Braintree Errors Id"] = result.Errors
                    }))
                    {
                        _logger.LogWarning($"Could not process customer payment, '{result.Message}'");

                        // Payment was not processed, place message on bus 
                        await context.SendLocal(new PaymentNotProcessed()
                        {
                            BookingId = message.BookingId,
                            DeclinedOn = DateTime.UtcNow,
                            UserEmail = user.Email,
                            Reason = "There was an error processing your payment, please double check the information provided",
                            TransactionId = result.Target.Id
                        });
                    }
                }
            }
        }

        private async Task<decimal> CalculateTotalAsync(ProcessBooking request)
        {
            _logger.LogInformation(LogEvents.CalculatingBookingTotal, "Calculating total for booking {bookingId}", request.BookingId);

            // Get the entities
            var booking = await _bookingRepo.GetByIdAsync(request.BookingId);


            // Get the pricing model
            Pricing pricing;
            if (request.PricingId == Guid.Empty)
                pricing = await _pricingRepo.GetByIdAsync(booking.PricingId);
            else
                pricing = await _pricingRepo.GetByIdAsync(request.PricingId);

            if (pricing == null)
            {
                _logger.LogError(LogEvents.CalculatingBookingTotal, "Could not find pricing entity with id {pricingId} or {pricingId2}", request.PricingId, booking.PricingId);
                return -1;
            }

            if (booking == null)
            {
                _logger.LogError(LogEvents.CalculatingBookingTotal, "Could not find booking entity with id {bookingId}", request.BookingId);
                return -2;
            }

            // Gets the total hours for the booking
            var hours = Convert.ToDecimal((booking.EndDate - booking.StartDate).TotalHours);

            // Calculate the totals for the booking duration
            var Total = hours * pricing.HourlyPrice;

            if (!booking.IsDockBooking) // Dock booking, doesn't cost a booking fee
                Total += pricing.BookingFees;

            _logger.LogInformation(LogEvents.CalculatingBookingTotal, "Calculated total for booking {bookingId} to be £{bookingTotal}", request.BookingId, Total);
            return Total;
        }
    }
}
