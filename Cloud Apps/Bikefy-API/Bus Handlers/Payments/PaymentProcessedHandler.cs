﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Braintree;
using CityCycles.Api.Services;
using CityCycles.Composite.BusMessages.API;
using CityCycles.Composite.BusMessages.Management;
using CityCycles.Composite.Data.ManagementData;
using CityCycles.Composite.Repository;
using CityCyclesAPI.Emailing;
using CityCycles.Api.Emailing.Models;
using CityCyclesAPI.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NServiceBus;
using Microsoft.Extensions.Configuration;

namespace CityCyclesAPI.BusHandlers.Payments
{
    public class PaymentProcessedHandler : IHandleMessages<PaymentProcessed>
    {
        private static class LogEvents
        {
            public static readonly EventId HandlingPaymentProcessed = new EventId(1, nameof(Handle));
        }

        private readonly IPdfService _pdfService;
        private readonly IRazorViewToStringRenderer _razorViewRenderer;
        private readonly IBraintreeGateway _braintree;

        private readonly IBookingRepository _bookingRepository;
        private readonly IPricingRepository _pricingRepo;
        private readonly IDockRepository _dockRepo;
        private readonly IBikeRepository _bikeRepo;

        private readonly IClientManager _clientManager;

        private readonly IConfiguration _configuration;

        private readonly ILogger _logger;

        public PaymentProcessedHandler(IServiceProvider serviceProvider)
        {
            _pdfService = serviceProvider.GetService<IPdfService>();
            _razorViewRenderer = serviceProvider.GetService<IRazorViewToStringRenderer>();

            _braintree = serviceProvider.GetService<IBraintreeGateway>();
            _pricingRepo = serviceProvider.GetService<IPricingRepository>();
            _dockRepo = serviceProvider.GetService<IDockRepository>();
            _bikeRepo = serviceProvider.GetService<IBikeRepository>();

            _clientManager = serviceProvider.GetService<IClientManager>();
            _bookingRepository = serviceProvider.GetService<IBookingRepository>();
            _logger = serviceProvider.GetService<ILogger<PaymentProcessedHandler>>();

            _configuration = serviceProvider.GetService<IConfiguration>();
        }

        public async Task Handle(PaymentProcessed message, IMessageHandlerContext context)
        {
            using (_logger.BeginScope(new Dictionary<string, object>()
            {
                ["bookingId"] = message.BookingId,
                ["userEmail"] = message.UserEmail,
                ["transactionId"] = message.TransactionId
            }))
            {
                // Load the order email template
                var orderEmail = await _razorViewRenderer.RenderViewToStringAsync("~/Emailing/Templates/Views/OrderEmail.cshtml", new OrderEmailModel());

                // Get the entities required
                var booking = await _bookingRepository.GetByIdAsync(message.BookingId);
                var user = await _clientManager.GetClientByIdAsync(booking.UserId);
                var pricing = await _pricingRepo.GetByIdAsync(booking.PricingId);
                var bike = await _bikeRepo.GetByIdAsync(booking.BikeId);
                var dock = await _dockRepo.GetByIdAsync(bike.DockId);

                _logger.LogInformation(LogEvents.HandlingPaymentProcessed, "Payment for booking {bookingId} was successfully processed", message.BookingId);

                // Get the braintree transactionn
                var transaction = await _braintree.Transaction.FindAsync(message.TransactionId);
                if (transaction == null)
                {
                    _logger.LogWarning(LogEvents.HandlingPaymentProcessed, "Could not find transaction with the specified id");
                    return;
                }

                _logger.LogInformation(LogEvents.HandlingPaymentProcessed, "Booking and transacton was found");


                // Calculate the times
                var timespan = booking.EndDate - booking.StartDate;
                var timeTotal = Convert.ToDecimal(timespan.TotalHours) * pricing.HourlyPrice;

                booking.Status = BookingStatus.Accepted;
                booking.Order = new Order()
                {
                    CreatedOn = message.ProcessedOn,
                    Total = transaction.Amount.Value,
                    Items = new Dictionary<string, decimal>()
                    {
                        ["BookingFee"] = booking.IsDockBooking ? 0 : pricing.BookingFees,
                        ["Time"] = timeTotal
                    }
                };

                var url = _configuration.GetValue<string>("ApiAppUrl");

                // Create the receipt model
                var receiptModel = new ReceiptModel()
                {
                    BookingFee = pricing.BookingFees.ToString("0.##"),
                    BookingId = message.BookingId,
                    DockLocation = dock.Name,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    GrandTotal = booking.Order.Total.ToString("0.##"),
                    TimeTotal = timeTotal.ToString("0.##"),
                    OrderDate = booking.Order.CreatedOn,
                    CardType = transaction.CreditCard.CardType,
                    Last4Digits = transaction.CreditCard.LastFour,
                    OrderQuantity = timespan.TotalHours.ToString(),
                    ReceiptUrl = $"{url}GenerateQrCode/{booking.Id}"
                };

                // Load the receipt tamplates
                var receiptHml = await _razorViewRenderer.RenderViewToStringAsync("~/Emailing/Templates/Views/ReceiptTemplate.cshtml", receiptModel);
                var pdf = _pdfService.GeneratePdf(receiptHml); // Generate the pdf from the html

                // Create a dictionary for the attachments
                var attachments = new Dictionary<string, byte[]>()
                {
                    [$"Invoice-{message.BookingId}.pdf"] = pdf
                };

                // Update the booking entity to include the new order
                await _bookingRepository.UpdateAsync(booking);

                _logger.LogInformation(LogEvents.HandlingPaymentProcessed, "Created order entity for the booking");

                // Send an email to the clien
                await context.SendLocal(new SendEmail()
                {
                    EmailContent = orderEmail,
                    Recipient = message.UserEmail,
                    Subject = "Citycyles receipt",
                    Attachments = new DataBusProperty<Dictionary<string, byte[]>>(attachments)
                });

                _logger.LogInformation(LogEvents.HandlingPaymentProcessed, "Request to send user email was successfully placed on the bus");
            }
        }
    }
}
