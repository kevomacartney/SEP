﻿using System;
using System.Threading.Tasks;
using CityCycles.Composite.BusMessages.API;
using CityCycles.Composite.Repository;
using Microsoft.Extensions.DependencyInjection;
using NServiceBus;
using CityCycles.Composite.Data.ManagementData;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using CityCycles.Api.Services;
using CityCyclesAPI.Emailing;
using CityCycles.Api.Emailing.Models;
using CityCycles.Composite.BusMessages.Management;
using Braintree;

namespace CityCyclesAPI.BusHandlers.Payments
{
    public class PaymentNotProcessedHandler : IHandleMessages<PaymentNotProcessed>
    {
        public class LogEvents
        {
            public static readonly EventId HandlingOrderNotProcessed = new EventId(1, nameof(Handle));
        }

        private readonly IBookingRepository _bookingRepo;
        private readonly ILogger _logger;

        private readonly IClientManager _clientManager;
        private readonly IDockRepository _dockRepo;
        private readonly IBikeRepository _bikeRepo;

        private readonly IRazorViewToStringRenderer _razorViewRenderer;
        private readonly IBraintreeGateway _braintree;

        public PaymentNotProcessedHandler(IServiceProvider serviceProvider)
        {
            _bookingRepo = serviceProvider.GetService<IBookingRepository>();
            _dockRepo = serviceProvider.GetService<IDockRepository>();
            _bikeRepo = serviceProvider.GetService<IBikeRepository>();

            _razorViewRenderer = serviceProvider.GetService<IRazorViewToStringRenderer>();
            _braintree = serviceProvider.GetService<IBraintreeGateway>();

            _clientManager = serviceProvider.GetService<IClientManager>();
            _logger = serviceProvider.GetService<ILogger<PaymentNotProcessedHandler>>();
        }

        public async Task Handle(PaymentNotProcessed message, IMessageHandlerContext context)
        {
            using (_logger.BeginScope(new Dictionary<string, object>()
            {
                ["bookingId"] = message.BookingId,
                ["userEmail"] = message.UserEmail,
                ["reason"] = message.Reason
            }))
            {
                _logger.LogInformation(LogEvents.HandlingOrderNotProcessed, $"Handling failed payment for booking {message.BookingId}");

                // Get the booking and mark it as declined
                var booking = await _bookingRepo.GetByIdAsync(message.BookingId);
                var user = await _clientManager.GetClientByIdAsync(booking.UserId);

                var bike = await _bikeRepo.GetByIdAsync(booking.BikeId);
                var dock = await _dockRepo.GetByIdAsync(bike.DockId);

                // Validate the booking exists
                if (booking == null)
                {
                    _logger.LogError(LogEvents.HandlingOrderNotProcessed, "Could not find a booking with the ID {bookingId}", message.BookingId);
                    return;
                }

                // Create the model
                var model = new FailedOrderEmail()
                {
                    DockName = dock.Name,
                    StartTime = booking.StartDate,
                    EndDate = booking.EndDate,
                    Reason = message.Reason
                };

                // get the braintree transaction
                if (!string.IsNullOrEmpty(message.TransactionId))
                {
                    _logger.LogInformation(LogEvents.HandlingOrderNotProcessed, "Get transaction {transactionId} from braintree", message.TransactionId);
                    var transaction = await _braintree.Transaction.FindAsync(message.TransactionId);

                    if (transaction == null)
                        _logger.LogError(LogEvents.HandlingOrderNotProcessed, "Could not find a braintree transaction with the ID {transactionId}", message.TransactionId);
                    
                    else
                        model.Last4Digits = transaction.CreditCard.LastFour;
                }

                var content = await _razorViewRenderer.RenderViewToStringAsync("~/Emailing/Templates/Views/FailedOrderEmail.cshtml", model);

                // Send an email to the client
                await context.SendLocal(new SendEmail()
                {
                    EmailContent = content,
                    Recipient = message.UserEmail,
                    Subject = "Citycyles Failed Order"
                });

                booking.Status = BookingStatus.Declined;
                // Update the entity
                await _bookingRepo.UpdateAsync(booking);
            }
        }
    }
}
