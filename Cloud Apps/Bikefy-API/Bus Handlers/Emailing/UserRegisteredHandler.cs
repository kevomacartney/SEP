﻿using System;
using System.Threading.Tasks;
using CityCycles.Api.Emailing.Models;
using CityCycles.Api.Services;
using CityCycles.Composite.BusMessages.API;
using CityCycles.Composite.BusMessages.Management;
using CityCyclesAPI.Emailing;
using Microsoft.Extensions.DependencyInjection;
using NServiceBus;

namespace CityCycles.Api.BusHandlers.Emailing
{
    public class UserRegisteredHandler : IHandleMessages<UserRegistered>
    {
        private readonly IRazorViewToStringRenderer _razorViewRenderer;

        private readonly IClientManager _clientManager;

        public UserRegisteredHandler(IServiceProvider serviceProvider)
        {
            _razorViewRenderer = serviceProvider.GetService<IRazorViewToStringRenderer>();

            _clientManager = serviceProvider.GetService<IClientManager>();
        }

        public async Task Handle(UserRegistered message, IMessageHandlerContext context)
        {
            // Load the order email template
            var register = await _razorViewRenderer.RenderViewToStringAsync("~/Emailing/Templates/Views/RegisterEmail.cshtml", new RegisterEmailModel());

            // Get the user
            var user = await _clientManager.GetClientByIdAsync(message.UserId);

            // Create request to send email to user
            var emailRequest = new SendEmail()
            {
                EmailContent = register,
                Recipient = user.Email,
                Subject = "You're now registered!"
            };

            // Send the request
            await context.SendLocal(emailRequest);
        }
    }
}
