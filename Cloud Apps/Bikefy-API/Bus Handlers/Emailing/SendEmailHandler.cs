﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;
using CityCycles.Composite.BusMessages.Management;
using CityCyclesAPI.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NServiceBus;

namespace CityCyclesAPI.BusHandlers.Emailing
{
    public class SendEmailHandler : IHandleMessages<SendEmail>
    {
        private static class LogEvents
        {
            public static readonly EventId HandlingSendEmailRequest = new EventId(1, nameof(Handle));
        }

        private readonly ILogger _logger;
        private readonly SmtpClient _smtpClient;

        public SendEmailHandler(IServiceProvider serviceProvider)
        {
            _logger = serviceProvider.GetService<ILogger<SendEmailHandler>>();
            _smtpClient = serviceProvider.GetService<SmtpClient>();
        }

        public async Task Handle(SendEmail message, IMessageHandlerContext context)
        {
            using (_logger.BeginScope(new Dictionary<string, object>()
            {
                ["userEmail"] = message.Recipient,
                ["EmailSubject"] = message.Subject
            }))
            {
                _logger.LogInformation(LogEvents.HandlingSendEmailRequest, "Request to dispatch email received");

                // Prepare the email
                MailMessage mail = new MailMessage("donotreply@cityCycles.co.uk", message.Recipient)
                {
                    Subject = message.Subject,
                    IsBodyHtml = true,
                    Body = message.EmailContent
                };

                //  check if request has included attachments
                if (message.Attachments != null && message.Attachments.HasValue)
                {
                    _logger.LogInformation(LogEvents.HandlingSendEmailRequest, "The email includes {attachmentCount} attachments.", message.Attachments.Value.Count);

                    foreach (var attachment in message.Attachments.Value)
                    {
                        var stream = new System.IO.MemoryStream(attachment.Value);

                        // attach a pdf
                        mail.Attachments.Add(new Attachment(stream, attachment.Key));

                        _logger.LogInformation(LogEvents.HandlingSendEmailRequest, "Attached '{attachment}' to email", attachment.Key);
                    }
                }


                // Send the mail
                _smtpClient.Send(mail);

                _logger.LogInformation(LogEvents.HandlingSendEmailRequest, "Email was successfully send to {email} using SMTP", message.Recipient);
            }
        }
    }
}
