﻿using System;
using System.Threading.Tasks;
using CityCycles.Api.Services;
using CityCycles.Composite.BusMessages.Management;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using NServiceBus;
using System.Collections.Generic;

namespace CityCycles.Api.BusHandlers.Dock
{
    public class DeleteDockAccountHandler : IHandleMessages<DeleteDockAccount>
    {
        public static class LogEvents
        {
            public static readonly EventId DeletingDockAccount = new EventId(1, nameof(Handle));
        }

        private readonly ILogger _logger;
        private readonly IClientManager _clientManager;

        public DeleteDockAccountHandler(IServiceProvider serviceProvider)
        {
            _logger = serviceProvider.GetService<ILogger<DeleteDockAccountHandler>>();
            _clientManager = serviceProvider.GetService<IClientManager>();
        }

        public async Task Handle(DeleteDockAccount message, IMessageHandlerContext context)
        {
            using (_logger.BeginScope(new Dictionary<string, object>()
            {
                ["dockId"] = message.DockId
            }))
            {
                _logger.LogInformation(LogEvents.DeletingDockAccount, $"Delete dock account for dock {message.DockId}");

                try
                {
                    await _clientManager.DeleteUserAsync(message.DockId);
                    _logger.LogInformation(LogEvents.DeletingDockAccount, $"Dock account for dock {message.DockId} has been deleted");
                }
                catch (InvalidOperationException e)
                {
                    _logger.LogWarning(LogEvents.DeletingDockAccount, e, "Could not delete dock account, InvalidOperation");
                }
            }
        }
    }
}
