﻿using System;
using System.Threading.Tasks;
using CityCycles.Composite.BusMessages.Management;
using CityCycles.Composite.Data;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using NServiceBus;
using System.Collections.Generic;
using CityCycles.Api.Services;
using CityCycles.Composite.Data.ManagementData.User;

namespace CityCyclesAPI.BusHandlers.Dock
{
    public class CreateDockAccountHandler : IHandleMessages<CreateDockAccount>
    {
        private readonly ILogger _logger;
        private readonly ManagementDataContext _context; 
        private readonly PasswordHasher _passwordHasher;

        public CreateDockAccountHandler(IServiceProvider serviceProvider)
        {
            _logger = serviceProvider.GetService<ILogger<CreateDockAccountHandler>>();
            _context = serviceProvider.GetService<ManagementDataContext>();

        }

        public async Task Handle(CreateDockAccount message, IMessageHandlerContext context)
        {
            using (_logger.BeginScope(new Dictionary<string, object>()
            {
                ["dockId"] = message.DockId
            }))
            {
                _logger.LogInformation("Creating account for dock {dockName}/{dockId}", message.DockName, message.DockId);

                var user = new User()
                {
                    Id = message.DockId,
                    FirstName = message.DockName,
                    PasswordHash = _passwordHasher.HashPassword(message.Password),
                    SecurityStamp = Guid.NewGuid().ToString()
                };

                // Add the dock as as user to the database
                await _context.AddAsync(user);

                _logger.LogInformation("Account for dock {dockName}/{dockId} was successfully created", message.DockName, message.DockId);
            }
        }
    }
}
