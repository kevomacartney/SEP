﻿using System;
using CityCycles.Composite.BusMessages.Management;
using CityCycles.Composite.Configurations;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NServiceBus;
using NServiceBus.Logging;
using NServiceBus.Serilog;

namespace CityCycles.Api.Extensions.ServiceBus
{
    public static class ServiceBus
    {
        /// <summary>
        /// Adds the service bus to the pipeline
        /// </summary>
        /// <param name="services">Services.</param>
        /// <param name="configuration">Configuration provider. </param>
        public static void AddServiceBus(this IServiceCollection services, IConfiguration configuration)
        {
            // Load the service bus configuration
            var serviceBusConfig = new ServiceBusConfiguration();
            configuration.Bind("ServiceBus", serviceBusConfig);

            // Default queue and error queue configuration based on the configuration file
            var endpointConfiguration = new EndpointConfiguration(serviceBusConfig.APIQueue);
            endpointConfiguration.SendFailedMessagesTo(serviceBusConfig.ErrorQueue);

            // More configuration
            endpointConfiguration.UsePersistence<InMemoryPersistence>();
            endpointConfiguration.UseSerialization<NewtonsoftSerializer>();
            endpointConfiguration.EnableInstallers();
            endpointConfiguration.UseContainer<ServicesBuilder>(
                customizations: customizations =>
                {
                    customizations.ExistingServices(services);
                });
              

            LogManager.Use<SerilogFactory>();

            // Configure bus to use blob storage as it's databus
            var dataBus = endpointConfiguration.UseDataBus<AzureDataBus>();
            dataBus.ConnectionString(serviceBusConfig.BlobStorageConStr);
            dataBus.Container("service-bus-blob");
            dataBus.DefaultTTL(Convert.ToInt64(TimeSpan.FromHours(5).TotalSeconds));
            dataBus.MaxRetries(5);
            dataBus.NumberOfIOThreads(5);

            // Use azure service bus transport
            var transport = endpointConfiguration.UseTransport<AzureServiceBusTransport>();
            transport.ConnectionString(serviceBusConfig.VmeServiceBusConStr);

            // Load all message automatically
            var routing = transport.Routing();

            // Connect to the endpoint
            var endpoint = Endpoint.Start(endpointConfiguration).Result;

            // Register the configuration and end point instance
            services.AddSingleton(serviceBusConfig);
            services.AddSingleton(endpoint);

        }
    }
}
