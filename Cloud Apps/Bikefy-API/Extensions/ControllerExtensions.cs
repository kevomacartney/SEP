﻿using CityCyclesAPI.Api;
using Microsoft.AspNetCore.Mvc;

using System;
namespace CityCyclesAPI.Extensions
{
    public static class ControllerExtensions
    {
        public static int StatusCodeToInt(this Controller controller, ApiStatusCodes statusCode)
        {
            return (int)statusCode;
        }
    }
}
