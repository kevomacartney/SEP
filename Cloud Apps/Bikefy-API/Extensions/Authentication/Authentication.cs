using System;
using System.Linq;
using CityCycles.Api.Services;
using CityCycles.Composite.Model.Shared.Settings;
using CityCycles.Composite.Shared_Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace CityCycles.Api.Extensions.Authentication
{
    public static class Authentication
    {
        /// <summary>
        /// Adds the Json Web Token(JWT) authentication to the pipeline
        /// </summary>
        /// <param name="services">Services.</param>
        public static void ConfigureAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            var secretKeys = new SecretKeys();
            configuration.GetSection("SecretKeys").Bind(secretKeys);

            // Create an instance of the encryption service and pass the certificate serial number
            var encryptService = new EncryptionService("00E7EB8C24190E2187", secretKeys);

            // Register the instance as singletons
            services.AddSingleton(encryptService);
            services.AddSingleton(secretKeys);


            var validator = new AudienceValidator((audiences, securityToken, validationParameters) =>
                                                   audiences.Any(s => s == "CityCycles.Api"));

            // Add the authentication to the pipeline
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = encryptService.TokenSignKey.Key,
                    ValidateIssuer = true,
                    ValidIssuer = "Bikefy Api",
                    ValidateAudience = true,
                    AudienceValidator = validator,
                    ValidateLifetime = true,
                    RequireExpirationTime = true,
                    ClockSkew = TimeSpan.Zero,
                    RequireSignedTokens = true
                };
            });
        }
    }
}
