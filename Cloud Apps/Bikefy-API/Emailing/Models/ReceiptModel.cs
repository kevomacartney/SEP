﻿using System;
using Braintree;

namespace CityCycles.Api.Emailing.Models
{
    public class ReceiptModel 
    {
        /// <summary>
        /// Gets or sets the booking identifier.
        /// </summary>
        /// <value>The booking identifier.</value>
        public Guid BookingId { get; set; }

        /// <summary>
        /// Gets or sets the first name identifier.
        /// </summary>
        /// <value>The user's first name.</value>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name identifier.
        /// </summary>
        /// <value>The user's last name.</value>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the start date identifier.
        /// </summary>
        /// <value>The start date identifier.</value>
        public DateTime OrderDate { get; set; }

        /// <summary>
        /// Gets or sets the dock location identifier.
        /// </summary>
        /// <value>The dock location identifier.</value>
        public String DockLocation { get; set; }

        /// <summary>
        /// Gets or sets the booking fee.
        /// </summary>
        /// <value>The booking fee.</value>
        public string BookingFee { get; set; }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        /// <value>The total.</value>
        public string TimeTotal { get; set; }

        /// <summary>
        /// Gets or sets the grand total.
        /// </summary>
        /// <value>The grand total.</value>
        public string GrandTotal { get; set; }

        /// <summary>
        /// Gets or sets the order quantity.
        /// </summary>
        /// <value>The order quantity.</value>
        public string OrderQuantity { get; set; }

        /// <summary>
        /// Gets or sets the last4 digits of the credit card used.
        /// </summary>
        /// <value>The last4 digits.</value>
        public string Last4Digits { get; set; }

        public string ReceiptUrl { get; set; }

        public CreditCardCardType CardType { get; set; }
    }
}
