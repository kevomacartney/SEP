﻿using System;
namespace CityCycles.Api.Emailing.Models
{
    public interface IBaseEmailModel
    {
        /// <summary>
        /// Gets or sets the preview message.
        /// </summary>
        /// <value>The preview message.</value>
        string PreviewMessage { get; set; }
    }
}
