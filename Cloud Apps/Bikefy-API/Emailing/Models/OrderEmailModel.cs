﻿using System;
using CityCycles.Api.Emailing.Models;

namespace CityCycles.Api.Emailing.Models
{
    public class OrderEmailModel : IBaseEmailModel
    {
        public string PreviewMessage { get; set; } = "Thanks for using City Cycles";
    }
}
