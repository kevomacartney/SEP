﻿using System;
namespace CityCycles.Api.Emailing.Models
{
    public class FailedOrderEmail : IBaseEmailModel
    {
        /// <summary>
        /// Gets or sets the preview message.
        /// </summary>
        /// <value>The preview message.</value>
        public string PreviewMessage { get; set; } = "There was an error while processing your order";

        /// <summary>
        /// Gets or sets the name of the dock.
        /// </summary>
        /// <value>The name of the dock.</value>
        public string DockName { get; set; }

        /// <summary>
        /// Gets or sets the start time.
        /// </summary>
        /// <value>The start time.</value>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>The end date.</value>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets the reason.
        /// </summary>
        /// <value>The reason.</value>
        public string Reason { get; set; }

        /// <summary>
        /// Gets or sets the last4 digits.
        /// </summary>
        /// <value>The last4 digits.</value>
        public string Last4Digits { get; set; }
    }
}
