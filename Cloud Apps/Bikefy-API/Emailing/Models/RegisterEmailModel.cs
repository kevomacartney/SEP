﻿using System;
using CityCycles.Api.Emailing.Models;

namespace CityCycles.Api.Emailing.Models
{
    public class RegisterEmailModel : IBaseEmailModel
    {
        public string PreviewMessage { get; set; } = "Thank you for registering with City Cycles!";
    }
}
