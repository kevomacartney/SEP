﻿using System.Threading.Tasks;

namespace CityCyclesAPI.Emailing
{
    /// <summary>
    /// Used to render razor view to prepare them for emailing
    /// </summary>
    public interface IRazorViewToStringRenderer
    {
        /// <summary>
        /// Renders the view the given view with the model and returns the raw html string
        /// </summary>
        /// <returns>The view to string async.</returns>
        /// <param name="viewName">View name.</param>
        /// <param name="model">Model.</param>
        /// <typeparam name="TModel">The 1st type parameter.</typeparam>
        Task<string> RenderViewToStringAsync<TModel>(string viewName, TModel model);
    }
}