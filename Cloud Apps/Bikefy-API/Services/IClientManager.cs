﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using CityCycles.Composite.Data.ManagementData.User;
using CityCycles.Composite.Model.Auth;
using Microsoft.AspNetCore.Identity;

namespace CityCycles.Api.Services
{
    public interface IClientManager : IDisposable
    {
        /// <summary>
        /// Creates the new user.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <return> The user's Identifier</return>
        Task<Guid> CreateNewUserAsync(CreateAccountModel model);

        /// <summary>
        /// Deletes the user account
        /// </summary>
        /// <returns>.</returns>
        /// <exception cref="InvalidOperationException">Thrown when no user with the specified user id could be found</exception>
        /// <param name="userId">User identifier.</param>
        Task DeleteUserAsync(Guid userId);

        /// <summary>
        /// Adds a claim to the user
        /// </summary>
        /// <returns>The claims async.</returns>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="claim">Claim.</param>
        Task AddClaimsAsync(Guid clientId, Claim claim);

        /// <summary>
        /// Adds a user to a role
        /// </summary>
        /// <returns>The roles async.</returns>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="roles">Roles.</param>
        Task AddRolesAsync(Guid clientId, string[] roles);

        /// <summary>
        /// Adds a user to a role
        /// </summary>
        /// <returns>The role async.</returns>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="role">Role.</param>
        Task<IdentityResult> AddRoleAsync(Guid clientId, string role);

        /// <summary>
        /// Adds a user to a role
        /// </summary>
        /// <returns>The role async.</returns>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="role">Role.</param>
        Task<IdentityResult> AddRoleAsync(Guid clientId, Role role);

        /// <summary>
        /// Checks the vme key.
        /// </summary>
        /// <returns><c>true</c>, if vme key was correct, <c>false</c> otherwise.</returns>
        /// <param name="user">User.</param>
        /// <param name="password">The password</param>
        bool CheckPasssword(User user, string password);

        /// <summary>
        /// Creates a new role for users
        /// </summary>
        /// <returns></returns>
        /// <param name="role">Role.</param>
        Task CreateRoleAsync(Role role);

        /// <summary>
        /// Creates the user claims identity async.
        /// </summary>
        /// <returns>The user claims identity.</returns>
        /// <param name="clientId">Client identifier.</param>
        Task<ClaimsIdentity> CreateUserClaimsIdentityAsync(Guid clientId);

        /// <summary>
        /// Gets all claims for a user.
        /// </summary>
        /// <returns><A collection of user <see cref="Claim"/></returns>
        /// <param name="clientId">Client identifier.</param>
        Task<List<Claim>> GetClaimsAsync(Guid clientId);

        /// <summary>
        /// Gets the client by their identifier.
        /// </summary>
        /// <returns>The client </returns>
        /// <param name="clientId">Client identifier.</param>
        Task<User> GetClientByIdAsync(Guid clientId);

        /// <summary>
        /// Gets the client by username.
        /// </summary>
        /// <returns>The client by username.</returns>
        /// <param name="username">Username.</param>
        Task<User> GetClientByUsernameAsync(string username);

        /// <summary>
        /// Gets all roles for a user
        /// </summary>
        /// <returns>A collection of user <see cref="Role"/>.</returns>
        /// <param name="clientId">Client identifier.</param>
        Task<List<string>> GetRolesAsync(Guid clientId);

        /// <summary>
        /// Gets the security stamp.
        /// </summary>
        /// <returns>The security stamp.</returns>
        /// <param name="clientId">Client identifier.</param>
        Task<string> GetSecurityTokenAsync(Guid clientId);

        /// <summary>
        /// Updates the security token async.
        /// </summary>
        /// <returns>The security token async.</returns>
        /// <param name="clientId">Client identifier.</param>
        Task UpdateSecurityTokenAsync(Guid clientId);

        /// <summary>
        /// wether or not user has been locked out
        /// </summary>
        /// <returns><c>true</c>, if user is locked out, <c>false</c> otherwise.</returns>
        /// <param name="clientId">Client identifier.</param>
        Task<bool> IsLockedOutAsync(Guid clientId);

        /// <summary>
        /// Locks out a user.
        /// </summary>
        /// <returns></returns>
        /// <param name="clientId">Client identifier.</param>
        Task<SignInResult> LockOutAsync(Guid clientId);

        /// <summary>
        /// Removes the claim from user.
        /// </summary>
        /// <returns>The claim async.</returns>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="claim">Claim.</param>
        Task<IdentityResult> RemoveClaimAsync(Guid clientId, Claim claim);

        /// <summary>
        /// Will reset a user lockout.
        /// </summary>
        /// <returns></returns>
        /// <param name="user">User.</param>
        Task ResetLockoutAsync(User user);

        /// <summary>
        /// if the role the exists.
        /// </summary>
        /// <returns><c>true</c>, if the role exists, <c>false</c> otherwise.</returns>
        /// <param name="role">Role.</param>
        Task<bool> RoleExistsAsync(string role);

        /// <summary>
        /// Gets the role by identifier async.
        /// </summary>
        /// <returns>The role by identifier async.</returns>
        /// <param name="roleId">Role identifier.</param>
        Task<Role> GetRoleByIdAsync(Guid roleId);

        /// <summary>
        /// Updates a user
        /// </summary>
        /// <returns>The async.</returns>
        /// <param name="user">User.</param>
        Task UpdateAsync(User user);

        /// <summary>
        /// Signs in a user with a given key.
        /// </summary>
        /// <returns><see cref="SignInResult"/> indicating the successfulness </returns>
        /// <param name="id">Identifier.</param>
        /// <param name="providedPassword">Provided key.</param>
        Task<SignInResult> PasswordSigninAsync(string id, string providedPassword);

        /// <summary>
        /// Will try and authenticate an application.
        /// </summary>
        /// <param name="appId"> The application's identifier s</param>
        /// <param name="accessKey"> The application's access key</param>
        /// <returns></returns>
        Task<SignInResult> ApplicationSignInAsync(Guid appId, string accessKey);

    }
}