﻿using System;
namespace CityCyclesAPI.Services
{
    public interface IPdfService
    {
        /// <summary>
        /// Generates a pdf from the html and returns the binary data of the pdf
        /// </summary>
        /// <returns>The pdf.</returns>
        /// <param name="html">Html.</param>
        byte[] GeneratePdf(string html);
    }
}
