﻿using System;
using Braintree;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CityCycles.Composite.Braintree
{
    public static class BraintreeExtension
    {
        /// <summary>
        /// Adds the braintree  to the service collection
        /// </summary>
        /// <param name="serverCollection">Server collection.</param>
        /// <param name="configuration">Configuration.</param>
        public static void AddBraintree(this IServiceCollection serverCollection, IConfiguration configuration)
        {
            // Parameter check
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            if (serverCollection == null)
                throw new ArgumentNullException(nameof(serverCollection));


            // Bind the braintree configuration instance with the configuration
            var braintreeConfig = new BrantreeConfig();
            configuration.Bind("Braintree", braintreeConfig);


            // Initialise the gateway
            var gateway = new BraintreeGateway
            {
                Environment = braintreeConfig.Environment == "Sandbox" ? global::Braintree.Environment.SANDBOX : global::Braintree.Environment.DEVELOPMENT,
                MerchantId = braintreeConfig.MarchantId,
                PublicKey = braintreeConfig.PublicKey,
                PrivateKey = braintreeConfig.PrivateKey
            };

            // Add the gatway instance to the services
            serverCollection.AddSingleton<IBraintreeGateway>(gateway);
        }
    }
}
