﻿namespace CityCycles.Composite.Braintree
{
    public class BrantreeConfig
    {
        /// <summary>
        /// Gets or sets the marchant identifier.
        /// </summary>
        /// <value>The marchant identifier.</value>
        public string MarchantId { get; set; }

        /// <summary>
        /// Gets or sets the private key.
        /// </summary>
        /// <value>The private key.</value>
        public string PrivateKey { get; set; }

        /// <summary>
        /// Gets or sets the public key.
        /// </summary>
        /// <value>The public key.</value>
        public string PublicKey { get; set; }

        /// <summary>
        /// Gets or sets the environment.
        /// </summary>
        /// <value>The environment.</value>
        public string Environment { get; set; }
    }
}
