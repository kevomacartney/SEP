﻿using System;

namespace CityCycles.Composite.Exceptions
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(string identifier)
           : base(String.Format("Invalid Entity Identifier: {0}", identifier))
        {
        }

        public EntityNotFoundException(string identifier, Exception innerException)
            : base(String.Format("Invalid Entity Identifier: {0}", identifier), innerException) { }
    }
}
