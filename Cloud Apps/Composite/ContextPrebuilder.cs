﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace CityCycles.Composite
{
    public class ContextPrebuilder
    {
        /// <summary>
        /// Gets Configuration instance
        /// </summary>
        public IConfiguration Configuration { get; set; }

        public ContextPrebuilder()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                .AddJsonFile("appsettings.json");

            Configuration = builder.Build();
        }

        /// <summary>
        /// Builds the contexts options using a string from the appconfig
        /// </summary>
        /// <typeparam name="TContext"></typeparam>
        /// <returns></returns>
        public DbContextOptionsBuilder<TContext> BuildContext<TContext>(string conntextionStrName) where TContext : DbContext
        {
            if (string.IsNullOrEmpty(Configuration.GetConnectionString(conntextionStrName)))
                throw new NullReferenceException("Connection string not valid");

            var optionsBuilder = new DbContextOptionsBuilder<TContext>();
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString(conntextionStrName));
            return optionsBuilder;
        }
    }
}
