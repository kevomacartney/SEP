﻿namespace CityCycles.Composite.Enums
{
    public enum BikeStatus
    {
        Available,
        Busy,
        Lost
    }
}
