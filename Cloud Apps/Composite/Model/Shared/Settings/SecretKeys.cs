﻿using System;

namespace CityCycles.Composite.Model.Shared.Settings
{
    /// <summary>
    /// Secret keys from configuration
    /// </summary>
    public class SecretKeys
    {
        /// <summary>
        /// Gets or sets the key encryption. Used for encrypting keys
        /// </summary>
        /// <value>The key encryption.</value>
        public String KeyEncryption { get; set; }

        /// <summary>
        /// Gets or sets the token sign key. Used for signing tokens
        /// </summary>
        /// <value>The token sign key.</value>
        public String TokenSignKey { get; set; }
    }
}
