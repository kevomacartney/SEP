﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CityCycles.Composite.Model.Pricing
{
    public class PricingRequest
    {
        /// <summary>
        /// Gets or sets Identifier
        /// </summary>
        public String Label { get; set; }

        /// <summary>
        /// Gets or sets Identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets hourly rate
        /// </summary>
        [Required(ErrorMessage = "Please enter hourly rate.")]
        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public Decimal Hourly { get; set; }

        /// <summary>
        /// Gets or sets overdue hourly rate
        /// </summary>
        [Required(ErrorMessage = "Please enter overdue hourly rate.")]
        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public Decimal OverdueHourly { get; set; }

        /// <summary>
        /// Gets or sets booking fee
        /// </summary>
        [Required(ErrorMessage = "Please enter booking fee.")]
        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public Decimal BookingFee { get; set; }

        /// <summary>
        /// Gets or sets booking fee
        /// </summary>
        [Required(ErrorMessage = "Please enter missed booking fee.")]
        [DisplayFormat(DataFormatString = "{0:0.00}", ApplyFormatInEditMode = true)]
        public Decimal MissedBookingFee { get; set; }

        /// <summary>
        /// Gets or sets ID 
        /// </summary>
        public Guid UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets ID 
        /// </summary>
        public DateTime UpdatedAt { get; set; }

        /// <summary>
        /// Gets or sets the discount rate.
        /// </summary>
        /// <value>The discount rate.</value>
        public decimal DiscountRate { get; set; }
    }
}
