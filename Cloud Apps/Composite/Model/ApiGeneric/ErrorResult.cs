using System;

namespace CityCycles.Composite.Model.ApiGeneric
{
    [Obsolete]
    class ErrorResult
    {
        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>The error message.</value>
        public string ErrorMessage { get; set; }

    }
}
