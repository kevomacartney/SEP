using System.ComponentModel.DataAnnotations;

namespace CityCycles.Composite.Model.Auth
{
    public class AuthenticateModel
    {
        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        /// <value>The username.</value>
        [Required(ErrorMessage = "Please provide a user name")]
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        [Required(ErrorMessage = "Please provide a password")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets boolean indicating if the user wants the browser to remember them
        /// </summary>
        public bool Remember { get; set; }
    }
}
