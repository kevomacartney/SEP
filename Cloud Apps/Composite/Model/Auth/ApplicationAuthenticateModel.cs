﻿using System;

namespace CityCycles.Composite.Model.Auth
{
    public class ApplicationAuthenticateModel
    {
        /// <summary>
        /// Gets or sets the app Id
        /// </summary>
        public Guid AppId { get; set; }

        /// <summary>
        /// Gets or sets the applications access key
        /// </summary>
        public string AccessKey { get; set; }
    }
}
