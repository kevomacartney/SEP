﻿using System.ComponentModel.DataAnnotations;

namespace CityCycles.Composite.Model.Auth
{
    public class CreateAccountModel
    {
        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The FirstName.</value>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the Last name.
        /// </summary>
        /// <value>The LastName.</value>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        [Required]
        public string Password { get; set; }
    }
}
