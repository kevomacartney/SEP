﻿using System;
using CityCycles.Composite.Enums;

namespace CityCycles.Composite.Model.Bike
{
    public class BikeModel
    {
        public BikeModel()
        {
        }

        /// <summary>
        /// Gets or sets the bike identifier.
        /// </summary>
        /// <value>The bike identifier.</value>
        public Guid BikeId { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public BikeStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the dock identifier.
        /// </summary>
        /// <value>The dock identifier.</value>
        public Guid DockId { get; set; }

        /// <summary>
        /// Gets or sets the last update.
        /// </summary>
        /// <value>The last update.</value>
        public DateTime LastUpdate { get; set; }
    }
}
