﻿using System;
using System.ComponentModel.DataAnnotations;
using CityCycles.Composite.Enums;

namespace CityCycles.Composite.Model.Bike
{
    /// <summary>
    /// Bike request to create a new dock
    /// </summary>
    public class BikeRequest
    {
        /// <summary>
        /// Gets or sets the bike identifier.
        /// </summary>
        /// <value>The bike identifier.</value>
        public Guid BikeId { get; set; }

        /// <summary>
        /// Gets or sets the dock identifier.
        /// </summary>
        /// <value>The dock identifier.</value>
        [Required]
        public Guid DockId { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public BikeStatus Status { get; set; } = BikeStatus.Available;
    }
}
