﻿using System;
using System.Collections.Generic;
using CityCycles.Composite.Model.Bike;

namespace CityCycles.Composite.Model.Dock
{
    public class DockStatus
    {
        public DockStatus()
        {
            Bikes = new HashSet<BikeModel>();
        }

        /// <summary>
        /// Gets or sets the bike status.
        /// </summary>
        /// <value>The bike statuses.</value>
        public ICollection<BikeModel> Bikes { get; set; }

        /// <summary>
        /// Gets or sets the dock identifier.
        /// </summary>
        /// <value>The dock identifier.</value>
        public Guid DockId { get; set; }

        /// <summary>
        /// Gets or sets the name of the dock.
        /// </summary>
        /// <value>The name of the dock.</value>
        public string DockName { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        /// <value>The location.</value>
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the maximum capacity.
        /// </summary>
        /// <value>The maximum capacity.</value>
        public int MaximumCapacity { get; set; }
    }
}
