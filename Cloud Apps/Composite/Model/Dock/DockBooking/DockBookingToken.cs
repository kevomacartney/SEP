﻿using System;
namespace CityCycles.Composite.Model.Dock
{
    /// <summary>
    /// A dock booking token, used to create a booking
    /// </summary>
    public class DockBookingToken
    {
        /// <summary>
        /// Gets or sets the payload which is json serialized string
        /// </summary>
        /// <value>The payload.</value>
        public string Payload { get; set; }

        /// <summary>
        /// Gets or sets the signature.
        /// </summary>
        /// <value>The signature.</value>
        public string Signature { get; set; }
    }
}
