﻿using System;
namespace CityCycles.Composite.Model.Dock
{
    /// <summary>
    /// Event send to API by dock to close a booking
    /// </summary>
    public class DockCloseBookingEvent
    {
        /// <summary>
        /// Gets or sets the date the booking was closed
        /// </summary>
        /// <value>The created on.</value>
        public DateTime ClosedOn { get; set; }

        /// <summary>
        /// Gets or sets the security token.
        /// </summary>
        /// <value>The security token.</value>
        public string SecurityToken { get; set; }

        /// <summary>
        /// Gets or sets the dock identifier.
        /// </summary>
        /// <value>The dock identifier.</value>
        public Guid BikeId { get; set; }
    }
}
