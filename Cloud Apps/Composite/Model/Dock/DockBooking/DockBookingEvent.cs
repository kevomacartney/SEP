﻿using System;
namespace CityCycles.Composite.Model.Dock
{
    /// <summary>
    /// Event send to API by dock to create a booking
    /// </summary>
    public class DockBookingEvent
    {
        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>The user identifier.</value>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the date the booking was created
        /// </summary>
        /// <value>The created on.</value>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the security token.
        /// </summary>
        /// <value>The security token.</value>
        public string SecurityToken { get; set; }

        /// <summary>
        /// Gets or sets the bike identifier.
        /// </summary>
        /// <value>The bike identifier.</value>
        public Guid BikeId { get; set; }
    }
}
