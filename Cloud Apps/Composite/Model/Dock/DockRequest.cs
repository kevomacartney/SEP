﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CityCycles.Composite.Model.Bike;

namespace CityCycles.Composite.Model.Dock
{
    /// <summary>
    /// A dock create request
    /// </summary>
    public class DockRequest
    {
        public DockRequest()
        {
            Bikes = new HashSet<BikeModel>();
        }

        /// <summary>
        /// Gets or sets the bike status.
        /// </summary>BB
        /// <value>The bike statuses.</value>
        public ICollection<BikeModel> Bikes { get; set; }

        /// <summary>
        /// Gets or sets the latitude.
        /// </summary>
        /// <value>The latitude.</value>
        [Required(ErrorMessage = "Please enter a latitude.")]
        public decimal Latitude { get; set; }

        /// <summary>
        /// Gets or sets the longitude.
        /// </summary>
        /// <value>The longitude.</value>
        [Required(ErrorMessage =  "Please enter a longitude.")]
        public decimal Longitude { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        [Required (ErrorMessage = "Please enter a dock name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the capacity.
        /// </summary>
        /// <value>The capacity.</value>
        [Required (ErrorMessage = "You must specify dock capacity")]
        public int Capacity { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public Guid Id { get; set; }
    }
}
