﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CityCycles.Composite.Model.Booking
{
    public class BookingRequest
    {
        /// <summary>
        /// Gets or sets the user identifier for this booking.
        /// </summary>
        /// <value>The user identifier.</value>
        [Required(ErrorMessage = "User must be specified for the booking")]
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>The start date.</value>
        [Required(ErrorMessage = "Please enter the end date.")]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>The end date.</value>
        [Required(ErrorMessage = "Please enter the end date.")]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets the bike identifier.
        /// </summary>
        /// <value>The bike identifier.</value>
        [Required(ErrorMessage = "Dock must specified for the booking")]
        public Guid DockId { get; set; }

        /// <summary>
        /// Gets or sets the pricing scheme identifier being used.
        /// </summary>
        /// <value>The pricing.</value>
        public Guid Pricing { get; set; }
    }
}
