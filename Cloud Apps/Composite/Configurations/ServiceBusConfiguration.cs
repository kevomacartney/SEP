﻿namespace CityCycles.Composite.Configurations
{
    public class ServiceBusConfiguration
    {
        /// <summary>
        /// Gets or sets the connections string to the service bus
        /// </summary>
        public string VmeServiceBusConStr { get; set; }

        /// <summary>
        /// Gets or sets the Management web role queue
        /// </summary>
        public string ManagementQueue { get; set; }

        /// <summary>
        /// Gets or sets the API worker role queue
        /// </summary>
        public string APIQueue { get; set; }

        /// <summary>
        /// Gets or sets the dock queue.
        /// </summary>
        /// <value>The dock queue.</value>
        public string DockQueue { get; set; }

        /// <summary>
        /// Gets or sets the Error queue
        /// </summary>
        public string ErrorQueue { get; set; }

        /// <summary>
        /// Gets or sets the BLOB storage connection string.
        /// </summary>
        /// <value>The BLOB storage con string.</value>
        public string BlobStorageConStr { get; set; }
    }
}
