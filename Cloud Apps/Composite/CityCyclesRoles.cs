﻿namespace CityCycles.Composite
{
    public static class CityCyclesRoles
    {
        #region Application roles

        public const string AccountManagerRole = "CityCycles.Managers.Account";

        public const string CityCyclesManager = "CityCycles.Staff.Managers";

        public const string CityCyclesDocks  = "CityCycles.Staff.Docks";
        #endregion
    }
}
