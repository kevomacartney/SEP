﻿using CityCycles.Composite.Data;
using Microsoft.EntityFrameworkCore.Design;

namespace CityCycles.Composite
{
    public class ManagementDataContextFactory : IDesignTimeDbContextFactory<ManagementDataContext>
    {
        private ContextPrebuilder PreBuilder { get; set; }

        public ManagementDataContextFactory()
        {
            PreBuilder = new ContextPrebuilder();
        }

        public ManagementDataContext CreateDbContext(string[] args)
        {
            var optionsBuilder = PreBuilder.BuildContext<ManagementDataContext>("ManagementDb");

            return new ManagementDataContext(optionsBuilder.Options);
        }

    }
}
