﻿using System;
using System.Collections.Generic;

namespace CityCycles.Composite.Data.ManagementData
{
    public class Pricing
    {
        public Pricing()
        {
            Bookings = new HashSet<Booking>();
        }

        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the pricing label
        /// </summary>
        /// <value>The name.</value>
        public string Label { get; set; }

        /// <summary>
        /// Gets or sets the identifier of the user that updated the record
        /// </summary>
        /// <value>The updatedby.</value>
        public Guid Updatedby { get; set; }

        /// <summary>
        /// Gets or sets the date the pricing was updated on
        /// </summary>
        /// <value>The update at.</value>
        public DateTime UpdateAt { get; set; }

        /// <summary>
        /// Gets or sets the hourly price.
        /// </summary>
        /// <value>The hourly.</value>
        public decimal HourlyPrice { get; set; }

        /// <summary>
        /// Gets or sets the discount hourly.
        /// </summary>
        /// <value>The discount hourly.</value>
        public decimal DiscountHourlyPrice { get; set; }

        /// <summary>
        /// Gets or sets the overdue hourly price.
        /// </summary>
        /// <value>The overdue hourly.</value>
        public decimal OverdueHourlyPrice { get; set; }

        /// <summary>
        /// Gets or sets the booking fee.
        /// </summary>
        /// <value>The booking fee.</value>
        public decimal BookingFees { get; set; }

        /// <summary>
        /// Gets or sets the booking fee.
        /// </summary>
        /// <value>The booking fee.</value>
        public decimal MissedBookingFees { get; set; }

        /// <summary>
        /// Gets or sets the bookings.
        /// </summary>
        /// <value>The bookings.</value>
        public ICollection<Booking> Bookings { get; set; }
    }
}
