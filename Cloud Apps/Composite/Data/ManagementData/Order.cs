﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CityCycles.Composite.Data.ManagementData
{
    public class Order
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the date the recep
        /// </summary>
        /// <value>The created on.</value>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        /// <value>The total.</value>
        public decimal Total { get; set; }

        /// <summary>
        /// Gets or sets the items in the order.
        /// </summary>
        /// <value>The items.</value>
        public Dictionary<string, decimal> Items { get; set; }

        /// <summary>
        /// Gets or sets the bookings.
        /// </summary>
        /// <value>The bookings.</value>
        public Booking Booking { get; set; }
    }
}
