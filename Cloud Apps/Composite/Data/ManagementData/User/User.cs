﻿using System;
using System.Collections.Generic;

namespace CityCycles.Composite.Data.ManagementData.User
{
    public class User
    {
        public User()
        {
            UserRoles = new HashSet<UserRole>();
            UserClaims = new HashSet<UserClaim>();
            Bookings = new HashSet<Booking>();
        }

        /// <summary>
        /// Gets or sets the unique identifier of the user
        /// </summary>
        /// <value>The identifier.</value>
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        public string FirstName { get; set; }
        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        public string LastName { get; set; }
        /// <summary>
        /// Gets or sets the user's email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; set; }
        /// <summary>
        /// Gets or sets the password hash.
        /// </summary>
        /// <value>The password hash.</value>
        public string PasswordHash { get; set; }
        /// <summary>
        /// Gets or sets the date the account was created.
        /// </summary>
        /// <value>The date created.</value>
        public DateTime? DateCreated { get; set; }
        /// <summary>
        /// Gets or sets the last login date UTC.
        /// </summary>
        /// <value>The last login date UTC.</value>
        public DateTime? LastLoginDateUtc { get; set; }
        /// <summary>
        /// Gets or sets the date the account was closed.
        /// </summary>
        /// <value>The account closed time.</value>
        public DateTime? AccountClosedTime { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:CityCycles.Composite.Data.ManagementData.User.User"/> is account closed.
        /// </summary>
        /// <value><c>true</c> if account closed; otherwise, <c>false</c>.</value>
        public bool AccountClosed { get; set; }
        /// <summary>
        /// Gets or sets the last session identifier.
        /// </summary>
        /// <value>The last session identifier.</value>
        public string LastSessionId { get; set; }
        /// <summary>
        /// Gets or sets the security stamp.
        /// </summary>
        /// <value>The security stamp.</value>
        public string SecurityStamp { get; set; }
        /// <summary>
        /// Gets or sets the lockout end date UTC.
        /// </summary>
        /// <value>The lockout end date UTC.</value>
        public DateTime? LockoutEndDateUtc { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:CityCycles.Composite.Data.ManagementData.User.User"/> has a lockout enabled.
        /// </summary>
        /// <value><c>true</c> if lockout enabled; otherwise, <c>false</c>.</value>
        public bool LockoutEnabled { get; set; }
        /// <summary>
        /// Gets or sets the access failed count.
        /// </summary>
        /// <value>The access failed count.</value>
        public int AccessFailedCount { get; set; }

        /// <summary>
        /// Gets or sets user's contact mobile number.
        /// </summary>
        /// <value>The contact number.</value>
        public string ContactNumber { get; set; }

        /// <summary>
        /// Gets or sets a boolean indicating if the user is staff or not
        /// </summary>
        /// <value>The name of the role.</value>
        public bool IsStaff { get; set; }

        /// <summary>
        /// Gets or sets the user's roles.
        /// </summary>
        /// <value>The user roles.</value>
        public ICollection<UserRole> UserRoles { get; set; }
        /// <summary>
        /// Gets or sets the user's claims.
        /// </summary>
        /// <value>The users claims.</value>
        public ICollection<UserClaim> UserClaims { get; set; }

        /// <summary>
        /// Gets or sets the bookings that the user has made.
        /// </summary>
        /// <value>The bookings.</value>
        public ICollection<Booking> Bookings { get; set; }
    }
}
