﻿using System;
using System.Collections.Generic;

namespace CityCycles.Composite.Data.ManagementData.User
{
    /// <summary>
    /// Represents a role
    /// </summary>
    public class Role
    {
        public Role()
        {
            UserRoles = new HashSet<UserRole>();
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets the role name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets all the users in this role
        /// </summary>
        /// <value>The user roles.</value>
        public ICollection<UserRole> UserRoles { get; set; }
    }
}
