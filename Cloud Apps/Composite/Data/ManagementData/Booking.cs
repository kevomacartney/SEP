﻿using System;

namespace CityCycles.Composite.Data.ManagementData
{
    public enum BookingStatus
    {
        Receieved,
        Processed,
        Accepted,
        Declined
    }
    public class Booking
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the created on.
        /// </summary>
        /// <value>The created on.</value>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>The start date.</value>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>The end date.</value>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets the user identifier that made the booking
        /// </summary>
        /// <value>The user.</value>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the bike identifier for this booking
        /// </summary>
        /// <value>The bike identifier.</value>
        public Guid BikeId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this
        /// <see cref="T:CityCycles.Composite.Data.ManagementData.Booking"/> booking used.
        /// </summary>
        /// <value><c>true</c> if booking used; otherwise, <c>false</c>.</value>
        public bool BookingUsed { get; set; }

        /// <summary>
        /// Gets or sets the bike for this booking.
        /// </summary>
        /// <value>The bike.</value>
        public Bike Bike { get; set; }

        /// <summary>
        /// Gets or sets the user that made the booking.
        /// </summary>
        /// <value>The user.</value>
        public User.User User { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this
        /// <see cref="T:CityCycles.Composite.Data.ManagementData.Booking"/> is a dock booking.
        /// </summary>
        /// <value><c>true</c> if is dock booking; otherwise, <c>false</c>.</value>
        public bool IsDockBooking { get; set; }

        /// <summary>
        /// Gets or sets the booking identifier.
        /// </summary>
        /// <value>The booking identifier.</value>
        public Guid PricingId { get; set; }

        /// <summary>
        /// Gets or sets the booking's pricing.
        /// </summary>
        /// <value>The booking pricing.</value>
        public Pricing Pricing { get; set; }

        /// <summary>
        /// Gets or sets the order for this booking
        /// </summary>
        /// <value>The order.</value>
        public Order Order { get; set; }

        /// <summary>
        /// Gets or sets the order identifier.
        /// </summary>
        /// <value>The order identifier.</value>
        public int? OrderId { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public BookingStatus Status { get; set; } = BookingStatus.Receieved;
    }
}
