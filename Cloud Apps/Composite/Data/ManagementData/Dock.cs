﻿using System;
using System.Collections.Generic;

namespace CityCycles.Composite.Data.ManagementData
{
    public class Dock
    {
        public Dock()
        {
            Bikes = new HashSet<Bike>();
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the maximum capacity.
        /// </summary>
        /// <value>The maximum capacity.</value>
        public int MaximumCapacity { get; set; }

        /// <summary>
        /// Gets or sets the location of the dock
        /// </summary>
        /// <value>The location.</value>
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the bikes.
        /// </summary>
        /// <value>The bikes.</value>
        public ICollection<Bike> Bikes { get; set; }
    }
}
