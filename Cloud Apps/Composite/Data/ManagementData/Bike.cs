﻿using System;
using System.Collections.Generic;
using CityCycles.Composite.Enums;

namespace CityCycles.Composite.Data.ManagementData
{
    public class Bike
    {
        public Bike()
        {
            Bookings = new HashSet<Booking>();
        }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>The identifier.</value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>The status.</value>
        public BikeStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the dock identifier.
        /// </summary>
        /// <value>The dock identifier.</value>
        public Guid DockId { get; set; }

        /// <summary>
        /// Gets or sets the dock.
        /// </summary>
        /// <value>The dock.</value>
        public Dock Dock { get; set; }

        /// <summary>
        /// Gets or sets the last report time.
        /// </summary>
        /// <value>The last report.</value>
        public DateTime LastReport { get; set; }

        /// <summary>
        /// Gets or sets a set of all bookings for this bike.
        /// </summary>
        /// <value>The booking.</value>
        public ICollection<Booking> Bookings { get; set; }
    }
}
