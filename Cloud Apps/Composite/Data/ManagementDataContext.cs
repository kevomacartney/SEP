﻿using System;
using System.Collections.Generic;
using CityCycles.Composite.Data.ManagementData;
using CityCycles.Composite.Data.ManagementData.User;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace CityCycles.Composite.Data
{
    public class ManagementDataContext : DbContext
    {
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<UserClaim> UserClaims { get; set; }

        public virtual DbSet<Bike> Bikes { get; set; }
        public virtual DbSet<Booking> Bookings { get; set; }
        public virtual DbSet<Dock> Docks { get; set; }

        public virtual DbSet<Pricing> Pricings { get; set; }
        public virtual DbSet<Order> Orders { get; set; }

        public ManagementDataContext(DbContextOptions<ManagementDataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                // Set the primary key
                entity.HasKey(u => u.Id);

                // Relationship with roles
                entity.HasMany(u => u.UserRoles)
                      .WithOne(u => u.User);

                // Relationship with claims
                entity.HasMany(u => u.UserClaims)
                      .WithOne(u => u.User);
            });

            modelBuilder.Entity<UserClaim>(entity =>
            {
                // Setup the primary key
                entity.HasKey(c => c.Id);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                // Setup the primary key
                entity.HasKey(e => e.Id);
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                // Setup the primary keys
                entity.HasKey(e => new { e.RoleId, e.UserId });
            });

            modelBuilder.Entity<Bike>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.HasOne(e => e.Dock)
                      .WithMany(e => e.Bikes)
                      .HasForeignKey(e => e.DockId);
            });

            modelBuilder.Entity<Booking>(entity =>
            {
                entity.HasKey(e => e.Id);

                // Bike relationship with one to many
                entity.HasOne(e => e.Bike)
                      .WithMany(e => e.Bookings)
                      .HasForeignKey(e => e.BikeId);

                // User relationship with one to many
                entity.HasOne(e => e.User)
                      .WithMany(e => e.Bookings)
                      .HasForeignKey(e => e.UserId);

                // Pricing relationship with one to many
                entity.HasOne(e => e.Pricing)
                      .WithMany(e => e.Bookings)
                      .HasForeignKey(e => e.PricingId);

                // Value conversion
                entity.Property(e => e.Status)
                      .HasConversion(s => s.ToString(),
                                     s => (BookingStatus)Enum.Parse(typeof(BookingStatus), s));
            });

            modelBuilder.Entity<Dock>(entity =>
            {
                entity.HasKey(e => e.Id);
            });

            modelBuilder.Entity<Pricing>(entity =>
            {
                entity.HasKey(e => e.Id);


                entity.Property(e => e.BookingFees)
                      .HasColumnType("decimal(18, 6)");

                entity.Property(e => e.HourlyPrice)
                     .HasColumnType("decimal(18, 6)");

                entity.Property(e => e.DiscountHourlyPrice)
                      .HasColumnType("decimal(18, 6)");

                entity.Property(e => e.MissedBookingFees)
                     .HasColumnType("decimal(18, 6)");

                entity.Property(e => e.OverdueHourlyPrice)
                     .HasColumnType("decimal(18, 6)");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.HasKey(e => e.Id);

                // Make key auto
                entity.Property(e => e.Id)
                      .ValueGeneratedOnAdd();

                entity.Property(e => e.Total)
                     .HasColumnType("decimal(18, 6)");

                // Relationship for booking and orders.
                entity.HasOne(e => e.Booking)
                      .WithOne(e => e.Order)
                      .HasForeignKey<Booking>(e => e.OrderId)
                      .IsRequired(false);

                // Conversion from string to concrete object and vice versa
                entity.Property(e => e.Items)
                    .HasConversion(s => JsonConvert.SerializeObject(s, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }),
                    s => JsonConvert.DeserializeObject<Dictionary<string, decimal>>(s, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore }));
            });

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                throw new InvalidOperationException("Management Db is not configured");
            }
        }

        public void AddOrUpdate(object entity)
        {
            var entry = this.Entry(entity);
            switch (entry.State)
            {
                case EntityState.Detached:
                    this.Add(entity);
                    break;
                case EntityState.Modified:
                    this.Update(entity);
                    break;
                case EntityState.Added:
                    this.Add(entity);
                    break;
                case EntityState.Unchanged:
                    //item already in db no need to do anything  
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
