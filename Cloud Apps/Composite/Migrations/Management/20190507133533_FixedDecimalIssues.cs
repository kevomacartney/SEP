﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CityCycles.Composite.Migrations.Management
{
    public partial class FixedDecimalIssues : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BookingFee",
                table: "Pricings");

            migrationBuilder.DropColumn(
                name: "DiscountHourly",
                table: "Pricings");

            migrationBuilder.DropColumn(
                name: "Hourly",
                table: "Pricings");

            migrationBuilder.DropColumn(
                name: "MissedBookingFee",
                table: "Pricings");

            migrationBuilder.DropColumn(
                name: "OverdueHourly",
                table: "Pricings");

            migrationBuilder.AddColumn<decimal>(
                name: "BookingFees",
                table: "Pricings",
                type: "decimal(18, 6)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountHourlyPrice",
                table: "Pricings",
                type: "decimal(18, 6)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "HourlyPrice",
                table: "Pricings",
                type: "decimal(18, 6)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "MissedBookingFees",
                table: "Pricings",
                type: "decimal(18, 6)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "OverdueHourlyPrice",
                table: "Pricings",
                type: "decimal(18, 6)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "Total",
                table: "Orders",
                type: "decimal(18, 6)",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BookingFees",
                table: "Pricings");

            migrationBuilder.DropColumn(
                name: "DiscountHourlyPrice",
                table: "Pricings");

            migrationBuilder.DropColumn(
                name: "HourlyPrice",
                table: "Pricings");

            migrationBuilder.DropColumn(
                name: "MissedBookingFees",
                table: "Pricings");

            migrationBuilder.DropColumn(
                name: "OverdueHourlyPrice",
                table: "Pricings");

            migrationBuilder.AddColumn<decimal>(
                name: "BookingFee",
                table: "Pricings",
                type: "decimal",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DiscountHourly",
                table: "Pricings",
                type: "decimal",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Hourly",
                table: "Pricings",
                type: "decimal",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "MissedBookingFee",
                table: "Pricings",
                type: "decimal",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "OverdueHourly",
                table: "Pricings",
                type: "decimal",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "Total",
                table: "Orders",
                type: "decimal",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18, 6)");
        }
    }
}
