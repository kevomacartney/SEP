﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CityCycles.Composite.Migrations.Management
{
    public partial class ChangedBookingColumnName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bookings_Pricings_BookingId",
                table: "Bookings");

            migrationBuilder.RenameColumn(
                name: "BookingId",
                table: "Bookings",
                newName: "PricingId");

            migrationBuilder.RenameIndex(
                name: "IX_Bookings_BookingId",
                table: "Bookings",
                newName: "IX_Bookings_PricingId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bookings_Pricings_PricingId",
                table: "Bookings",
                column: "PricingId",
                principalTable: "Pricings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Bookings_Pricings_PricingId",
                table: "Bookings");

            migrationBuilder.RenameColumn(
                name: "PricingId",
                table: "Bookings",
                newName: "BookingId");

            migrationBuilder.RenameIndex(
                name: "IX_Bookings_PricingId",
                table: "Bookings",
                newName: "IX_Bookings_BookingId");

            migrationBuilder.AddForeignKey(
                name: "FK_Bookings_Pricings_BookingId",
                table: "Bookings",
                column: "BookingId",
                principalTable: "Pricings",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
