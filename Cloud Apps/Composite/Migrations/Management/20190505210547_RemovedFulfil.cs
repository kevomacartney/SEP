﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CityCycles.Composite.Migrations.Management
{
    public partial class RemovedFulfil : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Fulfilled",
                table: "Bookings",
                newName: "IsDockBooking");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsDockBooking",
                table: "Bookings",
                newName: "Fulfilled");
        }
    }
}
