﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CityCycles.Composite.Migrations.Management
{
    public partial class AddedStaffBit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RoleName",
                table: "User");

            migrationBuilder.AddColumn<bool>(
                name: "IsStaff",
                table: "User",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsStaff",
                table: "User");

            migrationBuilder.AddColumn<string>(
                name: "RoleName",
                table: "User",
                nullable: true);
        }
    }
}
