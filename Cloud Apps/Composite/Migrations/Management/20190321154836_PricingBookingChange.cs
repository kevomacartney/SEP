﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CityCycles.Composite.Migrations.Management
{
    public partial class PricingBookingChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "BookingFee",
                table: "Pricing",
                newName: "MissedBookingFee");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "MissedBookingFee",
                table: "Pricing",
                newName: "BookingFee");
        }
    }
}
