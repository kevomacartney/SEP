﻿using System;
using NServiceBus;

namespace CityCycles.Composite.BusMessages.API
{
    public class PaymentNotProcessed : IMessage
    {
        /// <summary>
        /// Gets or sets the <see cref="Booking"/> identifier
        /// </summary>
        public Guid BookingId { get; set; }

        /// <summary>
        /// Gets or sets the date the transaction was processed
        /// </summary>
        public DateTime DeclinedOn { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string UserEmail { get; set; }

        /// <summary>
        /// Gets or sets the reason the payment was not processed.
        /// </summary>
        /// <value>The reason.</value>
        public string Reason { get; set; }

        public string TransactionId { get; set; }
    }
}
