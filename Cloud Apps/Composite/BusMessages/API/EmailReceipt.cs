﻿using NServiceBus;
using System;
using System.Collections.Generic;
using System.Text;

namespace CityCycles.Composite.BusMessages.API
{
    public class EmailReceipt : ICommand
    {
        /// <summary>
        /// Gets or sets the User id
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the booking id
        /// </summary>
        public Guid BookingId { get; set; }
    }
}
