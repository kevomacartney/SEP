﻿using System;
using NServiceBus;

namespace CityCycles.Composite.BusMessages.API
{
    public class UserRegistered : ICommand
    {
        /// <summary>
        /// Gets or sets the user identifier.
        /// </summary>
        /// <value>The user identifier.</value>
        public Guid UserId { get; set; }
    }
}
