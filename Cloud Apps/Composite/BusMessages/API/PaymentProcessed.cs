﻿using System;
using CityCycles.Composite.Data.ManagementData;
using NServiceBus;

namespace CityCycles.Composite.BusMessages.API
{
    /// <summary>
    /// 
    /// </summary>
    public class PaymentProcessed : IMessage
    {
        /// <summary>
        /// Gets or sets the <see cref="Booking"/> identifier
        /// </summary>
        public Guid BookingId { get; set; }

        /// <summary>
        /// Gets or sets the date the transaction was processed
        /// </summary>
        public DateTime ProcessedOn { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string UserEmail { get; set; }

        /// <summary>
        /// Gets or sets the braintree gateway transaction Id
        /// </summary>
        public string TransactionId { get; set; }
    }
}
