﻿using System;
using NServiceBus;

namespace CityCycles.Composite.BusMessages.Management
{
    public class ProcessBooking : ICommand
    {
        /// <summary>
        /// Gets or sets the booking identifier.
        /// </summary>
        /// <value>The booking identifier.</value>
        public Guid BookingId { get; set; }

        /// <summary>
        /// Gets or sets the payment nonce.
        /// </summary>
        /// <value>The payment nonce.</value>
        public string PaymentNonce { get; set; }

        /// <summary>
        /// Gets or sets the created on.
        /// </summary>
        /// <value>The created on.</value>
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// Gets or sets the pricing identifier.
        /// </summary>
        /// <value>The pricing identifier.</value>
        public Guid PricingId { get; set; }
    }
}
