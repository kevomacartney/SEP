﻿using System;
using NServiceBus;

namespace CityCycles.Composite.BusMessages.Management
{
    public class CreateDockAccount : ICommand
    {
        /// <summary>
        /// Gets or sets the dock identifier.
        /// </summary>
        /// <value>The dock identifier.</value>
        public Guid DockId { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>The password.</value>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the name of the dock.
        /// </summary>
        /// <value>The name of the dock.</value>
        public string DockName { get; set; }
    }
}
