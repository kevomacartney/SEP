﻿using System;
using System.Collections.Generic;
using NServiceBus;

namespace CityCycles.Composite.BusMessages.Management
{
    public class SendEmail : ICommand
    {
        /// <summary>
        /// Gets or sets the email address the email will be sent to.
        /// </summary>
        /// <value>The email.</value>
        public string Recipient { get; set; }

        /// <summary>
        /// Gets or sets the html content of the email.
        /// </summary>
        /// <value>The content of the email.</value>
        public string EmailContent { get; set; }

        /// <summary>
        /// Gets or sets email subject.
        /// </summary>
        /// <value>The subject.</value>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets a list of attachments
        /// </summary>
        /// <value>The attatchments.</value>
        public DataBusProperty<Dictionary<string, byte[]>> Attachments { get; set; }
    }
}
