﻿using System;
using NServiceBus;

namespace CityCycles.Composite.BusMessages.Management
{
    /// <summary>
    /// BHus message to remove user dock account.
    /// </summary>
    public class DeleteDockAccount : ICommand
    {
        /// <summary>
        /// Gets or sets the dock identifier.
        /// </summary>
        /// <value>The dock identifier.</value>
        public Guid DockId { get; set; }
    }
}
