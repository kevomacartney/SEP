﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CityCycles.Composite.Exceptions;

namespace CityCycles.Composite.Repository
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"> The entity type</typeparam>
    /// <typeparam name="TU"> The Id type</typeparam>
    public interface IRepository<T, in TU> : IDisposable where T : class
    {
        /// <summary>
        /// Gets entity by its id.
        /// </summary>
        /// <param name="id">identifier</param>
        /// <returns></returns>
        T GetById(TU id);

        /// <summary>
        /// Gets entity by its id asynchronously.
        /// </summary>
        /// <param name="id">identfier</param>
        /// <returns></returns>
        Task<T> GetByIdAsync(TU id);

        /// <summary>
        /// Creates a new entity and puts it on the database.
        /// </summary>
        /// <param name="entity">Entity</param>
        void Add(T entity);

        /// <summary>
        /// Asynchronously Adds an entity to the database.
        /// </summary>
        /// <param name="entity">Entity</param>
        Task AddAsync(T entity);

        /// <summary>
        /// Deletes the entity from the database.
        /// </summary>
        /// <param name="entity">Entity</param>
        void Delete(T entity);

        /// <summary>
        /// Deletes the entity from the database asynchronously
        /// </summary>
        /// <param name="entity">Entity.</param>
        Task DeleteAsync(T entity);

        /// <summary>
        /// Deletes the entity by it identifier.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <exception cref="EntityNotFoundException">Thrown if no entity with the given id was found </exception>
        void DeleteById(TU id);

        /// <summary>
        /// Deletes the entity by it identifier.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <exception cref="EntityNotFoundException">Thrown if no entity with the given id was found </exception>
        Task DeleteByIdAsync(TU id);

        /// <summary>
        /// Updates the database on the database with the new entity.
        /// </summary>
        /// <param name="entity">Entity</param>
        void Update(T entity);

        /// <summary>
        /// Updates an existing entity
        /// </summary>
        /// <param name="entity">Entity</param>
        Task UpdateAsync(T entity);

        /// <summary>
        ///  Updates an existing entity entity asynchronously
        /// </summary>
        /// <param name="entities">Entity</param>
        Task UpdateRangeAsync(ICollection<T> entities);
    }
}
