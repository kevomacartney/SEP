﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CityCycles.Composite.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace CityCycles.Composite.Repository
{
    public abstract class Repository<T, TU> : IRepository<T, TU> where T : class
    {
        /// <summary>
        /// An instance of Db-context for this repository
        /// </summary>
        protected readonly DbContext _rDbContext;

        protected Repository(DbContext rLocalContext)
        {
            _rDbContext = rLocalContext ?? throw new ArgumentNullException(nameof(rLocalContext));
        }

        /// <inheritdoc />
        public T GetById(TU id)
            => _rDbContext.Find<T>(id);

        /// <inheritdoc />
        public Task<T> GetByIdAsync(TU id)
            => _rDbContext.FindAsync<T>(id);

        /// <inheritdoc />
        public void Add(T entity)
        {
            _rDbContext.Add(entity);
            _rDbContext.SaveChanges();
        }

        /// <inheritdoc />
        public async Task AddAsync(T entity)
        {
            await _rDbContext.AddAsync(entity);
            await _rDbContext.SaveChangesAsync();
        }

        /// <inheritdoc />
        public void Delete(T entity)
        {
            if (Exists(entity))
                _rDbContext.Entry(entity).State = EntityState.Detached;

            _rDbContext.Entry(entity).State = EntityState.Deleted;
            _rDbContext.SaveChanges();
        }

        /// <inheritdoc />
        public Task DeleteAsync(T entity)
        {
            if (Exists(entity))
                _rDbContext.Entry(entity).State = EntityState.Detached;

            _rDbContext.Entry(entity).State = EntityState.Deleted;
            return _rDbContext.SaveChangesAsync();
        }

        /// <inheritdoc />
        public void Update(T entity)
        {
            if (Exists(entity))
                _rDbContext.Entry(entity).State = EntityState.Detached;

            _rDbContext.Entry(entity).State = EntityState.Modified;
            _rDbContext.SaveChanges();
        }

        /// <inheritdoc />
        public Task UpdateAsync(T entity)
        {
            if (Exists(entity))
                _rDbContext.Entry(entity).State = EntityState.Detached;

            _rDbContext.Entry(entity).State = EntityState.Modified;
            return _rDbContext.SaveChangesAsync();
        }

        public async Task UpdateRangeAsync(ICollection<T> entities)
        {
            foreach (var entity in entities)
                await UpdateAsync(entity);

            await _rDbContext.SaveChangesAsync();
        }

        /// <inheritdoc />
        public void Dispose()
        {
            _rDbContext.Dispose();
        }

        protected bool Exists(T entity)
        {
            return _rDbContext.Set<T>().Local.Any(e => e == entity);
        }

        public void DeleteById(TU id)
        {
            // Get the entity
            var entity = GetById(id);

            if (entity == null)
                throw new EntityNotFoundException(id.ToString());

            // Delete the entiy
            Delete(entity);
        }

        public async Task DeleteByIdAsync(TU id)
        {
            var entity = await GetByIdAsync(id);

            if (entity == null)
                throw new EntityNotFoundException(id.ToString());

            await DeleteAsync(entity);
        }
    }
}