﻿using System;
using CityCycles.Composite.Data;
using CityCycles.Composite.Data.ManagementData;

namespace CityCycles.Composite.Repository
{
    public interface IPricingRepository : IRepository<Pricing, Guid>
    {
        ManagementDataContext LocalContext { get; }
    }
}
