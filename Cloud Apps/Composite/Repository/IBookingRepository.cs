﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using CityCycles.Composite.Data;
using CityCycles.Composite.Data.ManagementData;

namespace CityCycles.Composite.Repository
{
    public interface IBookingRepository : IRepository<Booking, Guid>
    {
        ManagementDataContext LocalContext { get; }

        /// <summary>
        /// Gets the all active bookings for the specified bookings
        /// </summary>
        /// <returns>The active user booking.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<Booking>> GetActiveUserDockBooking(Guid userId);

        /// <summary>
        /// Gets all bookings by a user.
        /// </summary>
        /// <returns>The all bookings by user.</returns>
        /// <param name="userId">User identifier.</param>
        Task<List<Booking>> GetAllBookingsByUserAsync(Guid userId);

        /// <summary>
        /// Gets all bookings.
        /// </summary>
        /// <returns>The all bookings.</returns>
        Task<List<Booking>> GetAllBookingsAsync();
    }
}
