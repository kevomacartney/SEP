﻿using System;
using CityCycles.Composite.Data;
using CityCycles.Composite.Data.ManagementData;

namespace CityCycles.Composite.Repository.Concrete
{
    public class PricingRepository : Repository<Pricing, Guid>, IPricingRepository
    {
        public PricingRepository(ManagementDataContext context)
            :base(context)
        {
            LocalContext = context;
        }

        public ManagementDataContext LocalContext { get; }
    }
}
