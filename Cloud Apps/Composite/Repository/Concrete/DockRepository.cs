﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CityCycles.Composite.Data;
using CityCycles.Composite.Data.ManagementData;
using Microsoft.EntityFrameworkCore;

namespace CityCycles.Composite.Repository.Concrete
{
    public class DockRepository : Repository<Dock, Guid>, IDockRepository
    {
        public ManagementDataContext LocalContext { get; }

        public DockRepository(ManagementDataContext context)
            :base(context)
        {
            LocalContext = context;
        }

        public Task<List<Dock>> GetAllDocks()
           => LocalContext.Docks.Include(d => d.Bikes).ToListAsync();

        public new Task<Dock> GetByIdAsync(Guid id)
            => LocalContext.Docks.Include(d => d.Bikes).FirstOrDefaultAsync(d => d.Id == id);

        public new Dock GetById(Guid id)
            => LocalContext.Docks.Include(d => d.Bikes).FirstOrDefault(d => d.Id == id);

    }
}
