﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CityCycles.Composite.Data;
using CityCycles.Composite.Data.ManagementData;
using Microsoft.EntityFrameworkCore;

namespace CityCycles.Composite.Repository.Concrete
{
    public class BikeRepository : Repository<Bike, Guid>, IBikeRepository
    {
        public ManagementDataContext LocalContext { get; }

        public BikeRepository(ManagementDataContext context)
            :  base(context)
        {
            LocalContext = context;
        }

        public Task<List<Bike>> GetAllBikes()
            => LocalContext.Bikes.Include(d => d.Dock).ToListAsync();

        public Task<List<Booking>> GetAllActiveBookings(Guid bikeId, DateTime startDate, DateTime endDate)
            => LocalContext.Bikes
                           .Where(b => b.Id == bikeId)
                           .SelectMany(b => b.Bookings)
                           .Distinct()
                           .Where(b => !b.BookingUsed) // Booking hasn't been used
                           .Where(b => b.Status != BookingStatus.Declined)
                           .Where(b => b.StartDate >= startDate && b.StartDate <= endDate) // Bookings in the given range 
                           .ToListAsync();

        public Task<Booking> GetActiveDockBooking(Guid bikeId)
            => LocalContext.Bikes
                            .Where(b => b.Id == bikeId)
                            .SelectMany(b => b.Bookings)
                            .Distinct()
                            .FirstOrDefaultAsync(b =>!b.BookingUsed && b.IsDockBooking && b.Status == BookingStatus.Receieved);
                           
    }
}
