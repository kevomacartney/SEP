﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CityCycles.Composite.Data;
using CityCycles.Composite.Data.ManagementData;
using Microsoft.EntityFrameworkCore;

namespace CityCycles.Composite.Repository.Concrete
{
    public class BookingRepository : Repository<Booking, Guid>, IBookingRepository
    {
        public ManagementDataContext LocalContext { get; }

        public BookingRepository(ManagementDataContext context)
            :base(context)
        {
            LocalContext = context;
        }

        public new Task<Booking> GetByIdAsync(Guid id)
            => LocalContext.Bookings.Include(b => b.Order).FirstOrDefaultAsync(b => b.Id == id);

        public new Booking GetById(Guid id)
            => LocalContext.Bookings.Include(b => b.Order).FirstOrDefault(b => b.Id == id);

        public Task<List<Booking>> GetAllBookingsByUserAsync(Guid userId)
            => LocalContext.Bookings
                           .Include(b => b.Order)
                           .Where(b => b.UserId == userId)
                           .OrderByDescending(d => d.CreatedOn)
                           .ToListAsync();

        public Task<List<Booking>> GetAllBookingsAsync()
            => LocalContext.Bookings
                           .OrderBy(d => d.CreatedOn)
                           .ToListAsync();

        public Task<List<Booking>> GetActiveUserDockBooking(Guid userId)
            => LocalContext.Bookings.Where(b => b.UserId == userId 
                                                && b.Status == BookingStatus.Receieved 
                                                && !b.BookingUsed 
                                                && b.IsDockBooking).ToListAsync();
                                    
    }
}
