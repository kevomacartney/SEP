﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CityCycles.Composite.Data;
using CityCycles.Composite.Data.ManagementData;

namespace CityCycles.Composite.Repository
{
    public interface IBikeRepository : IRepository<Bike, Guid>
    {
        /// <summary>
        /// Gets the local context.
        /// </summary>
        /// <value>The local context.</value>
        ManagementDataContext LocalContext { get; }

        /// <summary>
        /// Gets all bikes.
        /// </summary>
        /// <returns>The all bikes.</returns>
        Task<List<Bike>> GetAllBikes();

        /// <summary>
        ///  Gets all active bookings for the specified bike on the specified date range
        /// </summary>
        /// <returns>The all active bookings.</returns>
        /// <param name="bikeId">Bike identifier.</param>
        Task<List<Booking>> GetAllActiveBookings(Guid bikeId, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Gets the active booking for the specified bike booked by the specified user
        /// </summary>
        /// <returns>The active booking.</returns>
        /// <param name="bikeId">Bike identifier.</param>
        Task<Booking> GetActiveDockBooking(Guid bikeId);
    }
}
