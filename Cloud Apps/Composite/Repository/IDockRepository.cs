﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CityCycles.Composite.Data;
using CityCycles.Composite.Data.ManagementData;

namespace CityCycles.Composite.Repository
{
    public interface IDockRepository : IRepository<Dock, Guid>
    {
        /// <summary>
        /// Gets the local context.
        /// </summary>
        /// <value>The local context.</value>
        ManagementDataContext LocalContext { get; }

        /// <summary>
        /// Gets all docks and their bikes.
        /// </summary>
        /// <returns>The all docks.</returns>
        Task<List<Dock>> GetAllDocks();
    }
}
