﻿using System;
using AutoMapper.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace CityCycles.Composite.Logging
{
    public static class AddLogging
    {
        public static void AddSeqLogging(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            var warningPath = configuration.GetValue<string>("Logging:FilePath");

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .WriteTo.RollingFile(warningPath, Serilog.Events.LogEventLevel.Warning)

                .WriteTo.Seq(configuration.GetValue<string>("Logging:Endpoint"),
#if DEBUG
                                              restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Debug,
#else
                                              restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Information,
#endif
                    apiKey: configuration.GetValue<string>("Logging:SeqApiKey"))
                .CreateLogger();
        }
    }
}
