using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using CityCycles.Composite.Model.Shared.Settings;
using Microsoft.IdentityModel.Tokens;

namespace CityCycles.Composite.Shared_Services
{
    public class EncryptionService
    {
        private const int DerivationIterations = 1000;
        private const int Keysize = 128;

        /// <summary>
        /// Gets or sets the X509Certificate2 
        /// </summary>
        protected X509Certificate2 Certificate { get; }

        /// <summary>
        /// The key used for encrypting 
        /// </summary>
        private string Key { get; }

        /// <summary>
        /// Gets the key used for signing JWT token
        /// </summary>
        public X509SigningCredentials TokenSignKey
            => new X509SigningCredentials(Certificate);

        /// <summary>
        /// Uses a certificate with the name for encrypting data.
        /// </summary>
        /// <param name="certName">The name of the certificate to use</param>
        /// <param name="encryptedKeys"> </param>
        public EncryptionService(string certName, SecretKeys encryptedKeys)
        {
            // Get the certificate from the storage
            Certificate = GetX509Certificate(certName);

            Key = DecryptKey(encryptedKeys.KeyEncryption);
        }

        /// <summary>
        /// Creates a Digital signature of a string and returns it
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string Sign(string str)
        {
            if (this.Certificate == null || string.IsNullOrEmpty(str))
                throw new ArgumentException("A x509 certificate and string for signing must be provided");

            if (!Certificate.HasPrivateKey)
                throw new InvalidOperationException("x509 certificate does not contain a private key.");

            var csp = (RSA)Certificate.PrivateKey;

            var bytesData = Encoding.UTF8.GetBytes(str);

            var result = csp.SignData(bytesData, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
            return Convert.ToBase64String(result);
        }

        /// <summary>
        /// Verifies that a digital signature is valid for the given string
        /// </summary>
        /// <param name="signature"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        public bool VerifySignature(string signature, string str)
        {
            if (this.Certificate == null || string.IsNullOrEmpty(str) || string.IsNullOrEmpty(signature))
                throw new ArgumentException("A x509 certificate, a signature and a string must be available");

            var csp = (RSA)Certificate.PublicKey.Key;

            var strBytes = Encoding.UTF8.GetBytes(str);
            var signatureBytes = Convert.FromBase64String(signature);
            try
            {
                return csp.VerifyData(strBytes, signatureBytes, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Encrypts a string and returns the encrypted result in base64string
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string Encrypt(string str)
        {
            var saltStringBytes = GenerateCryptoBits();
            var ivStringBytes = GenerateCryptoBits();
            var plainTextBytes = Encoding.UTF8.GetBytes(str);

            using (var key = new Rfc2898DeriveBytes(Key, saltStringBytes, DerivationIterations))
            {
                var keyBytes = key.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 128;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream())
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                            {
                                cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                cryptoStream.FlushFinalBlock();
                                // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
                                var cipherTextBytes = saltStringBytes;
                                cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                                cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Convert.ToBase64String(cipherTextBytes);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Decrypts a base64sting and returns the raw string.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string Decrypt(string str)
        {
            // Get the complete stream of bytes that represent:
            // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
            var strTextBytesWithSaltAndIv = Convert.FromBase64String(str);
            // Get the salt-bytes by extracting the first 32 bytes from the supplied cipherText bytes.
            var saltStringBytes = strTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
            // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
            var ivStringBytes = strTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
            // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
            var cipherTextBytes = strTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(strTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

            using (var key = new Rfc2898DeriveBytes(Key, saltStringBytes, DerivationIterations))
            {
                var keyBytes = key.GetBytes(Keysize / 8);
                using (var symmetricKey = new RijndaelManaged())
                {
                    symmetricKey.BlockSize = 128;
                    symmetricKey.Mode = CipherMode.CBC;
                    symmetricKey.Padding = PaddingMode.PKCS7;
                    using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                    {
                        using (var memoryStream = new MemoryStream(cipherTextBytes))
                        {
                            using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                            {
                                var plainTextBytes = new byte[cipherTextBytes.Length];
                                var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                memoryStream.Close();
                                cryptoStream.Close();
                                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                            }
                        }
                    }
                }
            }
        }

        private byte[] GenerateCryptoBits()
        {
            var randomBytes = new byte[16];
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                // Fill the array with cryptographically secure random bytes.
                rngCsp.GetBytes(randomBytes);
            }
            return randomBytes;
        }

        private string DecryptKey(string key)
        {
            if (this.Certificate == null || string.IsNullOrEmpty(key))
                throw new Exception("A x509 certificate and string for decryption must be provided");

            if (!Certificate.HasPrivateKey)
                throw new Exception("x509 certificate does not contain a private key for decryption");

            RSA csp = (RSA)Certificate.PrivateKey;

            byte[] bytesEncrypted = Convert.FromBase64String(key);
            byte[] bytesDecrypted = csp.Decrypt(bytesEncrypted, RSAEncryptionPadding.OaepSHA256);
            return Encoding.UTF8.GetString(bytesDecrypted);

        }

        private string EncryptKey(string key)
        {
            if (this.Certificate == null || string.IsNullOrEmpty(key))
                throw new Exception("A x509 certificate and string for decryption must be provided");

            if (!Certificate.HasPrivateKey)
                throw new Exception("x509 certificate does not contain a private key for decryption");

            RSA csp = (RSA)Certificate.PublicKey.Key;

            byte[] bytesData = Encoding.UTF8.GetBytes(key);
            byte[] bytesEncrypted = csp.Encrypt(bytesData, RSAEncryptionPadding.OaepSHA256);
            return Convert.ToBase64String(bytesEncrypted);

        }

        /// <summary>
        /// Returns a X509Certificate2 with the given name
        /// </summary>
        /// <param name="serialNum"> The serial number of the certificate</param>
        /// <returns></returns>
        protected X509Certificate2 GetX509Certificate(string serialNum)
        {
            // Get the certificate store for the current user.
            var store = new X509Store(StoreLocation.LocalMachine);

            try
            {
                store.Open(OpenFlags.ReadOnly);

                // Place all certificates in an X509Certificate2Collection object.
                var certCollection = store.Certificates;

                var currentCerts = certCollection.Find(X509FindType.FindByTimeValid, DateTime.Now, false);
                var signingCert = currentCerts.Find(X509FindType.FindBySerialNumber, serialNum, false);

                if (signingCert.Count == 0)
                    return null;

                return signingCert[0];
            }
            finally
            {
                store.Close();
            }
        }
    }
}