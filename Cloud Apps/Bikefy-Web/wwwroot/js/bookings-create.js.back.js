let bookingsCreate = (function () {

    let me = null;

    // Date picker elements
    let datePicker = null;
    let datePickerDate = null;
    let datePickerTitle = null;
    let datePickerStart = null;
    let datePickerEnd = null;

    //Braintree gateway elements
    this.paymentTokenInput = null;
    this.paymentForm = null;

    function bookingsCreate() {
        me = this;
    }

    bookingsCreate.prototype.init = function () {
        // initialise date picker elements
        this.datePickers = $(".booking-datepicker");
        // Braintree gateway
        this.paymentTokenInput = $("#Payment-Token");
        this.paymentForm = $("#payment-form");

        this.initDatePicker();
        this.initPayments();
        initPricing();
    }

    bookingsCreate.prototype.initPayments = function () {
        if (this.paymentTokenInput.length == 0) {
            console.log("Payment token was not provided")
            return;
        }
        var clientToken = this.paymentTokenInput.val();
        braintree.setup(clientToken, "dropin", {
            container: "payment-form"
        });
    }

    bookingsCreate.prototype.initPricing = function(){
        console.log(pricing);
    }

    bookingsCreate.prototype.initDatePicker = function () {
        // minimal setup
        this.datePickers.datetimepicker({
            todayHighlight: true,
            setDate: new Date(),
            autoclose: true,
            format: "dd/mm/yyyy hh:mm"
        });
    }

    return bookingsCreate;
}());

$(function () {
    var bookings = new bookingsCreate();
    bookings.init();
});
