"use-strict";

let DockLocations = (function () {
    let me = null;

    function DockLocations() {
        me = this;
        console.log(locations);
    }

    DockLocations.prototype.init = function () {
        // initialis the map
        inialiseMap();
    }


    function inialiseMap() {
        console.log("Map initialising");
        console.log(locations);

        //Town Square Location
        var townSquare = { lat: 53.80155, lng: -1.54882 };

        // The map, centered at Uni of Leeds
        var map = new google.maps.Map(
            document.getElementById('map'), { zoom: 14, center: townSquare });

        for (var i = 0; i < locations.length; i++) {
            console.log(locations[i]);
            var location = locations[i].location;
            console.log(location);
            var locationSplit = location.split(" ");
            console.log(locationSplit);
            var lati = parseFloat(locationSplit[0]);
            var long = parseFloat(locationSplit[1]);
            var dockID = locations[i].dockId;
            console.log(dockID);
            console.log(lati);
            console.log(long);

            var place = { lat: lati, lng: long };
            var pinPointPlace = new google.maps.Marker({ position: place, map: map });
        }
    }

    return DockLocations;
}());

$(function () {
    $("#docks-menu-item").addClass("k-menu__item--open k-menu__item--here");
    $("#docks-menu-item-locations").addClass("k-menu__item--active");

    // Delete dock option
    $('.dock-delete').click(function (e) {
        // Get the dock id fromthe clicked element
        var dockId = $(this).attr("data-dockId");

        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then(function (result) {
            if (result.value) {
                swal(
                    'Deleted!',
                    'The dock has been deleted.',
                    'success'
                )

                setTimeout(function () {
                    // Get the form element and submit it
                    var form = $("#dock-location-form-" + dockId);
                    form.submit();
                }, 1500);

            } else if (result.dismiss === 'cancel') {
                swal(
                    'Cancelled',
                    'The dock was not deleted',
                    'error'
                )
            }
        });
    });
});