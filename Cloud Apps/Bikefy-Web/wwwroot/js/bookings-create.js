let bookingsCreate = (function () {

    let me = null;

    // Date picker elements
    let datePicker = null;
    let datePickerDate = null;
    let datePickerTitle = null;
    let datePickerStart = null;
    let datePickerEnd = null;

    //Braintree gateway elements
    this.paymentTokenInput = null;
    this.paymentForm = null;

    function bookingsCreate() {
        me = this;
    }

    bookingsCreate.prototype.init = function () {
        // initialise date picker elements
        this.datePickers = $(".booking-datepicker");
        // Braintree gateway
        this.paymentTokenInput = $("#Payment-Token");
        this.paymentForm = $("#payment-form");

        this.initDatePicker();
        this.initPayments();
    }

    bookingsCreate.prototype.initPayments = function () {
        if (this.paymentTokenInput.length == 0) {
            console.log("Payment token was not provided")
            return;
        }
        var clientToken = this.paymentTokenInput.val();
        braintree.setup(clientToken, "dropin", {
            container: "payment-form"
        });
    }

    bookingsCreate.prototype.initDatePicker = function () {
        // minimal setup
        this.datePickers.datetimepicker({
            todayHighlight: true,
            setDate: this.datePickers.val(),
            autoclose: true,
            format: "dd/mm/yyyy hh:mm"
        });

        $(".booking-datepicker").change(changeEvent);

        changeEvent(null);
    }

    function changeEvent(event) {
        let startDateInput = $("#startDateId");
        let endDateInput = $("#endDateId");

        let startDate = moment(startDateInput.val(), "DD/MM/YYYY HH:mm");
        let endDate = moment(endDateInput.val(), "DD/MM/YYYY HH:mm");

        var duration = moment.duration(endDate.diff(startDate));
        var hours = duration.asHours();
        var roundedHours = hours.toFixed(2);

        let totalCost = hours * pricing.hourly;
        var roundedTotalCost = totalCost.toFixed(2);

        let grandTotal = pricing.bookingFee + totalCost;
        var roundedGrandTotal = grandTotal.toFixed(2);

        $("#bookingTime").html(roundedHours);
        $("#bookingTimeCost").html("&pound" + roundedTotalCost);
        $("#finalTotal").html("<b> &pound" + roundedGrandTotal.toString() + "</b>");
    }

    return bookingsCreate;
}());

$(function () {
    var bookings = new bookingsCreate();
    bookings.init();
});
