using CityCycles.Composite.Braintree;
using Microsoft.AspNetCore.Authorization;
using CityCycles.Web.Providers;
using CityCycles.Web.Providers.Concrete;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.Cookies;
using CityCycles.Composite.Repository;
using CityCycles.Composite.Repository.Concrete;
using Microsoft.AspNetCore.Identity;
using CityCycles.Composite.Data.ManagementData.User;
using CityCycles.Composite.Data;
using CityCycles.Composite.Logging;
using CityCycles.Web.Extensions.Authentication;
using CityCycles.Web.Extensions.ServiceBus;

namespace CityCycles.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add seq logging to the pipeline
            services.AddSeqLogging(Configuration);

            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // Register the API provider
            services.AddSingleton<IApiProvider, ApiProvider>();

            services.ConfigureAuthentication(Configuration);

            // Add braintree to the pipeline
            services.AddBraintree(Configuration);

            services.AddMvc(options =>
            {
                var authPolicy = new AuthorizationPolicyBuilder();
                authPolicy.RequireAuthenticatedUser();

                // options.Filters.Add(new AuthorizeFilter(authPolicy.Build()));

                options.RequireHttpsPermanent = true;

            });
            // Add Service Bus
            services.AddServiceBus(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseAuthentication();


            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
