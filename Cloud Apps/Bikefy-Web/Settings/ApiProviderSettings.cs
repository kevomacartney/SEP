﻿using System;

namespace CityCycles.Web.Settings
{
    public class ApiProviderSettings
    {
        /// <summary>
        /// Gets or sets the Api's Url
        /// </summary>
        public string ApiUrl { get; set; }

        /// <summary>
        /// Gets or sets the access key to the API
        /// </summary>
        public string AccessKey { get; set; }

        /// <summary>
        /// Gets or sets the application's identifier
        /// </summary>
        public Guid AppId { get; set; }
    }
}
