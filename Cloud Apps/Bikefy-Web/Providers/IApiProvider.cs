using System.Threading.Tasks;

namespace CityCycles.Web.Providers
{
    /// <summary>
    /// Provides api services and abstracts away from the http specifics required to access the API
    /// </summary>
    public interface IApiProvider
    {
        /// <summary>
        /// Builds and returns the full url.
        /// </summary>
        /// <param name="controller"> The controller for the request. </param>
        /// <param name="action"> The action for the request</param>
        /// <param name="query">The query for the request </param>
        /// <returns></returns>
        string BuildUrl(string controller, string action, string query = null);

        /// <summary>
        /// Sends a get request to the url provided
        /// </summary>
        /// <param name="url"> The url the Get request is being send to. </param>
        /// <param name="data">The data being sent to the endpoint.</param>
        /// <returns></returns>
        Task<string> SendGetRequest(string url, object data = null);

        /// <summary>
        /// Sends the post request to the url provided
        /// </summary>
        /// <returns>The post request.</returns>
        /// <param name="url"> The url the Get request is being send to. </param>
        /// <param name="data">The data being sent to the endpoint.</param>
        Task<string> SendPostRequest(string url, object data);
    }
}
