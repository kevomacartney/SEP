using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using CityCycles.Composite.Model.Auth;
using CityCycles.Web.Settings;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace CityCycles.Web.Providers.Concrete
{
    public class ApiProvider : IApiProvider
    {
        public static class LogEvents
        {
            public static readonly EventId SendingGetRequest = new EventId(1, nameof(SendGetRequest));

            public static readonly EventId SendingPostRequest = new EventId(2, nameof(SendingPostRequest));

            public static readonly EventId GettingAccessToken = new EventId(3, nameof(GetAccessTokenAsync));
        }

        /// <summary>
        /// The service provider.
        /// </summary>
        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// The http client.
        /// </summary>
        private readonly HttpClient _httpClient;

        /// <summary>
        /// The configuration instance
        /// </summary>
        private readonly IConfiguration _configuration;

        /// <summary>
        /// The logger
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// The provider settings
        /// </summary>
        private readonly ApiProviderSettings _providerSettings;

        /// <summary>
        /// Gets or sets the access token instance
        /// </summary>
        private AccessToken AccessToken { get; set; }

        public ApiProvider(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _configuration = _serviceProvider.GetService<IConfiguration>();

            _logger = _serviceProvider.GetService<ILogger<ApiProvider>>();

            _httpClient = new HttpClient();

            // Bind the api config to the instance
            _providerSettings = new ApiProviderSettings();
            _configuration.Bind("APIConfig", _providerSettings);
        }

        public string BuildUrl(string controller, string action, string query = null)
            => $"{_providerSettings.ApiUrl}{controller}/{action}?{query}";

        /// <summary>
        /// Prepares a HTTP response, will format and throw exception if IsSuccessStatusCode is false
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        private async Task<string> HandleHttpClientResponse(HttpResponseMessage response)
        {
            // If there wasn't an error
            if (response.IsSuccessStatusCode)
            {
                _logger.LogInformation(LogEvents.GettingAccessToken, $"{{API}} Request successful.");
                return await response.Content.ReadAsStringAsync();
            }

            // Prepare error 
 await response.Content.ReadAsStringAsync();

            // Internal server error request was not handled
            if (response.StatusCode == HttpStatusCode.InternalServerError)
            {
                using (_logger.BeginScope(new Dictionary<string, object>()
                {
                    ["Api-Status Code"] = response.StatusCode
                }))
                {
                    _logger.LogError(LogEvents.GettingAccessToken, $"{{API}} A request was send and got an internal server errror.");

                    throw new WebException("There was an error while handling the request, Please try again.", WebExceptionStatus.SendFailure);
                }
            }

            using (_logger.BeginScope(new Dictionary<string, object>()
            {
                ["Api-Status Code"] = response.StatusCode
            }))
            {
                _logger.LogInformation(LogEvents.GettingAccessToken, $"{{API}} A request was send and did not get status code 200.");

                // Request was sucessful but the server did not like it
                throw new WebException(response.StatusCode.ToString(), WebExceptionStatus.Success);
            }
        }

        /// <summary>
        /// Will return an access token to the API
        /// </summary>
        /// <exception cref="WebException"> Thrown if there was an error while processing the request </exception>
        /// <returns></returns>
        private async Task<string> GetAccessTokenAsync()
        {
            _logger.LogInformation(LogEvents.GettingAccessToken, $"{{API}} Getting web server access token.");

            async Task<AccessToken> GenerateToken()
            {
                _logger.LogInformation(LogEvents.GettingAccessToken, $"{{API}} Requesting new access token.");
                // Build url to the authentication action
                var url = BuildUrl("Auth", "AuthenticateApp");

                var model = new ApplicationAuthenticateModel()
                {
                    AppId = _providerSettings.AppId,
                    AccessKey = _providerSettings.AccessKey
                };

                // Send the post request to the api
                var response = await _httpClient.PostAsJsonAsync(url, model);

                // If application was unable to sign in, throw exception
                response.EnsureSuccessStatusCode();

                // Create the access token instance
                AccessToken = new AccessToken()
                {
                    ExpiryTime = DateTime.UtcNow + TimeSpan.FromMinutes(59),
                    Token = await response.Content.ReadAsStringAsync()
                };

                _logger.LogInformation(LogEvents.GettingAccessToken, $"{{API}} Recieved fresh access token.");

                // Return the access token
                return AccessToken;
            }

            // If token hasn't been created, or it has expire
            if (AccessToken == null || DateTime.UtcNow >= AccessToken.ExpiryTime)
            {
                _logger.LogInformation(LogEvents.GettingAccessToken, $"{{API}} Web server access token has expired.");
                // Request a token from the API and return it
                return (await GenerateToken()).Token;
            }

            // Token is still valid
            return AccessToken.Token;
        }

        public async Task<string> SendGetRequest(string url, object data = null)
        {
            _logger.LogInformation(LogEvents.SendingGetRequest, $"{{API}} Sending GET request to '{url}'");

            // Set new request to be a GET for the specified url
            var request = new HttpRequestMessage(HttpMethod.Get, url);

            if (data != null)
            {
                // Serialise the object
                var json = JsonConvert.SerializeObject(data, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
            }

            // Add the bearer token the header
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", await GetAccessTokenAsync());

            // Dispatch the request to the Url
            var response = await _httpClient.SendAsync(request);

            _logger.LogInformation(LogEvents.SendingGetRequest, $"{{API}}GET request was sent to API with '{response.StatusCode}' Status code.");

            // Return the response rate
            return await HandleHttpClientResponse(response);
        }

        public async Task<string> SendPostRequest(string url, object data)
        {
            _logger.LogInformation(LogEvents.SendingPostRequest, $"{{API}} Sending POST request to '{url}'");

            // Set new request to be a GET for the specified url
            var request = new HttpRequestMessage(HttpMethod.Post, url)
            {
                // Serialise the object and place it in the content body
                Content = new StringContent(JsonConvert.SerializeObject(data, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                }), Encoding.UTF8, "application/json")
            };

            // Add the bearer token the header
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", await GetAccessTokenAsync());

            // Dispatch the response to the Url
            var response = await _httpClient.SendAsync(request);

            _logger.LogInformation(LogEvents.SendingGetRequest, $"{{API}}POST request was sent to API with '{response.StatusCode}' Status code.");

            return await HandleHttpClientResponse(response);
        }
    }
}