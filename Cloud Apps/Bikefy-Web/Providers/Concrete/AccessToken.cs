﻿using System;

namespace CityCycles.Web.Providers.Concrete
{
    /// <summary>
    /// Represents an access token from the API
    /// </summary>
    public class AccessToken
    {
        /// <summary>
        /// Gets or sets the access token 
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Gets or sets the expiry time of the access token
        /// </summary>
        public DateTime ExpiryTime { get; set; }
    }
}
