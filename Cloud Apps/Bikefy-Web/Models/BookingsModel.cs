﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CityCycles.Web.Models
{
    public class BookingModel
    {
        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>The start date.</value>
        [Required(ErrorMessage = "Please enter the end date.")]
        public string StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>The end date.</value>
        [Required(ErrorMessage = "Please enter the end date.")]
        public string EndDate { get; set; }

        /// <summary>
        /// Gets or sets the dock identifier.
        /// </summary>
        /// <value>The dock identifier.</value>
        [Required(ErrorMessage = "Please select a dock.")]
        public Guid DockId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="T:CityCycles.Web.Models.BookingsModel"/> is paynow.
        /// </summary>
        /// <value><c>true</c> if paynow; otherwise, <c>false</c>.</value>
        public bool Paynow { get; set; } = true;

        /// <summary>
        /// Gets or sets the pricing model for the booking
        /// </summary>
        /// <value>The pricing.</value>
        public PricingModel Pricing { get; set; }
    }
}
