using System.ComponentModel.DataAnnotations;

namespace CityCycles.Web.Models
{
    public class LoginModel
    {
        /// <summary>
        /// Gets or sets Username
        /// </summary>
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Required(ErrorMessage = "Please enter username.")]
        public string Username { get; set; }

        /// <summary>
        /// Gets or set Password
        /// </summary>
        [Required(ErrorMessage = "Please enter password.")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets Remember value 
        /// </summary>
        public bool Remember { get; set; }

        /// <summary>
        /// Gets or sets Id string
        /// </summary>
        public string Id { get; set; }
    }
}