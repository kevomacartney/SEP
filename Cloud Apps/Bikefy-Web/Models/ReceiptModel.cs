﻿using System;
using CityCycles.Composite.Data.ManagementData;

namespace CityCycles.Web.Models
{
    public class ReceiptModel
    {
        /// <summary>
        /// Gets or sets the booking identifier.
        /// </summary>
        /// <value>The booking identifier.</value>
        public Guid BookingId { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        /// <value>The first name.</value>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        /// <value>The last name.</value>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the start date identifier.
        /// </summary>
        /// <value>The start date identifier.</value>
        public DateTime OrderDate { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// <value>The start date.</value>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        /// <value>The end date.</value>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets the booking fee.
        /// </summary>
        /// <value>The booking fee.</value>
        public string BookingFee { get; set; }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        /// <value>The total.</value>
        public string TimeTotal { get; set; }

        /// <summary>
        /// Gets or sets the units in hours for the booking
        /// </summary>
        /// <value>The units.</value>
        public string Units { get; set; }

        /// <summary>
        /// Gets or sets the grand total.
        /// </summary>
        /// <value>The grand total.</value>
        public string GrandTotal { get; set; }

        public BookingStatus Status { get; set; }
    }
}
