﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CityCycles.Web.Models
{
    public class PricingModel
    {
        /// <summary>
        /// Gets or sets Identifier
        /// </summary>
        public String Label { get; set; }

        /// <summary>
        /// Gets or sets hourly rate
        /// </summary>
        [Required(ErrorMessage = "Please enter hourly rate.")]
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public Decimal Hourly { get; set; }

        /// <summary>
        /// Gets or sets overdue hourly rate
        /// </summary>
        [Required(ErrorMessage = "Please enter overdue hourly rate.")]
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public Decimal OverdueHourly { get; set; }

        /// <summary>
        /// Gets or sets booking fee
        /// </summary>
        [Required(ErrorMessage = "Please enter booking fee.")]
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public Decimal BookingFee { get; set; }

        /// <summary>
        /// Gets or sets booking fee
        /// </summary>
        [Required(ErrorMessage = "Please enter missed booking fee.")]
        [DisplayFormat(DataFormatString = "{0:0.00}")]
        public Decimal MissedBookingFee { get; set; }

        public decimal DiscountRate { get; set; }

        /// <summary>
        /// Gets or sets the name of the user that updated the entity
        /// </summary>
        /// <value>The updated by.</value>
        public string UpdatedBy { get; set; }

        /// <summary>
        /// Gets or sets the date the entity was updated.
        /// </summary>
        /// <value>The updated at.</value>
        public DateTime UpdatedAt { get; set; }

        /// <summary>
        /// Gets or sets ID
        /// </summary>
        public Guid Id { get; set; }

    }
}
