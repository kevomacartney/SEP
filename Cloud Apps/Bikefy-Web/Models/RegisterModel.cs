using System.ComponentModel.DataAnnotations;

namespace CityCycles.Web.Models
{
    public class RegisterModel
    {
        /// <summary>
        /// Gets or sets the email address
        /// </summary>
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Required(ErrorMessage = "Email address required")]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the users first name
        /// </summary>
        [Required(ErrorMessage = "First Name required")]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the users last name
        /// </summary>
        [Required(ErrorMessage = "Last Name required")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter a Street Address")]
        public string StreetAddress { get; set; }

        [Required(ErrorMessage = "Please enter a city")]
        public string City { get; set; }

        [Required(ErrorMessage = "Please enter a postcode")]
        public string PostCode { get; set; }

        /// <summary>
        /// gets or sets password
        /// </summary>

        //[RegularExpression(@"(?=^.{8, 12}$)((?=.*\d)(?=.*[A - Z])(?=.*[a - z])|(?=.*\d)(?=.*[^ A - Za - z0 - 9])(?=.*[a - z])|(?=.*[^ A - Za - z0 - 9])(?=.*[A - Z])(?=.*[a - z])|(?=.*\d)(?=.*[A - Z])(?=.*[^ A - Za - z0 - 9]))^.*", ErrorMessage = "Invalid password.\r\nMust contain: \r\n8 characters, at least 1 capital letter, at least one special character.\r\n eg. Password1?")]

        [Required(ErrorMessage = "Password Required")]
        public string Password { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "Please confirm your password")]
        public string ConfirmPassword { get; set; }
    }
}
