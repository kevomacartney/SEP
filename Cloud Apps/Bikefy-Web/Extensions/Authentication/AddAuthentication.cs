using System;
using CityCycles.Composite.Model.Shared.Settings;
using CityCycles.Composite.Shared_Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CityCycles.Web.Extensions.Authentication
{
    public static class Authentication
    {
        /// <summary>
        /// Adds the Json Web Token(JWT) authentication to the pipeline
        /// </summary>
        /// <param name="services">Services.</param>
        public static void ConfigureAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            var secretKeys = new SecretKeys();
            configuration.GetSection("SecretKeys").Bind(secretKeys);

            var encryptService = new EncryptionService("00E7EB8C24190E2187", secretKeys);
            services.AddSingleton(encryptService);
            services.AddSingleton(secretKeys);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;

                x.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;

            }).AddCookie(c =>
            {
                c.Cookie.Name = "CityCyles.Auth";
                c.Cookie.HttpOnly = true;
                c.Cookie.Expiration = TimeSpan.FromDays(1);
                c.Cookie.SecurePolicy = Microsoft.AspNetCore.Http.CookieSecurePolicy.Always;
                //c.Cookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode;
                c.LoginPath = $"/Account/Login";
                c.LogoutPath = $"/Account/Logout";
                c.AccessDeniedPath = $"/Account/AccessDenied";
            });

        }
    }
}
