using Braintree;
using CityCycles.Composite.Model.Account;
using CityCycles.Composite.Model.Auth;
using CityCycles.Web.Models;
using CityCycles.Web.Providers;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Graph;

namespace CityCycles.Web.Controllers
{
    public class AccountController : Controller
    {
        public static class LogEvents
        {
            public static readonly EventId HandlingLogin = new EventId(6000, nameof(Login));

            public static readonly EventId RegisteringUser = new EventId(6001, nameof(Register));
        }

        private readonly IServiceProvider _serviceProvider;

        private readonly IApiProvider _apiProvider;

        private readonly ILogger _logger;

        private readonly IBraintreeGateway _braintree;

        public AccountController(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;

            _apiProvider = _serviceProvider.GetService<IApiProvider>();
            _logger = _serviceProvider.GetService<ILogger<AccountController>>();

            _braintree = _serviceProvider.GetService<IBraintreeGateway>();
        }

        /// <summary>
        /// Login action called when page is loaded, is passed username if coming from register page
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login([FromQuery] string username, [FromQuery(Name = "ReturnUrl")] string returnUrl)
        {
            // If user is already authenticated
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            using (_logger.BeginScope(new Dictionary<string, object>
            {
                ["Email"] = username
            }))
            {
                _logger.LogInformation(LogEvents.HandlingLogin, "User logging begin");

                // Get the return url for this request 
                ViewBag.ReturnUrl = returnUrl;

                // Autofil the username
                if (string.IsNullOrEmpty(username))
                {
                    return View();
                }

                return View(new LoginModel() { Username = username });
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginModel model, [FromForm(Name = "ReturnUrl")] string returnUrl)
        {
            // Check if model is valid
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            using (_logger.BeginScope(new Dictionary<string, object>
            {
                ["Email"] = model.Username
            }))
            {

                _logger.LogInformation(LogEvents.HandlingLogin, $"Login attempted for user");

                string result;

                //Create a model and insert relevant information
                AuthenticateModel apiModel = new AuthenticateModel
                {
                    Username = model.Username,
                    Password = model.Password
                };

                try
                {
                    result = await _apiProvider.SendPostRequest(_apiProvider.BuildUrl("Auth", "Authenticate"),
                        apiModel);

                    // Convert the string into a token
                    var token = new JwtSecurityTokenHandler().ReadJwtToken(result);

                    // Create cookie options
                    var authOptions = new AuthenticationProperties()
                    {
                        AllowRefresh = model.Remember,
                        ExpiresUtc = DateTime.UtcNow.AddHours(2),
                        IssuedUtc = DateTime.UtcNow,
                        IsPersistent = true
                    };

                    // Get the user claims from the user
                    var claimsIdentity = new ClaimsIdentity(token.Claims,
                        CookieAuthenticationDefaults.AuthenticationScheme, ClaimTypes.Name, ClaimTypes.Role);

                    // Sign in the user
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                        new ClaimsPrincipal(claimsIdentity), authOptions);

                    // Redirect the user to the requested page or take them home
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }

                    return RedirectToAction("Index", "Home");

                }
                catch (WebException ex)
                {
                    _logger.LogWarning(LogEvents.HandlingLogin, ex, "Error authenticating user.");

                    //Display the error
                    ModelState.AddModelError("Custom-Error", ex.Message);

                    return View(model);
                }
            }
        }

        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            // Signout the user 
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);


            return RedirectToAction("Login");
        }

        [ActionName("Register")]
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }

        //This is for post requests
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (!ModelState.IsValid)
            {
                //Display the error
                ModelState.AddModelError("Custom-Error", "Please double check the information provided.");
                return View(model);
            }

            using (_logger.BeginScope(new Dictionary<string, object>
            {
                ["Email"] = model.Email
            }))
            {
                _logger.LogInformation(LogEvents.RegisteringUser, "Registering a user");


                //Confirm both passwords are matching
                if (model.Password != model.ConfirmPassword)
                {
                    model.ConfirmPassword = null;
                    ModelState.AddModelError("Password", "Passwords do not match");

                    _logger.LogInformation(LogEvents.RegisteringUser,
                        "User not registered, confirm password is invalid");

                    //return the view with the new error message but keep entered info
                    return View(model);
                }

                //Create a model and insert relevant information
                CreateAccountModel apiModel = new CreateAccountModel
                {
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Password = model.Password
                };

                //Creation of url for Post request
                string url = _apiProvider.BuildUrl("Auth", "CreateUser");

                try
                {
                    //Send request to api to try for response
                    string message = await _apiProvider.SendPostRequest(url, apiModel);

                    var apiResult = JsonConvert.DeserializeObject<Dictionary<string, object>>(message);

                    using (_logger.BeginScope(apiResult))
                    {
                        _logger.LogInformation(LogEvents.RegisteringUser,
                            $"Registered user {model.Email} successfully with API");
                    }

                    var btRequest = new CustomerRequest
                    {
                        Id = apiResult["UserId"].ToString(),
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Email = model.Email
                    };

                    // Create customer
                    var btResult = await _braintree.Customer.CreateAsync(btRequest);

                    if (!btResult.IsSuccess())
                    {
                        using (_logger.BeginScope(new Dictionary<string, object>()
                        {
                            ["Braintree Error Obj"] = btResult.Errors
                        }))
                        {
                            _logger.LogWarning(LogEvents.RegisteringUser,
                                "There was an error while creating user account");
                        }

                        return RedirectToAction("Error", "Home");
                    }

                    var addressRequest = new AddressRequest()
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        StreetAddress = model.StreetAddress,
                        Locality = model.City,
                        PostalCode = model.PostCode
                    };

                    // Send an address request
                    var addResult = await _braintree.Address.CreateAsync(btRequest.Id, addressRequest);
                    if (!addResult.IsSuccess())
                    {
                        using (_logger.BeginScope(new Dictionary<string, object>()
                        {
                            ["Braintree Error Obj"] = addResult.Errors
                        }))
                        {
                            _logger.LogWarning(LogEvents.RegisteringUser,
                                "There was an error while adding user address to braintree");
                        }
                    }
                    else
                    {
                        _logger.LogInformation(LogEvents.RegisteringUser,
                            $"Registered user  {model.Email} as customer on Braintree");
                    }
                }
                catch (Exception e)
                {
                    //Display the error
                    ModelState.AddModelError("Custom-Error", e.Message);

                    return View(model);
                }

                //Redirect the action after successful register
                return RedirectToAction("Login", "Account", new { username = model.Email });
            }
        }

        public IActionResult Bookings()
        {
            return View();
        }


        [ActionName("AccountInfo")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> AccountInfo()
        {
            //Retrieve User data from database to display
            var profileUrl = _apiProvider.BuildUrl("Auth", $"Profile/{User.FindFirst("nameid").Value}");

            var result = await _apiProvider.SendGetRequest(profileUrl);

            var profile = JsonConvert.DeserializeObject<Profile>(result);

            //Create a model and insert relevant information
            AccountInfoModel infoModel = new AccountInfoModel
            {
                Email = profile.Email,
                FirstName = profile.FirstName,
                LastName = profile.LastName,
                City = profile.City,
                PostCode = profile.PostCode,
                StreetAddress = profile.StreetAddress
            };
            return View(infoModel);
        }

        [HttpGet]
        public async Task<IActionResult> UpdateAccountInfo()
        {
            //Retrieve User data from database to display
            var profileUrl = _apiProvider.BuildUrl("Auth", $"Profile/{User.FindFirst("nameid").Value}");

            var result = await _apiProvider.SendGetRequest(profileUrl);

            var profile = JsonConvert.DeserializeObject<Profile>(result);

            //Create a model and insert relevant information
            AccountInfoModel infoModel = new AccountInfoModel
            {
                Email = profile.Email,
                FirstName = profile.FirstName,
                LastName = profile.LastName,
                City = profile.City,
                PostCode = profile.PostCode,
                StreetAddress = profile.StreetAddress
            };
            return View(infoModel);
        }


        //This is for post requests
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult>UpdateAccountInfo(AccountInfoModel model)
        {
            if (!ModelState.IsValid)
            {
                //Display the error
                ModelState.AddModelError("Custom-Error", "Please double check the information provided.");
                return View("UpdateAccountInfo", model);
            }
            else
            {
                //Create a model and insert relevant information
                var profile = new Profile
                {
                    UserId = Guid.Parse(User.FindFirst("nameid").Value),
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    //Password = model.Password,
                    City = model.City,
                    PostCode = model.PostCode,
                    StreetAddress = model.StreetAddress
                };

                if (false) ;

                //Creation of url for Post request
                string url = _apiProvider.BuildUrl("Auth", $"UpdateProfile/{User.FindFirst("nameid").Value}");

                try
                {
                    //Send request to api to try for response
                    await _apiProvider.SendPostRequest(url, profile);

                }
                catch (Exception e)
                {
                    //Display the error
                    ModelState.AddModelError("Custom-Error", e.Message);

                    return View("UpdateAccountInfo",model);
                }
                return RedirectToAction("AccountInfo", "Account");
            }
        }
    }
}