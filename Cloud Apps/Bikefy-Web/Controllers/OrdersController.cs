﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CityCycles.Web.Controllers;
using CityCycles.Composite.Data.ManagementData;
using CityCycles.Web.Models;
using CityCycles.Web.Providers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;


namespace CityCycles.Web.Controllers
{
    public class OrdersController : Controller
    {
        private readonly IApiProvider _apiProvider;

        private readonly ILogger _logger;

        public OrdersController(IServiceProvider provider)
        {
            _apiProvider = provider.GetService<IApiProvider>();
            _logger = provider.GetService<ILogger<BookingsController>>();
        }

        // GET: Receipt/{id}
        [HttpPost("Receipt/{id}")]
        public IActionResult Receipt([FromRoute(Name = "id")]Guid bookingId)
        {
            return View();
        }

        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            //// Order Url
            //var orderUrl = _apiProvider.BuildUrl("Order", "FindAll");

            //try
            //{
            //    // Send both requests at once
            //    var orderResult = await _apiProvider.SendGetRequest(orderUrl);

            //    // wait for both requests results
            //    var orders = JsonConvert.DeserializeObject<List<BookingsModel>>(orderResult);

            //    var models = new List<BookingsModel>();

            //    // Create order models
            //    foreach (var order in orders)
            //    {
            //        // Get the user's name
            //        //So you can go through their bookings help on database
                    

            //        var m = new BookingsModel()
            //        {
            //            StartDate = order.StartDate,
            //            EndDate = order.EndDate,
            //            DockId = order.DockId,
            //            Pricing = order.Pricing

            //        };

            //        // Add the current model to the list of models 
            //        models.Add(m);
            //    }

            //    return View(models);
            //}
            //catch (WebException e)
            //{
            //    _logger.LogError(e, "There was error while retrieving booking models");

            //    return RedirectToAction("Error", "Home");
            //
            return View();
        }

    }
}
