﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Braintree;
using CityCycles.Composite.BusMessages.Management;
using CityCycles.Composite.Data.ManagementData;
using CityCycles.Composite.Model.Account;
using CityCycles.Composite.Model.Booking;
using CityCycles.Composite.Model.Dock;
using CityCycles.Web.Models;
using CityCycles.Web.Providers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NServiceBus;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CityCycles.Web.Controllers
{
    [Authorize]
    public class BookingsController : Controller
    {
        public static class LogEvents
        {
            public static readonly EventId MakingBooking = new EventId(7000, nameof(MakeBooking));

        }

        /// <summary>
        /// The logger.
        /// </summary>
        private readonly ILogger _logger;

        /// <summary>
        /// The API provider.
        /// </summary>
        private readonly IApiProvider _apiProvider;

        /// <summary>
        /// The braintree gateway
        /// </summary>
        private readonly IBraintreeGateway _braintree;

        /// <summary>
        /// The endpoint.
        /// </summary>
        private readonly IEndpointInstance _endpoint;

        public BookingsController(IServiceProvider serviceProvider)
        {
            _logger = serviceProvider.GetService<ILogger<BookingsController>>();

            _apiProvider = serviceProvider.GetService<IApiProvider>();
            _braintree = serviceProvider.GetService<IBraintreeGateway>();
            _endpoint = serviceProvider.GetService<IEndpointInstance>();
        }

        [HttpGet]
        public async Task<IActionResult> AllBookings()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> MyBookings()
        {
            var url = _apiProvider.BuildUrl("Booking", $"FindAllUserBookings/{User.FindFirst("nameid").Value}");

            var json = await _apiProvider.SendGetRequest(url);
            var myBookings = JsonConvert.DeserializeObject<List<Booking>>(json);

            var model = new List<ReceiptModel>();

            myBookings.ForEach(b =>
            {
                model.Add(new ReceiptModel()
                {
                    BookingId = b.Id,
                    EndDate = b.EndDate,
                    StartDate = b.StartDate,
                    GrandTotal = b?.Order?.Total.ToString("0.##"),
                    Status = b.Status,
                    TimeTotal = (b.EndDate - b.StartDate).TotalHours.ToString("0.##")
                });
            });

            return View(model);
        }

        [HttpGet("[controller]/[action]/{id}")]
        public async Task<IActionResult> Receipt([FromRoute(Name = "id")]Guid bookingId)
        {
            if (bookingId == Guid.Empty)
                return BadRequest(); // todo, Joe add better error handling

            // Get the booking
            var url = _apiProvider.BuildUrl("Booking", $"FindById/{bookingId}");

            // todo, handle the error
            var json = await _apiProvider.SendGetRequest(url);

            var booking = JsonConvert.DeserializeObject<Booking>(json);


            // Get user profile
            url = _apiProvider.BuildUrl("Auth", $"Profile/{User.FindFirst("nameid").Value}");
            var user = JsonConvert.DeserializeObject<Profile>(await _apiProvider.SendGetRequest(url));

            // Get Pricing
            url = _apiProvider.BuildUrl("Pricing", $"FindById/{booking.PricingId}");
            var pricing = JsonConvert.DeserializeObject<Pricing>(await _apiProvider.SendGetRequest(url));


            // Calculate the times
            var timespan = booking.EndDate - booking.StartDate;
            var timeTotal = Convert.ToDecimal(timespan.TotalHours) * pricing.HourlyPrice;

            // Create hthe model
            var model = new ReceiptModel()
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                TimeTotal = timeTotal.ToString("0.##"),
                BookingId = booking.Id,
                Status = booking.Status,
                StartDate = booking.StartDate,
                Units = timespan.Hours.ToString("0.##"),
                EndDate = booking.EndDate,
                GrandTotal = booking.Order.Total.ToString("0.##"),
                OrderDate = booking.CreatedOn
            };

            // If booking contains booking fee
            if (booking.Order.Items != null && booking.Order.Items.TryGetValue("BookingFee", out var fee))
            {
                model.BookingFee = booking.Order.Items["BookingFee"].ToString("0.##");
            }


            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> MakeBooking()
        {
            // Start request to get access token
            var tokenT = _braintree.ClientToken.GenerateAsync(new ClientTokenRequest
            {
                CustomerId = User.FindFirst("nameid").Value
            });

            try
            {
                // Prepare the bookings model
                var model = await PrepareBookingsModel();

                // await the token from the braintree api
                ViewData["PaymentToken"] = await tokenT;


                // Return the booking mdoel
                return View(model);
            }
            catch (Exception e)
            {
                _logger.LogWarning(LogEvents.MakingBooking, e, "Could not get get docks from API");
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> MakeBooking(BookingModel request)
        {
            DateTime startDate;
            DateTime endDate;

            try
            {
                // Convert stirng dates to date structure
                startDate = DateTime.ParseExact(request.StartDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                endDate = DateTime.ParseExact(request.EndDate, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
                if (e is ArgumentNullException || e is FormatException)
                {
                    _logger.LogWarning(LogEvents.MakingBooking, e, "Could not convert string dates '{startDate}' and '{endDate}' to Datetime structures", request.StartDate, request.StartDate);
                    BookingModel model = await PrepErroredBookingState();
                    return View(model);
                }

                _logger.LogError(LogEvents.MakingBooking, e, "Could convert date time due to an unexpected exception");

                throw e;
            }

            // validate form
            if (!ModelState.IsValid)
            {
                BookingModel model = await PrepErroredBookingState();

                // Return the booking mdoel
                return View(model);
            }

            var bookingRequest = new BookingRequest()
            {
                DockId = request.DockId,
                StartDate = startDate,
                EndDate = endDate,
                UserId = Guid.Parse(User.FindFirst("nameid").Value)
            };

            var url = _apiProvider.BuildUrl("Booking", "CreateNew");

            // send booking request to api
            var result = await _apiProvider.SendPostRequest(url, bookingRequest);

            // deserialize model to get booking Id
            Dictionary<string, Guid> booking = JsonConvert.DeserializeObject<Dictionary<string, Guid>>(result);

            // If there was an error while reading the dictionary
            if (booking == null || !booking.TryGetValue("BookingId", out var bookingId))
                return RedirectToAction("Error", "Home");

            // If customer wants to pay for the booking now
            if (request.Paynow)
            {
                url = _apiProvider.BuildUrl("Pricing", "Default");

                // Get the default pricing from the api
                var pricing = JsonConvert.DeserializeObject<Pricing>(await _apiProvider.SendGetRequest(url));

                var completeOrder = new ProcessBooking()
                {
                    BookingId = bookingId,
                    CreatedOn = DateTime.UtcNow,
                    PaymentNonce = Request.Form["payment_method_nonce"].FirstOrDefault(),
                    PricingId = pricing.Id
                };

                // Send the message to be processed.
                await _endpoint.Send(completeOrder);
            }

            // take user to their bookings page
            return RedirectToAction(nameof(MyBookings));
        }

        [HttpGet("[controller]/[action]/{id}")]
        public async Task<IActionResult> RetryPayment([FromRoute(Name = "id")]Guid bookingId)
        {
            // Get the booking
            var url = _apiProvider.BuildUrl("Booking", $"FindById/{bookingId}");

            // todo, handle the error
            var booking = JsonConvert.DeserializeObject<Booking>(await _apiProvider.SendGetRequest(url));


            url = _apiProvider.BuildUrl("Pricing", "Default");

            // Get the default pricing from the api
            var pricing = JsonConvert.DeserializeObject<Pricing>(await _apiProvider.SendGetRequest(url));


            var completeOrder = new ProcessBooking()
            {
                BookingId = bookingId,
                CreatedOn = DateTime.UtcNow,
                PricingId = pricing.Id
            };

            // Send payment to be retried
            await _endpoint.Send(completeOrder);

            return RedirectToAction(nameof(MyBookings));
        }

        private async Task<BookingModel> PrepErroredBookingState()
        {
            // Start request to get access token
            var tokenT = _braintree.ClientToken.GenerateAsync(new ClientTokenRequest
            {
                CustomerId = User.FindFirst("nameid").Value
            });

            // Prepare the bookings model
            var model = await PrepareBookingsModel();

            // await the token from the braintree api
            ViewData["PaymentToken"] = await tokenT;
            return model;
        }

        /// <summary>
        /// Gets all the docks
        /// </summary>
        /// <returns>The docks async.</returns>
        private async Task<List<DockStatus>> GetDocksAsync()
        {
            var url = _apiProvider.BuildUrl("Dock", "FindAll");

            var result = await _apiProvider.SendGetRequest(url);

            // Get docks from the api and deserialize them to dock status object
            var dock = JsonConvert.DeserializeObject<List<DockStatus>>(result);

            return dock;
        }

        /// <summary>
        /// Prepares the booking page.
        /// </summary>
        /// <returns>The booking page.</returns>
        private async Task<BookingModel> PrepareBookingsModel()
        {
            // Get the docks
            var dock = await GetDocksAsync();
            if (dock != null)
                // Create the dropdown of docks for the view
                ViewData["DockId"] = new SelectList(dock, "DockId", "DockName");
            else
                _logger.LogWarning(LogEvents.MakingBooking, "API did not return valid Dockstatus array");

            // Get the dafault pricing scheme
            var url = _apiProvider.BuildUrl("Pricing", "Default");
            var result = await _apiProvider.SendGetRequest(url);
            var pricing = JsonConvert.DeserializeObject<Pricing>(result);

            // Null checking
            if (pricing == null)
                throw new NullReferenceException("Could not retrieve default pricing scheme.");

            var pricingModel = new PricingModel()
            {
                BookingFee = pricing.BookingFees,
                Hourly = pricing.HourlyPrice,
                MissedBookingFee = pricing.MissedBookingFees,
                OverdueHourly = pricing.OverdueHourlyPrice,

            };

            return new BookingModel()
            {
                Pricing = pricingModel,
                StartDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm"),
                EndDate = DateTime.Now.AddHours(2).ToString("dd/MM/yyyy HH:mm")
            };
        }
    }
}
