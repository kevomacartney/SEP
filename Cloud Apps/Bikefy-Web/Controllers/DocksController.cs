﻿using CityCycles.Composite.BusMessages.Management;
using CityCycles.Composite.Data.ManagementData;
using CityCycles.Composite.Model.Bike;
using CityCycles.Composite.Model.Dock;
using CityCycles.Web.Providers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using NServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CityCycles.Web.Controllers
{
    [Route("[controller]/[action]")]
    public class DocksController : Controller
    {
        public static class LogEvents
        {
            public static readonly EventId OnBoardingDock = new EventId(1, nameof(OnBoardPost));
            public static readonly EventId UpdatingDock = new EventId(2, nameof(Update));
        }

        private readonly IServiceProvider _serviceProvider;

        private IApiProvider _apiProvider;
        private IEndpointInstance _endpointInstance;

        private ILogger _logger;

        public DocksController(IServiceProvider provider)
        {
            _apiProvider = provider.GetService<IApiProvider>();
            _endpointInstance = provider.GetService<IEndpointInstance>();

            _logger = provider.GetService<ILogger<DocksController>>();
        }

        [AllowAnonymous]
        public async Task<IActionResult> Index()
        {
            var json = await _apiProvider.SendGetRequest(_apiProvider.BuildUrl("Dock", "FindAll"));

            var docks = JsonConvert.DeserializeObject<List<DockStatus>>(json);


            return View(docks);
        }


        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Locations()
        {
            var url = _apiProvider.BuildUrl("Dock", "FindAll");

            // Send the request
            var result = await _apiProvider.SendGetRequest(url);

            var model = JsonConvert.DeserializeObject<List<DockStatus>>(result);

            return View(model);
        }

        [HttpGet]
        //[Authorize(Roles = CityCyclesRoles.CityCyclesDocks)]
        public IActionResult OnBoard()
        {
            return View();
        }

        [HttpPost, ActionName("OnBoard")]
        [ValidateAntiForgeryToken]
        //Authorize(Roles = CityCyclesRoles.CityCyclesDocks)
        public async Task<IActionResult> OnBoardPost(DockRequest request)
        {
            // Model is not valid
            if (!ModelState.IsValid)
            {
                return View(request);
            }

            var url = _apiProvider.BuildUrl("Dock", "CreateNew");

            try
            {
                // Send the request
                var result = await _apiProvider.SendPostRequest(url, request);

                var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(result);

                // Generate a random string of length 32 to be used as the password to the dock.
                var p = RandomString(32);

                // Request Api to create a dock account
                await _endpointInstance.Send(new CreateDockAccount()
                {
                    DockId = Guid.Parse(dict["dockId"]),
                    DockName = request.Name,
                    Password = p
                });

                // Set view data to contain the password 
                ViewData["hasPassword"] = true;
                ViewData["dockId"] = dict["dockId"];
                ViewData["DockPassword"] = p;

                return View();
            }
            catch (WebException e)
            {
                _logger.LogWarning(LogEvents.OnBoardingDock, e, "Could not get a dock");

                ViewBag.CustomError = true;
                //Display the error
                ModelState.AddModelError("Custom-Error", e.Message);

                return View(request);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Update([FromRoute(Name = "id")]Guid dockId)
        {
            // Validate dock id
            if (dockId == Guid.Empty)
            {
                return RedirectToAction("Error", "Home");
            }

            var url = _apiProvider.BuildUrl("Dock", $"FindById/{dockId}");

            try
            {
                var dock = JsonConvert.DeserializeObject<Dock>(await _apiProvider.SendGetRequest(url));

                // If dock couldn't be found
                if (dock == null)
                {
                    ModelState.AddModelError("Custom-Error", "Dock with the provided id could not be retrieved");
                    ViewBag.CustomError = true;
                    return View();
                }

                // Split it by the comma
                var locations = dock.Location.Split(" ");

                // Create dock request model and pass it to the view
                var dockRequest = new DockRequest()
                {
                    Id = dock.Id,
                    Name = dock.Name,
                    Capacity = dock.MaximumCapacity,
                    Latitude = decimal.Parse(locations.FirstOrDefault()),
                    Longitude = decimal.Parse(locations.ElementAtOrDefault(1)),
                };

                // Iterate over all the bikes for this dock
                foreach (var bike in dock.Bikes)
                {
                    // Add the current bike to the dock model
                    dockRequest.Bikes.Add(new BikeModel()
                    {
                        BikeId = bike.Id,
                        Status = bike.Status,
                        DockId = bike.DockId,
                        LastUpdate = bike.LastReport
                    });
                }

                return View(dockRequest);
            }
            catch (Exception e)
            {
                // Log error and send user to error page
                _logger.LogError(LogEvents.UpdatingDock, e, "There was an error retriving dock from the api");
                return RedirectToAction("Error", "Home");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(DockRequest request)
        {
            // Validate the model
            if (!ModelState.IsValid)
            {
                return View();
            }

            var url = _apiProvider.BuildUrl("Dock", "Update");

            try
            {
                await _apiProvider.SendPostRequest(url, request);

                return RedirectToAction("Locations");
            }
            catch (WebException e)
            {
                using (_logger.BeginScope(new Dictionary<string, object>
                {
                    ["Dock"] = request.Id
                }))
                {
                    // Log error and send user to error page
                    _logger.LogError(LogEvents.UpdatingDock, e, "There was an error updating the dock.");

                    ModelState.AddModelError("Custom-Error", e.Message);
                    ViewBag.CustomError = true;
                    return View(request);
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(Guid DockId)
        {
            var url = _apiProvider.BuildUrl("Dock", $"Delete/{DockId}");

            try
            {
                await _apiProvider.SendGetRequest(url);

                // Send bus message to delete  dock account
                await _endpointInstance.Send(new DeleteDockAccount()
                {
                    DockId = DockId
                });

                return RedirectToAction("Locations");
            }
            catch (WebException e)
            {
                using (_logger.BeginScope(new Dictionary<string, object>
                {
                    ["Dock"] = DockId
                }))
                {
                    // Log error and send user to error page
                    _logger.LogError(LogEvents.UpdatingDock, e, "There was an error updating the dock.");

                    return RedirectToAction("Error", "Home");
                }
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> AddBike(Guid id)
        {
            BikeRequest req = new BikeRequest
            {
                DockId = id
            };

            //Creation of url for Post request
            string url = _apiProvider.BuildUrl("Bikes", "CreateNew");

            try
            {
                //Send request to api to try for response
                string message = await _apiProvider.SendPostRequest(url, req);

                var apiResult = JsonConvert.DeserializeObject<Dictionary<string, object>>(message);

                using (_logger.BeginScope(apiResult))
                {
                    //_logger.LogInformation(LogEvents.CreateNewBike,
                    //    $"Created new Bike at Dock {model.DockName} successfully with API");
                }

            }
            catch (Exception e)
            {
                //Display the error
                ModelState.AddModelError("Custom-Error", e.Message);

                return RedirectToAction(nameof(Update), new { id = id });
            }

            return RedirectToAction(nameof(Update), new { id = id });
        }

        // GET: api/Bike/Delete
        [HttpGet("{id}")]
        public async Task<IActionResult> DeleteBike([FromQuery(Name = "bike")]Guid bikeId, [FromRoute(Name = "id")]Guid dockId)
        {
            var url = _apiProvider.BuildUrl("Bikes", $"Delete/{bikeId}");

            await _apiProvider.SendGetRequest(url);

            return RedirectToAction(nameof(Update), new { id = dockId });

        }

        /// <summary>
        /// Generates a random the string.
        /// </summary>
        /// <returns>The string.</returns>
        /// <param name="length">Length.</param>
        public static string RandomString(int length)
        {
            var random = new Random();

            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@$#.?';=+><|";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
