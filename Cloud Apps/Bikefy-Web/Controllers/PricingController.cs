﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using CityCycles.Composite.Data.ManagementData;
using CityCycles.Composite.Model.Pricing;
using CityCycles.Web.Models;
using CityCycles.Web.Providers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CityCycles.Web.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class PricingController : Controller
    {
        private readonly IApiProvider _apiProvider;

        private readonly ILogger _logger;

        public PricingController(IServiceProvider provider)
        {
            _apiProvider = provider.GetService<IApiProvider>();
            _logger = provider.GetService<ILogger<PricingController>>();
        }

        [HttpGet]
        public async Task<IActionResult> Table()
        {
            // Pricing Url
            var pricingUrl = _apiProvider.BuildUrl("Pricing", "FindAll");

            try
            {
                // Send both requests at once
                var pricingsResult = await _apiProvider.SendGetRequest(pricingUrl);

                // wait for both requests results
                var pricings = JsonConvert.DeserializeObject<List<Pricing>>(pricingsResult);

                var models = new List<PricingModel>();

                // Create pricing models
                foreach (var pricing in pricings)
                {
                    // Get the user's name
                    var nameUrl = _apiProvider.BuildUrl("Auth", $"NamesById/{pricing.Updatedby}");
                    var updatedbyT = _apiProvider.SendGetRequest(nameUrl);

                    var m = new PricingModel()
                    {
                        Id = pricing.Id,
                        Label = pricing.Label,
                        UpdatedAt = pricing.UpdateAt,
                        OverdueHourly = pricing.OverdueHourlyPrice,
                        Hourly = pricing.HourlyPrice,
                        BookingFee = pricing.BookingFees,
                        MissedBookingFee = pricing.MissedBookingFees,
                        DiscountRate = pricing.DiscountHourlyPrice
                    };

                    var names = JsonConvert.DeserializeObject<Dictionary<string, string>>(await updatedbyT);

                    m.UpdatedBy = $"{names["FirstName"]} {names["LastName"]}";

                    // Add the current model to the list of models 
                    models.Add(m);
                }

                return View(models);
            }
            catch (WebException e)
            {
                _logger.LogError(e, "There was error while retrieving pricing models");

                return RedirectToAction("Error", "Home");
            }
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [Authorize, ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PricingModel model)
        {
            // Check if model is valid
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            //Create a new pricing request with which will be passed to the api
            var request = new PricingRequest()
            {
                Label = model.Label,
                Hourly = model.Hourly,
                OverdueHourly = model.OverdueHourly,
                BookingFee = model.BookingFee,
                MissedBookingFee = model.MissedBookingFee,
                UpdatedBy = Guid.Parse(User.FindFirst("nameid").Value),
                DiscountRate = model.DiscountRate
            };

            var url = _apiProvider.BuildUrl("Pricing", "CreateNew");

            //todo, handle api error

            // Send the request
            var result = await _apiProvider.SendPostRequest(url, request);

            return RedirectToAction("Table", "Pricing");
        }

        //Used when first displaying the original pricing structure before update
        [HttpGet("{id}")]
        public async Task<IActionResult> Update([FromRoute(Name = "id")]Guid pricingId)
        {
            // Validate pricing id
            if (pricingId == Guid.Empty)
                return RedirectToAction("Error", "Home");

            var url = _apiProvider.BuildUrl("Pricing", $"FindById/{pricingId}");

            try
            {
                var pricing = JsonConvert.DeserializeObject<Pricing>(await _apiProvider.SendGetRequest(url));
                // If pricing couldn't be found
                if (pricing == null)
                {
                    _logger.LogWarning("Could not find pricing entiy with the given identifier");
                    return RedirectToAction("Error", "Home");
                }

                // Create pricing request model and pass it to the view
                var pricingRequest = new PricingRequest()
                {
                    Id = pricing.Id,
                    Hourly = pricing.HourlyPrice,
                    OverdueHourly = pricing.OverdueHourlyPrice,
                    BookingFee = pricing.BookingFees,
                    MissedBookingFee = pricing.MissedBookingFees,
                    UpdatedBy = pricing.Updatedby,
                    UpdatedAt = pricing.UpdateAt,
                    Label = pricing.Label
                };

                return View(pricingRequest);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "There was an eror while getting pricing entity for update page.");
                return View();
            }

        }

        [HttpPost("{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(
            PricingRequest request)
        {
            // Check if model is valid
            if (!ModelState.IsValid)
            {
                return View();
            }

            request.UpdatedBy = Guid.Parse(User.FindFirst("nameid").Value);

            var url = _apiProvider.BuildUrl("Pricing", "Update");

            // Send the request
            var result = await _apiProvider.SendPostRequest(url, request);

            return RedirectToAction("Table", "Pricing");
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Delete([FromRoute]Guid id)
        {
            var url = _apiProvider.BuildUrl("Pricing", $"Delete/{id}");

            // Send the request
            var result = await _apiProvider.SendGetRequest(url);
            // todo, Joe  handlings of errors

            return RedirectToAction("Index", "Pricing");
        }
    }
}
